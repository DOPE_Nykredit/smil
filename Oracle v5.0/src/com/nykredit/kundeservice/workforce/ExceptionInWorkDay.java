package com.nykredit.kundeservice.workforce;
import com.nykredit.kundeservice.util.TimeOfDay;
import com.nykredit.kundeservice.util.TimeOutOfBoundsException;
import com.nykredit.kundeservice.util.TimeSpan;

public class ExceptionInWorkDay {
	public enum WorkdayExceptionType{TEAM_M�DE(1, 3, "Teamm�de", true),
									 DELVIS_FRI(2, 3, "Delvis fri", false),
									 SYG(new int[]{3, 4}, 3, "Syg", false),
									 UDDANNELSE(5, 3, "Uddannelse", true),
									 UDVIDET_M�DE(6, 3, "Udvidet m�de", true),
									 L�GE_TANDL�GE(13, 3, "L�ge/tandl�ge", false),
									 FORSINKET(14, 3, "Forsinket", false),
									 BARSEL(new int[]{18, 166}, 3, "Barsel", false),
									 ORLOV(19, 3, "Orlov", false),
									 EKSAMEN_STUD(22, 3, "Eksamen stud", false),
									 INTERNT_M�DE(24, 3, "Internt m�de", true),
									 EKSTERNT_M�DE(25, 3, "Eksternt m�de", true),
									 EKSTERN_UDDANNELSE(86, 3, "Ekstern uddannelse", true),
									 HELLIGDAG(64, 3, "Helligdag", false),
									 BARN_SYG(new int[]{15, 47, 126}, 3, "Barn syg", false),
									 TELEKOMPENSRING(65, 3, "Telekompensering", false),
									 ANDET_ARBEJDE(146, 3, "Andet arbejde", true),
									 UKENDT(0, 0, "Ukendt", true),
									 AFTENSMAD(new int[]{25, 49}, 5, "Afentsmad", false),
									 FROKOST(1, 5, "Frokost", false),
									 FRI(1, 2, "Fri", false),
									 FERIE_OMSORG(0, 2, "Ferie/Omsorg", false),
									 EKSTRA_ARBEJDE(1, 9, "Ekstra arbejde", true);
		
		private int typeIds[];
		private int group;
		private String description;
		private boolean countedAsAttendence;
		
		public int[] getTypeIds(){
			return this.typeIds;
		}
		public int getGroup(){
			return this.group;
		}
		public String getDescription(){
			return this.description;
		}
		public boolean isCountedAsAttendence(){
			return this.countedAsAttendence;
		}
		
		private WorkdayExceptionType(int typeId, int group, String description, boolean countedAsAttendence){
			this(new int[]{typeId}, group, description, countedAsAttendence); 
		}
		private WorkdayExceptionType(int typeIds[], int group, String description, boolean countedAsAttendence){
			this.typeIds = typeIds;
			this.group = group;
			this.description = description;
			this.countedAsAttendence = countedAsAttendence;
		}		
	}
	
	private TimeSpan span;
	private WorkdayExceptionType type;
	
	public TimeSpan getSpan(){
		return this.span;
	}
	
	public WorkdayExceptionType getType(){
		return this.type;
	}
	
	public double getDurationHours(){
		return this.span.getDurationHours();
	}
	public int getDurationInMinutes(){
		return this.span.getDurationMinutes();
	}
	
	private static WorkdayExceptionType getExceptionType(int typeId, int group){
		for (WorkdayExceptionType e : WorkdayExceptionType.values()){
			if (e.getGroup() == group)
				for (int t : e.getTypeIds())
					if (t == typeId)
						return e;		
		}
		return WorkdayExceptionType.UKENDT;
	}
	
	public ExceptionInWorkDay(TimeSpan span, int type, int group) throws TimeOutOfBoundsException{
		this.span = span;
		
		this.type = ExceptionInWorkDay.getExceptionType(type, group);
	}
	public ExceptionInWorkDay(String stateData) throws TimeOutOfBoundsException{
		String stateDataSplit[] = stateData.split(";");
		
		int group = Integer.parseInt(stateDataSplit[0]);
		int type = Integer.parseInt(stateDataSplit[1]);
		
		int startTimeDouble = Integer.parseInt(stateDataSplit[2]);
		int endTimeDouble = Integer.parseInt(stateDataSplit[3]);
		
		this.span = new TimeSpan(new TimeOfDay(startTimeDouble),
										  new TimeOfDay(endTimeDouble));
		
		this.type = ExceptionInWorkDay.getExceptionType(type, group);
	}
}