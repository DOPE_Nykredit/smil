package com.nykredit.kundeservice.workforce;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.LocalDate;

import com.nykredit.kundeservice.baseTypes.Agent;
import com.nykredit.kundeservice.data.*;
import com.nykredit.kundeservice.data.sql.*;
import com.nykredit.kundeservice.util.OperationAbortedException;
import com.nykredit.kundeservice.util.TimeOutOfBoundsException;

public class WorkForceData {
	
	public static AgentSchedule getAgentSchedule(Agent agent,
												 Interval workforceDataInterval,
												 CTIWF2Connection conn) throws OperationAbortedException{
		if(agent == null) 
			throw new IllegalArgumentException("Initials cannot be null.");
		
		if(agent.getInitials().length() < 2 || agent.getInitials().length() > 4) 
			throw new IllegalArgumentException("Initials must be 3 or 4 characters.");
		
		if(workforceDataInterval == null) 
			throw new IllegalArgumentException("Workforce data interval cannot be null.");
		
		if(conn == null) 
			throw new IllegalArgumentException("Conn cannot be nothing");
		
		String sql = "SELECT " +
					     "days.WM_DATE AS ScheduleDate, " +
					     "days.WM_DAY_TYPE AS DayType, " +
					     "days.WM_EXCEPTION_TYPE_ID AS ExceptionType, " +
					     "days.WM_SHIFT_START_DATETIME AS ShiftStart, " +
					     "days.WM_SHIFT_END_DATETIME AS ShiftEnd, " +
					     "days.WM_PAID_MINUTES AS PaidMinutes, " +
					     "days.WM_WORK_MINUTES AS WorkMinutes, " +
					     "days.WM_SCH_STATE_DATA AS StateData, " +
					     "age.GSW_EMPLOYEE_ID AS Initials " +
					 "FROM " +
					 	 "GENESYS.WMV_SCH_NEW_DAYS days, " +
					 	 "GENESYS.WM_AGENTS age " + 
					 "WHERE " +
					 	 "days.GSW_AGENT_ID = age.GSW_AGENT_ID AND " +
					 	 "days.WM_DATE BETWEEN " + OracleFormats.convertDateTime(workforceDataInterval.getStart()) + " AND " + OracleFormats.convertDateTime(workforceDataInterval.getEnd()) + "AND " +
					 	 "age.WM_END_DATE is null AND " + 
					 	 "age.GSW_EMPLOYEE_ID = " + OracleFormats.convertString(agent.getInitials()) + " AND " + 
					 	 "sysdate BETWEEN WM_START_DATE AND NVL(WM_END_DATE, sysdate) " +
					 "ORDER BY " + 
					 	 "days.WM_DATE DESC, " + 
					 	 "age.GSW_EMPLOYEE_ID ASC";
		
		SqlQuery query = new SqlQuery(conn, sql);
		
		AgentSchedule schedule = new AgentSchedule(agent.getAgentId(),
				   								   agent.getInitials(),
				   								   agent.getFirstName(),
				   								   agent.getLastName());
		
		try {
			query.execute();
			
			while(query.next()){
				LocalDate scheduleDate = query.getLocalDate("ScheduleDate");
				int dayType = query.getInteger("DayType");
				int fullDayExceptiontype = query.getInteger("ExceptionType");
				DateTime scheduledStart = query.getDateTime("ShiftStart");
				DateTime scheduledEnd = query.getDateTime("ShiftEnd");				
				int paidMinutes = query.getInteger("PaidMinutes");
				int workMinutes = query.getInteger("workMinutes");
				String stateData[] = WorkForceData.splitStateData(query.getString("StateData"));
				
				schedule.getWorkdays().add(new WorkDay(scheduleDate, 
													   (scheduledStart == null || scheduledEnd == null ? null : new Interval(scheduledStart, scheduledEnd)), 
													   workMinutes, 
													   paidMinutes,
													   dayType,
													   fullDayExceptiontype,
													   stateData));
			}
		} catch (DopeException e) {
			throw new OperationAbortedException(e, "Error retrieving agent schedule for " + agent.getInitials() + ".", "Der skete en fejl ved indlęsning af workforcedata for " + agent.getInitials() + ".");
		}
		
		return schedule;
	}
	
	public static ArrayList<AgentSchedule> getWorkForceDataByAgent(int monthsToRetrieve, KSDriftConnection conn) throws DopeDBException, 
																				 										DopeResultSetException {
		ArrayList<AgentSchedule> agents = WorkForceData.getEmployedAgents(conn);
		
		String query = "SELECT " +
							"days.WM_DATE AS ScheduleDate, " +
					   		"days.WM_DAY_TYPE AS DayType, " +
							"days.WM_EXCEPTION_TYPE_ID AS ExceptionType, " +
							"days.WM_SHIFT_START_DATETIME AS ShiftStart, " +
							"days.WM_SHIFT_END_DATETIME AS ShiftEnd, " +
							"days.WM_PAID_MINUTES AS PaidMinutes, " +
							"days.WM_WORK_MINUTES AS WorkMinutes, " +
							"days.WM_SCH_STATE_DATA AS StateData, " +
							"age.GSW_EMPLOYEE_ID AS Initials " +
					   "FROM " +
					   		"GENESYS.WMV_SCH_NEW_DAYS@WFCM.NYKREDITNET.NET days " +
					   "INNER JOIN " +
					   		"GENESYS.WM_AGENTS@WFCM.NYKREDITNET.NET age " +
					   "ON " +
					   		"days.GSW_AGENT_ID = age.GSW_AGENT_ID " +
					   "WHERE " +
					   		"days.WM_DATE BETWEEN trunc(ADD_MONTHS(sysdate, -" + monthsToRetrieve + ")) AND " +
					   		"sysdate AND age.WM_END_DATE is null " +
					   "ORDER BY " +
					   		"days.WM_DATE DESC, " +
					   		"age.GSW_EMPLOYEE_ID ASC";

		SqlQuery workForceQuery = new SqlQuery(conn, query);
		workForceQuery.execute();
		
		while (workForceQuery.next()){
			AgentSchedule agentOfDay = null;
			
			String initials = workForceQuery.getString("Initials");
			
			for (AgentSchedule a : agents)
				if (a.getInitials().equals(initials)){
					agentOfDay = a;
					break;
				}
			
			if (agentOfDay != null){
				LocalDate scheduleDate = workForceQuery.getLocalDate("ScheduleDate");
				int dayType = workForceQuery.getInteger("DayType");
				int fullDayExceptiontype = workForceQuery.getInteger("ExceptionType");
				DateTime scheduledStart = workForceQuery.getDateTime("ShiftStart");
				DateTime scheduledEnd = workForceQuery.getDateTime("ShiftEnd");				
				int paidMinutes = workForceQuery.getInteger("PaidMinutes");
				int workMinutes = workForceQuery.getInteger("workMinutes");
				String stateData[] = WorkForceData.splitStateData(workForceQuery.getString("StateData"));
				
				try {
				    agentOfDay.getWorkdays().add(new WorkDay(scheduleDate, 
				    								   		 (scheduledStart == null || scheduledEnd == null ? null : new Interval(scheduledStart, scheduledEnd)),
				    								   		 workMinutes, 
				    								   		 paidMinutes,
				    								   		 dayType,
				    								   		 fullDayExceptiontype,
				    								   		 stateData));
				} catch (TimeOutOfBoundsException e) {
					e.printStackTrace();
				}
			}
		}
		
		return agents;
	}

	public static ArrayList<AgentSchedule> getWorkForceDataByAgent(LocalDate dateToRetrieve, KSDriftConnection conn) throws DopeDBException, 
															   																DopeResultSetException{
		ArrayList<AgentSchedule> agents = WorkForceData.getEmployedAgents(conn);
		
		SimpleDateFormat oracleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		String query = "SELECT " +
							"days.WM_DATE AS ScheduleDate, " +
					   		"days.WM_DAY_TYPE AS DayType, " +
							"days.WM_EXCEPTION_TYPE_ID AS ExceptionType, " +
							"days.WM_SHIFT_START_DATETIME AS ShiftStart, " +
							"days.WM_SHIFT_END_DATETIME AS ShiftEnd, " +
							"days.WM_PAID_MINUTES AS PaidMinutes, " +
							"days.WM_WORK_MINUTES AS WorkMinutes, " +
							"days.WM_SCH_STATE_DATA AS StateData, " +
							"age.GSW_EMPLOYEE_ID AS Initials " +
					   "FROM " +
					   		"GENESYS.WMV_SCH_NEW_DAYS@WFCM.NYKREDITNET.NET days " +
					   "INNER JOIN " +
					   		"GENESYS.WM_AGENTS@WFCM.NYKREDITNET.NET age " +
					   "ON " +
					   		"days.GSW_AGENT_ID = age.GSW_AGENT_ID " +
					   "WHERE " +
					   		"days.WM_DATE = to_date('" + oracleDateFormat.format(dateToRetrieve)  + "','YYYY-MM-DD') AND " +
					   		"age.WM_END_DATE is null " +
					   "ORDER BY " +
					   		"days.WM_DATE DESC, " +
					   		"age.GSW_EMPLOYEE_ID ASC";
		
		SqlQuery workForceQuery = new SqlQuery(conn, query);
		workForceQuery.execute();
		
		while (workForceQuery.next()){
			AgentSchedule agentOfDay = null;
			
			String initials = workForceQuery.getString("Initials");
			
			for (AgentSchedule a : agents)
				if (a.getInitials().equals(initials)){
					agentOfDay = a;
					break;
				}
			
			if (agentOfDay != null){
				LocalDate scheduleDate = workForceQuery.getLocalDate("ScheduleDate");
				int dayType = workForceQuery.getInteger("DayType");
				int fullDayExceptiontype = workForceQuery.getInteger("ExceptionType");
				DateTime scheduledStart = workForceQuery.getDateTime("ShiftStart");
				DateTime scheduledEnd = workForceQuery.getDateTime("ShiftEnd");		
				int paidMinutes = workForceQuery.getInteger("PaidMinutes");
				int workMinutes = workForceQuery.getInteger("workMinutes");
				String stateData[] = WorkForceData.splitStateData(workForceQuery.getString("StateData"));
				
				try {
				    agentOfDay.getWorkdays().add(new WorkDay(scheduleDate, 
				    								   		 (scheduledStart == null || scheduledEnd == null ? null : new Interval(scheduledStart, scheduledEnd)),
				    								   		 workMinutes, 
				    								   		 paidMinutes,
				    								   		 dayType,
				    								   		 fullDayExceptiontype,
				    								   		 stateData));				    
				} catch (TimeOutOfBoundsException e) {
					e.printStackTrace();
				}
			}
		}
		
		return agents;
	}
	
	private static ArrayList<AgentSchedule> getEmployedAgents(KSDriftConnection conn) throws DopeDBException, 
															  DopeResultSetException{
		
		ArrayList<AgentSchedule> agents = new ArrayList<AgentSchedule>();
		
		String query = "SELECT " +
							"age.ID, " +
					   		"vt.INITIALER, " +
					   		"age.FORNAVN, " +
					   		"age.EFTERNAVN, " +
					   		"wage.WM_CONTRACT_ID " +
					   "FROM " +
					   		"V_TEAM_DATO vt " +
					   "INNER JOIN " +
					   		"AGENTER_LISTE age " +
					   "ON " +
					   		"vt.AGENT_ID = age.ID " +
					   "LEFT JOIN " +
					   		"GENESYS.WM_AGENTS@WFCM.NYKREDITNET.NET wage " +
					   "ON " +
					   		"vt.INITIALER = wage.GSW_EMPLOYEE_ID AND wage.WM_CONTRACT_ID is not null " +
					   "WHERE " +
					   		"DATO = trunc(sysdate) AND (wage.WM_END_DATE is null or wage.WM_END_DATE > sysdate) AND LENGTH(vt.INITIALER) < 5 " +
					   "ORDER BY " +
					   		"vt.INITIALER";
		
		SqlQuery agentQuery = new SqlQuery(conn, query);
		
		agentQuery.execute();
		
		while (agentQuery.next()){
			AgentSchedule agentToAdd = new AgentSchedule(agentQuery.getInteger("ID"),
										 				 agentQuery.getString("INITIALER"),
										 				 agentQuery.getString("FORNAVN"),
										 				 agentQuery.getString("EFTERNAVN"));
			
			int contractId = agentQuery.getInteger("WM_CONTRACT_ID");
			
			agentToAdd.setIsFullTime(!(contractId == 32 || contractId == 606));
			
			agents.add(agentToAdd);
		}
		
		return agents;
	}
	
	private static String[] splitStateData(String stateData){
		if (stateData == null)
			return new String[0];
		
		return stateData.split("\\r?\\n");
	}

	public static ArrayList<AgentSchedule> getWorkForceDataByAgent(String startDate,
																   String endDate, 
																   KSDriftConnection conn) throws DopeDBException, 
																				 				  DopeResultSetException {
		ArrayList<AgentSchedule> agents = WorkForceData.getEmployedAgents(conn);
						
		String query = "SELECT " +
							"days.WM_DATE AS ScheduleDate, " +
					   		"days.WM_DAY_TYPE AS DayType, " +
							"days.WM_EXCEPTION_TYPE_ID AS ExceptionType, " +
							"days.WM_SHIFT_START_DATETIME AS ShiftStart, " +
							"days.WM_SHIFT_END_DATETIME AS ShiftEnd, " +
							"days.WM_PAID_MINUTES AS PaidMinutes, " +
							"days.WM_WORK_MINUTES AS WorkMinutes, " +
							"days.WM_SCH_STATE_DATA AS StateData, " +
							"age.GSW_EMPLOYEE_ID AS Initials " +
					   "FROM " +
					   		"GENESYS.WMV_SCH_NEW_DAYS@WFCM.NYKREDITNET.NET days " +
					   "INNER JOIN " +
					   		"GENESYS.WM_AGENTS@WFCM.NYKREDITNET.NET age " +
					   "ON " +
					   		"days.GSW_AGENT_ID = age.GSW_AGENT_ID " +
					   "WHERE " +
					   		"days.WM_DATE BETWEEN TO_DATE('"+startDate+"','YYYY-MM-DD') AND TO_DATE('"+endDate+"','YYYY-MM-DD') AND  " +
					   		"age.WM_END_DATE is null " +
					   "ORDER BY " +
					   		"days.WM_DATE DESC, " +
					   		"age.GSW_EMPLOYEE_ID ASC";
		
		SqlQuery workForceQuery = new SqlQuery(conn, query);
		workForceQuery.execute();
		
		while (workForceQuery.next()){
			AgentSchedule agentOfDay = null;
			
			String initials = workForceQuery.getString("Initials");
			
			for (AgentSchedule a : agents)
				if (a.getInitials().equals(initials)){
					agentOfDay = a;
					break;
				}
			
			if (agentOfDay != null){
				LocalDate scheduleDate = workForceQuery.getLocalDate("ScheduleDate");
				int dayType = workForceQuery.getInteger("DayType");
				int fullDayExceptiontype = workForceQuery.getInteger("ExceptionType");
				DateTime scheduledStart = workForceQuery.getDateTime("ShiftStart");
				DateTime scheduledEnd = workForceQuery.getDateTime("ShiftEnd");			
				int paidMinutes = workForceQuery.getInteger("PaidMinutes");
				int workMinutes = workForceQuery.getInteger("workMinutes");
				String stateData[] = WorkForceData.splitStateData(workForceQuery.getString("StateData"));
				
				try {
					agentOfDay.getWorkdays().add(new WorkDay(scheduleDate, 
															 (scheduledStart == null || scheduledEnd == null ? null : new Interval(scheduledStart, scheduledEnd)),
															 workMinutes, 
															 paidMinutes,
															 dayType,
															 fullDayExceptiontype,
															 stateData));
				    
//			    	if (dayType == 1){
//			    		workDayToAdd.addWorkdayException(new ExceptionInWorkDay(null, 1, 2));
//			    	} else if (dayType == 2){
//			    		workDayToAdd.addWorkdayException(new ExceptionInWorkDay(null, 0, 2));
//			    	} else if (dayType == 3){
//			    		workDayToAdd.addWorkdayException(new ExceptionInWorkDay(null, fullDayExceptiontype, 3));
//			    	} else if (dayType == 4){
//			    		if (stateData != null)
//					    	for (String d : stateData)
//					    		workDayToAdd.addWorkdayException(new ExceptionInWorkDay(d));				    			
//			    	} else if (dayType == 5){
//			    		workDayToAdd.addWorkdayException(new ExceptionInWorkDay(null, 1, 2));
//			    	}
//			    	
//			    	agentOfDay.getWorkdays().add(workDayToAdd);
			    
				    
				} catch (TimeOutOfBoundsException e) {
					e.printStackTrace();
				}
			}
		}
		
		return agents;
	}
	
}
