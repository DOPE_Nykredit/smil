package com.nykredit.kundeservice.util;

public class TimeOutOfBoundsException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public TimeOutOfBoundsException(String message){
		super(message);
	}
}