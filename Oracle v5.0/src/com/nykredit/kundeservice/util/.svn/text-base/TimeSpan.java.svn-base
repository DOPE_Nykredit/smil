package com.nykredit.kundeservice.util;
public class TimeSpan {
	
	private TimeOfDay start;
	private TimeOfDay end;
	
	public TimeOfDay getStart(){
		return this.start;
	}
	public TimeOfDay getEnd(){
		return this.end;
	}
	
	public double getDurationHours(){
		if (this.start == null || this.end == null)
			return 0;
		
		TimeOfDay length = this.end.clone();
		
		try {
			length.subtractTime(this.start);
		} catch (TimeOutOfBoundsException e) {
			// Never thrown since start and end are validated in constructor.
		}
		
		return length.getTimeAsDecimal();
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!(obj instanceof TimeSpan)) return false;
		return (this.start.equals(((TimeSpan)obj).getStart()) && this.start.equals(((TimeSpan)obj).getEnd()));
	}
	public int getDurationMinutes(){
		if (this.start == null || this.end == null)
			return 0;
		
		return (this.end.getTimeAsMinutes() - this.start.getTimeAsMinutes());
	}
	
	public TimeSpan(TimeOfDay start, TimeOfDay end) throws TimeOutOfBoundsException{
		if (start == null)
			throw new IllegalArgumentException("Start of TimeSpan cannot be null");
		
		if (end == null)
			throw new IllegalArgumentException("End of TimeSpan cannot be null");
		
		if (start.after(end))
			throw new TimeOutOfBoundsException("Start of TimeSpan cannot be after end.");
		
		this.start = start;
		this.end = end;
	}
	
	public boolean containsTime(TimeOfDay time){
		if (this.start == null || this.end == null)
			return false;
		
		return ((this.start.equals(time) || this.start.before(time)) && (this.end.equals(time) || this.end.after(time)));
	}
	public boolean covers(TimeSpan span){
		return (this.start.getTimeAsDecimal() <= span.getStart().getTimeAsDecimal() && this.end.getTimeAsDecimal() >= span.getEnd().getTimeAsDecimal());
	}
	public boolean partiallyCovers(TimeSpan span){
		if (span == null)
			return false;
		
		if (this.start.after(span.getStart()) & this.start.before(span.getEnd()))
			return true;
		else if (this.end.after(span.getStart()) & this.end.before(span.getEnd()))
			return true;
		else if ((this.start.before(span.getStart()) | this.start.equals(span.getStart())) && (this.end.after(span.getEnd()) | this.end.equals(span.getEnd())))
			return true;
		else if (this.start.after(span.getStart()) && this.end.before(span.getEnd()))
			return true;
		else
			return false;
	}
	@Override
	public String toString() {
		return (this.start.toString() + " - " + this.end.toString());
	}
}