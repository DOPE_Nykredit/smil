package com.nykredit.kundeservice.data.sql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import com.nykredit.kundeservice.data.DopeDBConnection;
import com.nykredit.kundeservice.data.DopeDBException;
import com.nykredit.kundeservice.util.TimeOfDay;
import com.nykredit.kundeservice.util.TimeOutOfBoundsException;

public class SqlQuery extends SqlStatement {

	private ResultSet results;
	
	public ResultSet getResults(){
		return this.results;
	}
	
	public SqlQuery(DopeDBConnection conn, String query) {
		super(conn, query);
	}
	
	public synchronized void execute() throws DopeDBException{
		if(super.conn.isClosed())
			super.conn.openConnection();
		
		this.results = conn.executeQuery(super.sql);
	}
	
	public boolean next() throws DopeResultSetException {
		try {
			return this.results.next();
		} catch (SQLException e) {
			throw new DopeResultSetException(e, "Cannot move to next in resultset.", "Fejl ved indlæsning af data fra database.");
		}
	}
	
	private int getColumnIndex(String columnLabel) throws DopeResultSetException {
		try {
			return this.results.findColumn(columnLabel);
		} catch (SQLException e) {
			throw new DopeResultSetException(e, "ColumnLabel could not be found in resultSet. ColumnLabel: " + columnLabel, "Fejl ved indlæsning af data fra database. Angivet kollonnenavn kunne ikke findes.");
		}
	}
	private void validateColumnIndex(int columnIndex) throws DopeResultSetException {
		if (this.results == null)
			throw new DopeResultSetException("Query not executed.", "Forespørgsel ikke eksekveret.");
		
		try {
			if (this.results.getMetaData().getColumnCount() < columnIndex)
				throw new DopeResultSetException("ColumnIndex out of bounds.", "Fejl ved indlæsning af data fra database. Forespurgte kollonne er over grænsen.");
		} catch (SQLException e) {
			throw new DopeResultSetException(e, "Error retrieving columnCount from resultset.", "Fejl ved indlæsning af data fra database.");
		}
	}
	
	public Object getObject(int columnIndex) throws DopeResultSetException {		
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getObject(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException(e, "Error retrieving columnValue from resultset", "Fejl ved indlæsning af data fra database. Kunne ikke indlæse objekt.");
		}
	}
	
	public String getString(String columnLabel) throws DopeResultSetException {
		return this.getString(this.getColumnIndex(columnLabel));
	}
	public String getString(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getString(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex + ". Type is not of String.", "Fejl ved indlæsning af data fra database. Forkert datatype.");
		}
	}
	
	public Integer getInteger(String columnLabel) throws DopeResultSetException {
		return	this.getInteger(this.getColumnIndex(columnLabel));
	}
	public Integer getInteger(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getInt(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indlæsning af data fra database. Forkert datatype.");
		}			
	}
	
	public Double getDouble(String columnLabel) throws DopeResultSetException {
		return this.getDouble(this.getColumnIndex(columnLabel));
	}
	public Double getDouble(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getDouble(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indlæsning af data fra database. Forkert datatype.");
		}
	}

	public Float getFloat(String columnLabel) throws DopeResultSetException {
		return this.getFloat(this.getColumnIndex(columnLabel));
	}
	public Float getFloat(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getFloat(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indlæsning af data fra database. Forkert datatype.");
		}
	}
	
	public Boolean getBoolean(String columnLabel) throws DopeResultSetException {
		return this.getBoolean(this.getColumnIndex(columnLabel));
	}
	public Boolean getBoolean(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getBoolean(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indlæsning af data fra database. Forkert datatype.");
		}
	}

	public Byte getByte(String columnLabel) throws DopeResultSetException {
		return this.getByte(this.getColumnIndex(columnLabel));
	}
	public Byte getByte(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getByte(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indlæsning af data fra database. Forkert datatype.");
		}
	}

	public Date getDate(String columnLabel) throws DopeResultSetException {
		return this.getDate(this.getColumnIndex(columnLabel));
	}
	public Date getDate(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getDate(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indlæsning af data fra database. Forkert datatype.");
		}
	}
	
	public Date getDateAndTime(String columnLabel) throws DopeResultSetException {
		return this.getDateAndTime(this.getColumnIndex(columnLabel));
	}
	public Date getDateAndTime(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getTimestamp(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indlæsning af data fra database. Forkert datatype.");
		}
	}
	
	public Date getTime(String columnLabel) throws DopeResultSetException {
		return this.getTime(this.getColumnIndex(columnLabel));
	}
	public Date getTime(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			return this.results.getTime(columnIndex);
		} catch (SQLException e) {
			throw new DopeResultSetException("Data type mismatch of columnIndex : " + columnIndex, "Fejl ved indlæsning af data fra database. Forkert datatype.");
		}
	}
	
	public Calendar getCalendar(String columnLabel) throws DopeResultSetException {
		return this.getCalendar(this.getColumnIndex(columnLabel));
	}
	public Calendar getCalendar(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		Calendar c = Calendar.getInstance();
		c.setTime(this.getDateAndTime(columnIndex));
		return c;
	}
	
	public DateTime getDateTime(String columnLabel) throws DopeResultSetException {
		return this.getDateTime(this.getColumnIndex(columnLabel));
	}
	public DateTime getDateTime(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);

		Date dateTime = this.getDateAndTime(columnIndex);
		
		if(dateTime != null)
			return new DateTime(dateTime);
		else
			return null;
	}
	
	public LocalDate getLocalDate(String columnLabel) throws DopeResultSetException {
		return this.getLocalDate(this.getColumnIndex(columnLabel));
	}
	public LocalDate getLocalDate(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		Date date = this.getDate(columnIndex);
		
		if(date != null)
			return (LocalDate.fromDateFields(date));
		else 
			return null;
	}
	
	public LocalTime getLocalTime(String columnLabel) throws DopeResultSetException {
		return this.getLocaltime(this.getColumnIndex(columnLabel));
	}
	public LocalTime getLocaltime(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		Date date = this.getDate(columnIndex);
		
		if(date != null)
			return LocalTime.fromDateFields(date);
		else
			return null;
	}
	
	public TimeOfDay getTimeOfDay(String columnLabel) throws DopeResultSetException {
		return this.getTimeOfDay(this.getColumnIndex(columnLabel));
	}
	public TimeOfDay getTimeOfDay(int columnIndex) throws DopeResultSetException {
		this.validateColumnIndex(columnIndex);
		
		try {
			Calendar dateTime = Calendar.getInstance();
			
			Date time = this.getTime(columnIndex);
			
			if (time == null)
				return null;
			
			dateTime.setTime(time);
			return new TimeOfDay(dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE));
		} catch (TimeOutOfBoundsException e) {
			throw new DopeResultSetException(e, "Could not create TimeOfDay object from data in column: " + columnIndex, "Fejl ved indlæsning af data fra database. Forkert datatype.");
		}
	}
}