package com.nykredit.kundeservice.data;

import java.sql.ResultSet;

public interface IDopeDBConnectionListener {
	public void queryExecuted(ResultSet rs);
	public void statementExecuted();
}
