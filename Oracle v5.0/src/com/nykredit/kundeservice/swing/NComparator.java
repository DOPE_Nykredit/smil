package com.nykredit.kundeservice.swing;

import java.util.Comparator;

public class NComparator implements Comparator<Object>{
	
	private String getNumericChar(String str){
		
		String returnString = "";
		if(str.length()>0)
			if(Character.isDigit(str.charAt(0))){
				
				
				char[] chr = str.toCharArray();
				for (char c:chr){
					if(Character.isDigit(c))
						if(c == ','){
							break;
						}else{
						returnString += c;
					}
				}
			}
		return returnString;
	}
	

	@Override
	public int compare(Object o1, Object o2) {

		
    	String string1 =  o1.toString();//getNumericChar(o1.toString());
    	String string2 = o2.toString();//getNumericChar(o2.toString());
    	Double test1 = null;
    	Double test2 = null;
    	if(string1.contains(",")){
    		String temp1 = string1.substring(string1.indexOf(",")+1, string1.length());
    		
    		Integer temp1ASInt = Integer.parseInt(temp1);
    		double t = 0;
    		if(temp1ASInt > 10){
    			t = (double)temp1ASInt/100;
    		}else{
    			t = (double)temp1ASInt/10;
    		}
    		Integer i = Integer.parseInt(string1.substring(0,string1.indexOf(",")));
    		double iResult = i + t;
        	test1 = iResult;
    	}else{
    		test1 = (double) Integer.parseInt(o1.toString());
    	}
    	if(string2.contains(",")){
		String temp2 = string2.substring(string2.indexOf(",")+1, string2.length());
    		
    		Integer temp2ASInt = Integer.parseInt(temp2);
    		double t2= 0;
    		if(temp2ASInt > 10){
    			t2 = (double)temp2ASInt/100;
    		}else{
    			t2 = (double)temp2ASInt/10;
    		}
    		Integer i2 = Integer.parseInt(string2.substring(0,string2.indexOf(",")));
    		double iResult2 = i2 + t2;
    		test2 = iResult2;    		
    	}else{
    		test2 = (double) Integer.parseInt(o2.toString());
    	}
    	
    	
        return test1.compareTo(test2);
	}

}
