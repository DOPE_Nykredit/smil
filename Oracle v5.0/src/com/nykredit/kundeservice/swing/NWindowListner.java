package com.nykredit.kundeservice.swing;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Calendar;

import javax.swing.JOptionPane;

public class NWindowListner implements WindowListener {

	private NFrame parent;
	
	public NWindowListner(NFrame parent){
		this.parent = parent;
	}
	
	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowClosing(WindowEvent e) {
		Object[] options = {"Ja", "Nej"};
		
		String msg = "Er du sikker p� du vil lukke programmet?";
		
		if(System.getenv("USERNAME").equalsIgnoreCase("CKOH")){
			Calendar random = Calendar.getInstance();
			int seconds = random.get(Calendar.SECOND);
				
			if(seconds % 2 == 1)
				msg = "Er du sikker p� at du ikke vil lukke programmet";
		 		
			int confirmed = JOptionPane.showOptionDialog(parent,
														 msg,
														 parent.getTitle(),
														 JOptionPane.YES_NO_OPTION,
														 JOptionPane.QUESTION_MESSAGE,
														 null,     //do not use a custom Icon
														 options,  //the titles of buttons
														 options[0]); //default button title
			
			if(seconds % 2 == confirmed)
				System.exit(0);
		}else
			System.exit(0);
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

}
