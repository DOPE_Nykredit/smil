package com.nykredit.kundeservice.swing;
import java.awt.event.KeyEvent;

import javax.swing.JTextField;

public class IntTextField extends JTextField {
	private static final long serialVersionUID = 1L;
	final String badchars = "`~!@#$%^&*()_+=\\|\"':;?/>.<,���{[]}�� ";
	boolean Uint = true;
	int limit;
	
	public IntTextField(){
		limit = 5;
	}

	public IntTextField(int limit){
		this.limit = limit;
	}
	
	public void setMaxLength(int val) {
		limit = val;
	}
	
	public int getMaxLength() {
		return limit;
	}
	
	public void setUnsigned(boolean val) {
		Uint = val;
	}
	
	public boolean getUnsigned() {
		return Uint;
	}
	
    public void processKeyEvent(KeyEvent ev) {
        char c = ev.getKeyChar();

        
        if (
	        (ev.getKeyCode() ==  8) | // Backspace
			(ev.getKeyCode() == 127) | // Delete
			(ev.getKeyCode() == 37) | // Left arrow
			(ev.getKeyCode() == 35) | // End
			(ev.getKeyCode() == 36) | // Home
			(ev.getKeyCode() == 39)  // Right arrow
        ){
        	super.processKeyEvent(ev);
        }else {
	        if ((Character.isLetter(c) & !ev.isAltDown()) |
        		badchars.indexOf(c) > -1 | 
				getDocument().getLength() >= limit &
				(int)ev.getID() != 402 |
        		c == '-' & (getDocument().getLength() > 0 |Uint) |
        		c == '0' & getDocument().getLength() == 0) {
	        	ev.consume();
	        } else super.processKeyEvent(ev);
        }
    }
}