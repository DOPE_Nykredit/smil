package com.nykredit.kundeservice.swing;

import java.awt.image.BufferedImage;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class NFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private URL url = this.getClass().getClassLoader().getResource("com/nykredit/kundeservice/images/NykreditLogo.jpg");
	
	public NFrame(boolean exitDialog){
		initialize(exitDialog);
	}
	
	public NFrame(){
		initialize(false);
	}
	
	private void initialize(boolean exitDialog){
		BufferedImage logo = null;
		try {
			logo = ImageIO.read(url);
			this.setIconImage(logo);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,e,"Fejl",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		
		if(exitDialog){
			this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
			this.addWindowListener(new NWindowListner(this));	
		}else{
			this.setDefaultCloseOperation(EXIT_ON_CLOSE);	
		}
	}

}
