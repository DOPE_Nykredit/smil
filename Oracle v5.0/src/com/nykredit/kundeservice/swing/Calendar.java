package com.nykredit.kundeservice.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.MouseInfo;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.nykredit.kundeservice.util.Date;
import com.nykredit.kundeservice.util.SF;



public class Calendar extends NDialog {
	private static final long serialVersionUID = 1L;
	
	private boolean HBS = false;
	private Date dato = new Date();
	private JTextField year = null;
	private JComboBox month = null;
	private JLabel[] day = new JLabel[56];
	private boolean[] days = new boolean[7];
	private int Y,M,D;

/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Class Constructor XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */

	public Calendar() {

		days[0] = true;
		days[1] = true;
		days[2] = true;
		days[3] = true;
		days[4] = true;
		days[5] = true;
		days[6] = true;
		
		setDato();
		setFrame();
	}
	
	public Calendar(String date) {

		days[0] = true;
		days[1] = true;
		days[2] = true;
		days[3] = true;
		days[4] = true;
		days[5] = true;
		days[6] = true;
		
		dato.override(date);
		setDato();
		setFrame();
	}
	
	public Calendar(String date, boolean ma, boolean ti, boolean on, boolean to, boolean fr, boolean l�, boolean s�) {
		days[0] = ma;
		days[1] = ti;
		days[2] = on;
		days[3] = to;
		days[4] = fr;
		days[5] = l�;
		days[6] = s�;

		dato.override(date);
		setDato();
		setFrame();

	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Private Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	
	private void setFrame(){
		this.setSize(243, 157);
		this.setTitle("Kalender");
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocation(MouseInfo.getPointerInfo().getLocation().x, MouseInfo.getPointerInfo().getLocation().y);
		this.setContentPane(getJContentPane());
		this.setModal(true);
		this.setModalExclusionType( ModalExclusionType.NO_EXCLUDE );
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				dato.set('Y',Y);
				dato.set('M',M);
				dato.set('D',D);
				Calendar.this.dispose();
			}
		});
		this.setAlwaysOnTop(true);
		this.setVisible(true);
	}
	
	
	
	private void setDato() {
		Y = dato.getInfo('Y');
		M = dato.getInfo('M');
		D = dato.getInfo('D');
	}
	private JPanel getJContentPane() {
		JPanel Pane = new JPanel();
		Pane.setLayout(new BorderLayout());
		Pane.add(getJDesktopPane(), BorderLayout.CENTER);
		return Pane;
	}
	
	private JDesktopPane getJDesktopPane() {
		JDesktopPane mainPane = new JDesktopPane();
		int x, y, w;
		for (int i = 0; i < 56; i++) {
			x = -3 + (i % 8) * 30;
			y = 19 + (i / 8) * 15;
			w = 30;
			if (i % 8 == 0) {
				x = 1;
				w = 26;
			}
			day[i] = new JLabel();
			mainPane.add(getLabel(day[i],x,y,w), null);
		}
		mainPane.setBackground(new Color(122,138,153));
		setLabel(day[1],"ma",0);
		setLabel(day[2],"ti",0);
		setLabel(day[3],"on",0);
		setLabel(day[4],"to",0);
		setLabel(day[5],"fr",0);
		setLabel(day[6],"l�",0);
		setLabel(day[7],"s�",0);
		mainPane.add(getMonth(), null);
		mainPane.add(getYear(), null);
		mainPane.add(getButton(0,0,"\u25B2",new SF(){public void func() {But(true, 1);}}), null);
		mainPane.add(getButton(0,9,"\u25BC",new SF(){public void func() {But(true, -1);}}), null);
		mainPane.add(getButton(197,0,"\u25B2",new SF(){public void func() {But(false, 1);}}), null);
		mainPane.add(getButton(197,9,"\u25BC",new SF(){public void func() {But(false, -1);}}), null);
		update();
		return mainPane;
	}
	
	private void update() {
		year.setText(Integer.toString(dato.getInfo('Y')));
		dato.trunc('M');
		dato.trunc('W');
		String text = "";
		int k = 0;
		for (int i = 8; i < 56; i++) {
			text = "";
			k = 0;
			if (! (i > 40 && dato.getInfo('D') < 15)) {
				if (i % 8 == 0) text = Integer.toString(dato.getInfo('W'));
				else {
					if (! (i < 18 && dato.getInfo('D') > 15)) {
						text = Integer.toString(dato.getInfo('D'));
						k = dato.getInfo('D');
					}
					dato.edit('D', 1);
				}
			}
			setLabel(day[i], text, k);
		}
		dato.edit('M', -1);
	}
	
	private void But(boolean MY, int i) {
		if (MY) dato.edit('M', i);
		else	dato.edit('Y', i);
		month.setSelectedIndex(dato.getInfo('M'));
	}
	
	private void Lab(int i) {
		if (i != 0) {
			dato.set('D', i);
			HBS = true;
			Calendar.this.dispose();
		}
	}

	private JButton getButton(int x, int y, String Text, final SF sf) {
		JButton But = new JButton();
		But.setText(Text);
		But.setFocusPainted(false);
		But.setFont(new Font("Dialog", Font.PLAIN, 6));
		But.setBounds(new Rectangle(x, y, 40, 10));
		But.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				sf.func();
			}
		});
		return But;
	}
	
	private JLabel getLabel(JLabel Lab, int x, int y,int w) {
		Lab.setHorizontalAlignment(JTextField.CENTER);
		if (x == 1 || y == 19)
			Lab.setBackground(new Color(210,228,238));
		else
			Lab.setBackground(new Color(240,245,250));
		Lab.setOpaque(true);
		Lab.setBounds(new Rectangle(x, y, w, 15));
		Lab.setText("");
		return Lab;
	}
	
	private void setLabel(JLabel Lab, String Text, final int i) {
		final boolean active = dayActiv( Lab.getX() );
		if (Lab.getX() != 1 && Lab.getY() != 19) {
			if (active) Lab.setForeground(new Color(0,0,0));
			else Lab.setForeground(new Color(255,0,0));
			if ("".contentEquals(Text))
				Lab.setBackground(new Color(240,245,250));
			else
				Lab.setBackground(new Color(220,225,235));
			try {if (dato.getInfo('M') == M && dato.getInfo('Y') == Y && Integer.parseInt(Text) == D)
					Lab.setBackground(new Color(255,235,235));
			} catch (Exception e) {}
		}
		Lab.setText(Text);
		Lab.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if ( active ) Lab(i);
			}
		});
	}

	private JComboBox getMonth() {
		month = new JComboBox();
		month.setBounds(new Rectangle(39, 0, 90, 19));
		month.addItem("januar");
		month.addItem("februar");
		month.addItem("marts");
		month.addItem("april");
		month.addItem("maj");
		month.addItem("juni");
		month.addItem("juli");
		month.addItem("august");
		month.addItem("september");
		month.addItem("oktober");
		month.addItem("november");
		month.addItem("december");
		month.setSelectedIndex(M);
		month.setMaximumRowCount(12);
		month.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dato.set('M', month.getSelectedIndex());
				update();
			}
		});
		return month;
	}

	private JTextField getYear() {
		year = new JTextField();
		year.setBounds(new Rectangle(129, 1, 68, 17));
		year.setText(Integer.toString(Y));
		year.setEditable(false);
		year.setFont(new Font("Dialog", Font.BOLD, 12));
		year.setHorizontalAlignment(JTextField.CENTER);
		return year;
	}
	
	private boolean dayActiv(int x) {
		if (x < 10) return true;
		x = (x+3) / 30;
		return days[x-1];
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Public Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */	
	public String getDate() {
		return dato.ToString();
	}
	public String getOracleDate() {
		return dato.ToString("yyyy-MM-dd");
	}
	public boolean HasBinSelected() {
		return HBS;
	}
}