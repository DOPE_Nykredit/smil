package com.nykredit.kundeservice.baseTypes;

public class Agent {

	private int agentId;
	private String initials;
	private String firstName;
	private String lastName;
	
	public int getAgentId(){
		return this.agentId;
	}
	public String getInitials(){
		return this.initials;
	}
	public String getFirstName(){
		return this.firstName;
	}
	public String getLastName(){
		return this.lastName;
	}
	public String getFullName(){
		return this.firstName + " " + this.lastName;
	}
	
	public Agent(int agentId, String initials, String firstName, String lastName) {
		this.agentId = agentId;
		this.initials = initials;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!(obj instanceof Agent)) return false;
		return ((Agent) obj).getInitials().equalsIgnoreCase(this.initials);
	}
	@Override
	public int hashCode() {
		return (this.agentId * 231 + this.initials.hashCode() * 401);
	}
}