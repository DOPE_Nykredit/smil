package com.nykredit.kundeservice.smil;

import com.nykredit.kundeservice.util.Formatter;

public class OverallPerformanceColumns {
	
	private int incommingCalls;
	private int internalCalls;
	private double answeredCallsPercentage;
	private int closedActivities;
	private Formatter bf = new Formatter();
	
	public OverallPerformanceColumns(int incommingCalls,int internalCalls,double answeredCallsPercentage,int closedActivities){
		this.incommingCalls = incommingCalls;
		this.internalCalls = internalCalls;
		this.answeredCallsPercentage = answeredCallsPercentage;
		this.closedActivities = closedActivities;		
	}

	public String getIncommingCalls() {
		return bf.KiloDotFill(incommingCalls, true);
	}
	public void setIncommingCalls(int incommingCalls) {
		this.incommingCalls = incommingCalls;
	}

	public String getInternalCalls() {
		return bf.KiloDotFill(internalCalls, true);
	}

	public void setInternalCalls(int internalCalls) {
		this.internalCalls = internalCalls;
	}

	public String getAnsweredCallsPercentage() {
		return bf.toProcent(answeredCallsPercentage, true);
	}

	public void setAnsweredCallsPercentage(double answeredCallsPercentage) {
		this.answeredCallsPercentage = answeredCallsPercentage;
	}

	public String getClosedActivities() {
		return bf.KiloDotFill(closedActivities, true);
	}

	public void setClosedActivities(int closedActivities) {
		this.closedActivities = closedActivities;
	}

}
