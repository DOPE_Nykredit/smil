package com.nykredit.kundeservice.smil;

import java.util.ArrayList;

public class DataTable {

	private Data aOracle;

	public DataTable(Data aOracle){
		this.aOracle = aOracle;
	}

	//***********BEREGNER GENNEMSNIT PÅ FELTERNE I DE ENKELTE KOLONNER TAL OG TIDER*********
	public void calcGns(DataTableColumns dtc,int noAgents){
		//*******************************************KALD****************************************
		dtc.setCbr(dtc.getCbr() /noAgents);
		dtc.setConsult(dtc.getConsult() /noAgents);
		dtc.setFirstCall(dtc.getFirstCall() /noAgents);
		dtc.setIndKald(dtc.getIndgåendeKald()/noAgents);
		dtc.setKaldIalt(dtc.getKaldIalt() /noAgents);
		dtc.setUdKald(dtc.getUdKald() /noAgents);
		//****************************************CTI********************************************
		dtc.setFrokost(dtc.getFrokost() /noAgents);
		dtc.setPause(dtc.getPause() /noAgents);
		dtc.setSpærret(dtc.getSpærret() /noAgents);
		dtc.setKorrigeret(dtc.getKorrigeret()/noAgents);
		dtc.setKlar(dtc.getKlar() /noAgents);
		dtc.setTaler(dtc.getTaler() /noAgents);
		dtc.setØvrigTid(dtc.getØvrigTid() /noAgents);
		dtc.setWebdesk(dtc.getWebdesk() /noAgents);
		dtc.setEmail(dtc.getEmail() /noAgents);
		dtc.setEfterbeh(dtc.getEfterbehandling() /noAgents);
		dtc.setMøde(dtc.getMøde() /noAgents);
		dtc.setUddannelse(dtc.getUddannelse() /noAgents);
		dtc.setKollegahj(dtc.getKollegahj() /noAgents);
		//******************************SALG********************************************************		
		dtc.setForsikringspræmie(dtc.getForsikringspræmie() / noAgents);
		dtc.setHenvisninger(dtc.getHenvisninger() / noAgents);
		dtc.setGennemførtSalg(dtc.getGennemførtSalg() / noAgents);
		dtc.setMersalg(dtc.getMersalg() / noAgents);
		dtc.setKampagnePotentialer(dtc.getKampagnePotentialer()/ noAgents);
		//*****************************AKTIVITETER*************************************************
		dtc.setAktiviteterAfsluttet(dtc.getAktiviteterAfsluttet()/ noAgents);
		dtc.setIndenforTidsfrist(dtc.getIndenforTidsfrist() / noAgents);
		dtc.setOverskredne(dtc.getOverskredne() / noAgents);
		dtc.setEmailIndgående(dtc.getEmailIndgående() / noAgents);
		dtc.setTlfBesk(dtc.getTlfBeskeder() / noAgents);
		dtc.setSupportOpgaverAfsluttet(dtc.getSupportOpgaverAfsluttet() / noAgents);
		dtc.setAktiviteterOprettet(dtc.getAktiviteterOprettet() / noAgents);
		dtc.setCallbacks(dtc.getCallbacks() / noAgents);
		//**************************BEREGNINGER***************************************************		
		dtc.setPlanlagttid(dtc.getPlanlagttid() / noAgents);
		dtc.setPlanlagtKundetid(dtc.getPlanlagtKundetid() / noAgents);
		dtc.setTilbudteSelvbetjeningsprodukter(dtc.getTilbudteSelvbetjeningsprodukter() / noAgents);
		dtc.setOpfølgningsserviceAfsluttet(dtc.getOpfølgningsserviceAfsluttet() / noAgents);
		dtc.setIndgåendeKaldDialog(dtc.getIndgåendeKaldDialog() / noAgents);
		//***************************EKSPEDITIONSSERVICE*******************************************
		dtc.setOpfølgningsserviceAfsluttet(dtc.getOpfølgningsserviceAfsluttet() / noAgents);
		dtc.setIndgåendeKaldDialog(dtc.getIndgåendeKaldDialog() / noAgents);
		dtc.setTilbudteSelvbetjeningsprodukter(dtc.getTilbudteSelvbetjeningsprodukter() / noAgents);
		dtc.setTabtSalg(dtc.getTabtSalg() / noAgents);
		//***************************HISTORIK*******************************************
		dtc.setHenvisningerQ12013(dtc.getHenvisningerQ12013() / noAgents);
		dtc.setGennemførtSalgQ12013(dtc.getHenvisningerQ12013() / noAgents);
	}

	//********************Sum af totaler på tal og tid IKKE Procenter**************************
	public void sumData(DataTableColumns dtc, DataTableColumns a){
		//*******************************************KALD****************************************
		dtc.setCbr(dtc.getCbr() + a.getCbr());
		dtc.setConsult(dtc.getConsult() + a.getConsult());
		dtc.setFirstCall(dtc.getFirstCall() + a.getFirstCall());
		dtc.setIndKald(dtc.getIndgåendeKald() + a.getIndgåendeKald());
		dtc.setKaldIalt(dtc.getKaldIalt() + a.getKaldIalt());
		dtc.setUdKald(dtc.getUdKald() + a.getUdKald());
		dtc.setWebdeskKald(dtc.getWebdeskKald() + a.getWebdeskKald());
		//****************************************CTI********************************************		
		dtc.setAndreOpg(dtc.getAndreOpg() + a.getAndreOpg());
		dtc.setEfterbeh(dtc.getEfterbehandling() + a.getEfterbehandling());
		dtc.setEmail(dtc.getEmail() + a.getEmail());
		dtc.setFrokost(dtc.getFrokost() + a.getFrokost());
		dtc.setKlar(dtc.getKlar() + a.getKlar());
		dtc.setKollegahj(dtc.getKollegahj() + a.getKollegahj());
		dtc.setKorrigeret(dtc.getKorrigeret() + a.getKorrigeret());
		dtc.setMøde(dtc.getMøde() + a.getMøde());
		dtc.setPause(dtc.getPause() + a.getPause());
		dtc.setSpærret(dtc.getSpærret() + a.getSpærret());
		dtc.setTaler(dtc.getTaler() + a.getTaler());
		dtc.setUddannelse(dtc.getUddannelse() + a.getUddannelse());
		dtc.setWebdesk(dtc.getWebdesk() + a.getWebdesk());
		dtc.setØvrigTid(dtc.getØvrigTid() + a.getØvrigTid());
		//******************************SALG********************************************************
		dtc.setForsikringspræmie(dtc.getForsikringspræmie() + a.getForsikringspræmie());
		dtc.setHenvisninger(dtc.getHenvisninger() + a.getHenvisninger());
		dtc.setGennemførtSalg(dtc.getGennemførtSalg() + a.getGennemførtSalg());
		dtc.setMersalg(dtc.getMersalg() + a.getMersalg());
		dtc.setKampagnePotentialer(dtc.getKampagnePotentialer() + a.getKampagnePotentialer());
		//*****************************AKTIVITETER****ALLE TOTALER********************************
		dtc.setAktiviteterAfsluttet(dtc.getAktiviteterAfsluttet() + a.getAktiviteterAfsluttet());
		dtc.setAktiviteterOprettet(dtc.getAktiviteterOprettet() + a.getAktiviteterOprettet());
		dtc.setTlfBesk(dtc.getTlfBeskeder() + a.getTlfBeskeder());
		dtc.setIndenforTidsfrist(dtc.getIndenforTidsfrist() + a.getIndenforTidsfrist());
		dtc.setOverskredne(dtc.getOverskredne() + a.getOverskredne());
		dtc.setEmailIndgående(dtc.getEmailIndgående() + a.getEmailIndgående());
		dtc.setSupportOpgaverAfsluttet(dtc.getSupportOpgaverAfsluttet() + a.getSupportOpgaverAfsluttet());
		dtc.setSupportIndenforFrist(dtc.getSupportIndenforFrist() + a.getSupportIndenforFrist());
		dtc.setCallbacks(dtc.getCallbacks() + a.getCallbacks());
		dtc.setMeetingsBooked(dtc.getMeetingsBookedDouble() + a.getMeetingsBookedDouble());
		

		//**************************BEREGNINGER***************************************************			
		dtc.setPlanlagttid(dtc.getPlanlagttid()+ a.getPlanlagttid());								//Total
		dtc.setPlanlagtKundetid(dtc.getPlanlagtKundetid()+ a.getPlanlagtKundetid());
		dtc.setWebdeskTalerTid(dtc.getWebdeskTalerTid()+a.getWebdeskTalerTid());
		//***************************EKSPEDITIONSSERVICE*******************************************
		dtc.setOpfølgningsserviceAfsluttet(dtc.getOpfølgningsserviceAfsluttet() + a.getOpfølgningsserviceAfsluttet());
		dtc.setIndgåendeKaldDialog(dtc.getIndgåendeKaldDialog() + a.getIndgåendeKaldDialog());
		dtc.setTilbudteSelvbetjeningsprodukter(dtc.getTilbudteSelvbetjeningsprodukter() + a.getTilbudteSelvbetjeningsprodukter());
		dtc.setTabtSalg(dtc.getTabtSalg() + a.getTabtSalg());
		//***************************HISTORIK*******************************************
		dtc.setHenvisningerQ12013(dtc.getHenvisningerQ12013() + a.getHenvisningerQ12013());
		dtc.setGennemførtSalgQ12013(dtc.getGennemførtSalgQ12013() + a.getGennemførtSalgQ12013());
	}

	public DataTableColumns getGns(ArrayList<DataTableColumns> agents){
		int i = 0;

		for(DataTableColumns a : agents)
			if(a.getKorrigeret() > 0)
				i++;

		DataTableColumns total = new DataTableColumns();

		for(DataTableColumns a: agents)
			sumData(total, a);

		if (i!=0)
			calcGns(total,i);

		return total;
	}

	public ArrayList<DataTableColumns> setDataTable(String fromDate,String toDate,ArrayList<DataTableColumns> dataTable) {
		dataTable = aOracle.getAgentCbr(fromDate, toDate, dataTable);
		dataTable = aOracle.getOprettedePotentialer(fromDate, toDate,dataTable);
		dataTable = aOracle.getOprettedeAktiviteter(fromDate, toDate, dataTable);
		dataTable = aOracle.getFærdiggørelsesgrad(fromDate, toDate, dataTable);
		dataTable = aOracle.getGennemførtePotentialer(fromDate, toDate, dataTable);
		dataTable = aOracle.getGennemførteSalg(fromDate, toDate, dataTable);
		//	dataTable = aOracle.getSupportOpgaver(fromDate, toDate, dataTable);
		dataTable = aOracle.getGennemførteSalgHistorik(fromDate, toDate, dataTable);
		dataTable = aOracle.getPlanlagtTid(fromDate, toDate, dataTable);
		dataTable = aOracle.getPlanlagtKundeTid(fromDate, toDate, dataTable);
		dataTable = aOracle.getIndgåendeKaldDialog(fromDate, toDate, dataTable);
		dataTable = aOracle.getWebdeskData(fromDate, toDate, dataTable);
		dataTable = aOracle.getAfsluttedeAktiviteter(fromDate, toDate, dataTable);
		dataTable = aOracle.getMødeService(fromDate, toDate, dataTable);
		dataTable = aOracle.getSupportOpgaver(fromDate, toDate, dataTable);
		dataTable = aOracle.getCallbacks(fromDate, toDate, dataTable);
		return dataTable;
	}
}
