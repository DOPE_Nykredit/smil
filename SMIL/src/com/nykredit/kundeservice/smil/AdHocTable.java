package com.nykredit.kundeservice.smil;

import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import com.nykredit.kundeservice.swing.NComparator;
import com.nykredit.kundeservice.swing.NTable;
// test
public class AdHocTable extends NTable {
	private static final long serialVersionUID = 1L;
	private DefaultTableModel tableModel = new DefaultTableModel();
	private ArrayList<DataTableColumns> dataTable = new ArrayList<DataTableColumns>();
	private DataTableColumns subtotal = new DataTableColumns(); 
	private DataTableColumns total = new DataTableColumns();
	private ArrayList<DataTableColumns> b1b7Stat = new ArrayList<DataTableColumns>();
	private Data aOracle = new Data();
	private DataTable dataBuilder = new DataTable(aOracle);
	private Vector<Object> rowData;
	private TableRowSorter<DefaultTableModel> sorter;
	
	public AdHocTable(Data adHocOracle) {
		setAdHocTableModel();

		//kolonne st�rrelsen p� arket "data"
		this.getTableHeader().setPreferredSize(new Dimension(this.getColumnModel().getTotalColumnWidth()+5, 50));
				
		this.getTableHeader().setFont(new Font("Arial", Font.PLAIN, 9));

		if (!EColumn.SHOWAGENTS.isSelected())
			EColumn.SHOWSUBTOTAL.getCheckBox().setEnabled(false);
		else
			EColumn.SHOWSUBTOTAL.getCheckBox().setEnabled(true);
	}

	public boolean isCellEditable(int rowIndex, int colIndex) {
		return false; // Disallow the editing of any cell
	}

	public void setAdHocTableModel() {
		tableModel = new DefaultTableModel();
		this.setModel(tableModel);
		tableModel.addColumn("Team");
		tableModel.addColumn("Initialer");

		for (EColumn c : EColumn.values())
			if (c.isSelected() & (!c.getCategory().equals(ECategory.SETTINGS)&!c.getCategory().equals(ECategory.COLORING))) {
				tableModel.addColumn(c);
			}
		/// this is a test
		setDefaultRenderer(Object.class, new AdHocTableCellRender());

		this.getColumnModel().getColumn(0).setPreferredWidth(120);
		this.getColumnModel().getColumn(1).setPreferredWidth(50);
		
		int x = this.getColumnModel().getColumnCount();

		for (int i = 2; i < x; i++) {
			this.getColumnModel().getColumn(i).setPreferredWidth(65);
		}
		
		sorter = new TableRowSorter<DefaultTableModel>(tableModel);
		this.setRowSorter(sorter);
		
		for (int i = 2; i<this.getColumnModel().getColumnCount();i++){
			sorter.setComparator(i, new NComparator());	
		}

		this.setAutoResizeMode(0);
		this.getTableHeader().setReorderingAllowed(true);
	}
	
	public void loadStatistics(String whereClause,
		String fromDate, String toDate, boolean agentSelected) {

		aOracle.SS_spy();
		b1b7Stat = new ArrayList<DataTableColumns>();
		dataTable = new ArrayList<DataTableColumns>();
		dataBuilder = new DataTable(aOracle);
		ArrayList<DataTableColumns> allData = new ArrayList<DataTableColumns>();

		dataTable = aOracle.getSelectedAgents(whereClause, fromDate, toDate, dataTable);

		for (DataTableColumns a : dataTable) {
			allData.add(new DataTableColumns(a.getInitials(), a.getTeam(),a.getTeamNavn()));
		}

		if (allData.size() != 0) {
			allData = dataBuilder.setDataTable(fromDate, toDate, allData);

			for (DataTableColumns d : allData) {
				for (DataTableColumns d1 : dataTable) {
					if (d1.getInitials().contentEquals(d.getInitials()) && 
						d1.getTeam().contentEquals(d.getTeam()))
						dataTable.set(dataTable.indexOf(d1), d);
				}
				for (DataTableColumns d1 : b1b7Stat) {
					if (d1.getInitials().contentEquals(d.getInitials()) && 
						d1.getTeam().contentEquals(d.getTeam()))
						b1b7Stat.set(b1b7Stat.indexOf(d1), d);
				}
			}
		}
		
		fillTable();
	}

	public void fillTable() {
		this.setAdHocTableModel();
		total = new DataTableColumns();
		
		if (EColumn.COLORINGE1E2.isSelected()){
			insertEkspeditionGoals();
			this.setRowHeight(0, 40);
		}
		
		if (EColumn.COLORINGE1.isSelected()){
			insertE1Goals();
			this.setRowHeight(0, 40);
		}
		
		if (EColumn.COLORINGE2.isSelected()){
			insertE2Goals();
			this.setRowHeight(0, 40);
		}
		
		if (!EColumn.SHOWAGENTS.isSelected()){
			EColumn.SHOWSUBTOTAL.setSelected(true);
			EColumn.SHOWSUBTOTAL.getCheckBox().setSelected(true);
			EColumn.SHOWSUBTOTAL.getCheckBox().setEnabled(false);
			this.removeColumn(this.getColumnModel().getColumn(1));
		}else{
			EColumn.SHOWSUBTOTAL.getCheckBox().setEnabled(true);
		}

		if (!(dataTable.size() == 0)) {

			subtotal = new DataTableColumns();
			String lastTeamName = null;

			for (DataTableColumns a : dataTable) {
				if (lastTeamName == null) {
					lastTeamName = a.getTeamNavn();
				}
				if (!(lastTeamName.contentEquals((a.getTeamNavn())))) {
					if (EColumn.SHOWSUBTOTAL.isSelected()){
						this.insertTotal(lastTeamName, "Sub total", this.subtotal);
						subtotal = new DataTableColumns();
					}
				}

				if (EColumn.SHOWAGENTS.isSelected()) {
					EColumn.insertRow(a);
					rowData = new Vector<Object>();
					rowData.add(a.getTeamNavn());
					rowData.add(a.getInitials());
					
					for (EColumn c : EColumn.values()) {
						if (c.isSelected() & (!c.getCategory().equals(ECategory.SETTINGS)&!c.getCategory().equals(ECategory.COLORING))) {
							rowData.add(c.getRowData());
							}
						}
					
					if(EColumn.HIDEEMPTYLOGIN.isSelected()){
						if (a.getKorrigeret() != 0)
							tableModel.addRow(rowData);
					}else{
						tableModel.addRow(rowData);
					}
					
					}
				lastTeamName = a.getTeamNavn();
				dataBuilder.sumData(this.subtotal, a);
				dataBuilder.sumData(this.total, a);
			}
			if (EColumn.SHOWSUBTOTAL.isSelected())
				insertTotal(lastTeamName, "Sub total", this.subtotal);
			if (EColumn.SHOWTOTAL.isSelected())
				insertTotal("Total", "", this.total);
		}
		

		if (EColumn.SHOWAVERAGE.isSelected())
			insertTotal("Gns. B2-B8", "", dataBuilder.getGns(b1b7Stat));
	}

	private void insertTotal(String team, String initialer, DataTableColumns total) {
		EColumn.insertRow(total);
		rowData = new Vector<Object>();
		rowData.add(team);
		rowData.add(initialer);
		for (EColumn c : EColumn.values()) {
			if (c.isSelected() & (!c.getCategory().equals(ECategory.SETTINGS)& !c.getCategory().equals(ECategory.COLORING))) {
				rowData.add(c.getRowData());
			}
		}
		tableModel.addRow(rowData);
	}
	
	private void insertEkspeditionGoals() {
		EColumn.getEkspeditionGoalText();
		rowData = new Vector<Object>();
		rowData.add("  M�L:");
		rowData.add("");
		for (EColumn c : EColumn.values()) {
			if (c.isSelected() & (!c.getCategory().equals(ECategory.SETTINGS) & !c.getCategory().equals(ECategory.COLORING))) {
				rowData.add(c.getRowData());
			}
		}
		tableModel.addRow(rowData);
	}
	private void insertE1Goals() {
		EColumn.getE1GoalText();
		rowData = new Vector<Object>();
		rowData.add("  M�L:");
		rowData.add("");
		for (EColumn c : EColumn.values()) {
			if (c.isSelected() & (!c.getCategory().equals(ECategory.SETTINGS) & !c.getCategory().equals(ECategory.COLORING))) {
				rowData.add(c.getRowData());
			}
		}
		tableModel.addRow(rowData);
	}
	private void insertE2Goals() {
		EColumn.getE2Text();
		rowData = new Vector<Object>();
		rowData.add("  M�L:");
		rowData.add("");
		for (EColumn c : EColumn.values()) {
			if (c.isSelected() & (!c.getCategory().equals(ECategory.SETTINGS) & !c.getCategory().equals(ECategory.COLORING))) {
				rowData.add(c.getRowData());
			}
		}
		tableModel.addRow(rowData);
	}
}
