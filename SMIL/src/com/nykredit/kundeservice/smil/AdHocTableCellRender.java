package com.nykredit.kundeservice.smil;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import com.nykredit.kundeservice.swing.NTableRenderer;

public class AdHocTableCellRender extends NTableRenderer  {
	private static final long serialVersionUID = 1L;
	
	private String header;
	private JTable table;
	private int row;
	private Component cell;
	private Object value;
	
    public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus, int row, int column){    
    	this.cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    	this.header = table.getColumnModel().getColumn(column).getHeaderValue().toString().replace("<html><p style=\"text-align: center;\">","");
    	this.value = value;
    	this.table = table;
    	this.row = row;
    	    	    	    
		if ((EColumn.SHOWAGENTS.isSelected() && column > 1) || (!EColumn.SHOWAGENTS.isSelected() && column > 0))
			this.setHorizontalAlignment(SwingConstants.CENTER);
		else
			this.setHorizontalAlignment(SwingConstants.LEFT);
		
		this.farvBlåHvisTotalEllerSubtotal();

		if (row == 0 && (EColumn.COLORINGE1E2.isSelected() ||
						 EColumn.COLORINGE1.isSelected() ||
						 EColumn.COLORINGE2.isSelected()))
			this.setFont(new Font("Arial", Font.PLAIN, 9));
		
    	if(EColumn.COLORINGKONCERN.isSelected())
    		getColoringTilgængelighed(75, 70);

    	if(EColumn.COLORINGB1B6.isSelected()){
    		getColoringTilgængelighed(62, 58);
    		getColoringKPI(8, 7.5);
    	}

    	if(EColumn.COLORING_MS.isSelected())
    		getColoringKPIWebdesk(7.5, 7);
		    		
    	if(EColumn.COLORINGNETBANK.isSelected()){
    		getColoringTilgængelighed(70, 66);
    		getColoringAktiviteterIndenforTidsfrist(95, 89);
    	}

    	if(EColumn.COLORINGFORSIKRING.isSelected()){
    		getColoringAktiviteterIndenforTidsfrist(95, 89);
    		getColoringKPI(5.0, 4.7);
    		getColoringProcentvisMersalg(15, 14.2);
    	}
    	   	
    	if(EColumn.COLORINGERHVERV.isSelected())
    		getColoringAktiviteterIndenforTidsfrist(95, 89);
    	
    	if(EColumn.COLORINGSUPPORT.isSelected()){
    		getColoringAktiviteterIndenforTidsfrist(95, 89);
    		getColoringProcentSupportIndenforTidsfrist(95, 89);    	
    		getColoringKPISupport(16, 15.1);
    	}

    	if(row > 0)
    		if(EColumn.COLORINGE1E2.isSelected()){
    			getColoringAHT(280, 295);
    			getColoringSalgsrate(12.5, 11.8);
    			getColoringTilgængelighed(64, 61);
    			getColoringHenvisningerPrLoginTime(0.15,0.09);
    		}
		
    	if(row > 0)
    		if(EColumn.COLORINGE1.isSelected()){
    			getColoringAHT(292, 278);
    			getColoringSalgsrate(11.8, 11.8);
    			getColoringTilgængelighed(67, 63);
    			getColoringHenvisningerPrLoginTime(0.09, 0.09);
    		}
		
    	if(row > 0)
    		if(EColumn.COLORINGE2.isSelected()){
    			getColoringAHT(284, 300);
    			getColoringSalgsrate(12.5, 11.8);
    			getColoringTilgængelighed(64, 60);
    			getColoringHenvisningerPrLoginTime(0.15, 0.9);
    		}
		
    	addMarking(cell,isSelected,new Color(0,0,128),80);
        
    	return cell; 
    }  

    private void farvBlåHvisTotalEllerSubtotal(){
    	try{
    		String val1 = (String)table.getValueAt(row, 0);
    		String val2 = (String)table.getValueAt(row, 1);
    		
    		if(("Sub total").contentEquals(val2) ||
		       ("Total").contentEquals(val1)) {          
    			cell.setForeground(Color.white);
		        cell.setBackground(new Color(0,0,128));
		    }
		}catch(Exception e){
			System.out.println(e.toString());
		}
    }

    private void getColoringKPI(double green, double red){
  		try{
  			double dob = Double.parseDouble(value.toString().replace(",","."));
  			if (header.contentEquals("Produktivitet")){
				if(dob>red & dob<green){
					cell.setBackground(Constants.yellow);
					cell.setForeground(Color.black);
			}else if (dob>=green){
				cell.setBackground(Constants.green);
				cell.setForeground(Color.black);
			}else{
				cell.setBackground(Constants.red);
				cell.setForeground(Color.black);
			}
		}
  		}catch(Exception e){}
    }
    private void getColoringKPISupport(double green, double red){
  		try{
  			double dob = Double.parseDouble(value.toString().replace(",","."));
  			if (header.contentEquals("Produkti-<br>vitet m.<br>supp. opg")){
				if(dob>red & dob<green){
					cell.setBackground(Constants.yellow);
					cell.setForeground(Color.black);
			}else if (dob>=green){
				cell.setBackground(Constants.green);
				cell.setForeground(Color.black);
			}else{
				cell.setBackground(Constants.red);
				cell.setForeground(Color.black);
			}
		}
  		}catch(Exception e){}
    }
    private void getColoringKPIWebdesk(double green, double red){
 		try{
  			double dob = Double.parseDouble(value.toString().replace(",","."));
  			
  			if (header.contentEquals("Produkti-<br>vitet m.<br>Webdesk")){
				if(dob>red & dob<green){
					cell.setBackground(Constants.yellow);
					cell.setForeground(Color.black);
			}else if (dob>=green){
				cell.setBackground(Constants.green);
				cell.setForeground(Color.black);
			}else{
				cell.setBackground(Constants.red);
				cell.setForeground(Color.black);
			}
		}
  		}catch(Exception e){}
    }
    
    private void getColoringAHT(int green, int red){
    	try{
    		if(header.contentEquals("AHT") && value != null){
    			String[] time = ((String)value).split(":");
    		
    			int seconds = Integer.parseInt(time[0]) * 60 + Integer.parseInt(time[1]);
    		
    			if(seconds < red & seconds > green){
    				cell.setBackground(Constants.yellow);
    				cell.setForeground(Color.black);
    			}else if (seconds <= green){
    				cell.setBackground(Constants.green);
    				cell.setForeground(Color.black);
    			}else{
    				cell.setBackground(Constants.red);
    				cell.setForeground(Color.black);
    			}
    		}
    	}catch (Exception e){
    		
    	}
    }
    
    private void getColoringSalgsrate(double green, double red){
		try{
			double val = Double.parseDouble(value.toString().replace("%",""));
			
			if (header.contentEquals("Salgsrate")){
				if(val > red & val < green){
					cell.setBackground(Constants.yellow);
					cell.setForeground(Color.black);
				}else if (val >= green){
					cell.setBackground(Constants.green);
					cell.setForeground(Color.black);
				}else{
					cell.setBackground(Constants.red);
					cell.setForeground(Color.black);
				}
			}
		}catch(Exception e){}
    }
    
    private void getColoringHenvisningerPrLoginTime(double green,double red){
  		try{
  			
  			if (this.row != 0){
  			double dob = 0;
  			try{
  			dob = Double.parseDouble(value.toString().replace(",","."));
  			}catch(Exception e){}
  			
  			if (header.contentEquals("Henv.<br>pr.<br>logintime")){
				if(dob>red & dob<green){
					cell.setBackground(Constants.yellow);
					cell.setForeground(Color.black);
			}else if (dob>=green){
				cell.setBackground(Constants.green);
				cell.setForeground(Color.black);
			}else{
				cell.setBackground(Constants.red);
				cell.setForeground(Color.black);
				}
  			}
		}
  		}catch(Exception e){

  		}
    }
    
    private void getColoringTilgængelighed(int green,int red){
		try{
			int val = Integer.parseInt(value.toString().replace("%",""));
			
			if (header.contentEquals("Tilgænge-<br>lighed")){
				if(val > red & val < green){
					cell.setBackground(Constants.yellow);
					cell.setForeground(Color.black);
				}else if (val >= green){
					cell.setBackground(Constants.green);
					cell.setForeground(Color.black);
				}else{
					cell.setBackground(Constants.red);
					cell.setForeground(Color.black);
				}
			}
		}catch(Exception e){}
    }
    
    private void getColoringAktiviteterIndenforTidsfrist(int green,int red){
		try{
			int val = Integer.parseInt(value.toString().replace("%",""));
			if (header.contentEquals("Akt.<br>ind.<br>tidsfr. %")){
				if(val>red & val<green){
						cell.setBackground(Constants.yellow);
						cell.setForeground(Color.black);
				}else if (val>=green){
					cell.setBackground(Constants.green);
					cell.setForeground(Color.black);
				}else{
					cell.setBackground(Constants.red);
					cell.setForeground(Color.black);
				}
			}
		}catch(Exception e){}
    }   
    private void getColoringProcentSupportIndenforTidsfrist(int green,int red){
		try{
			int val = Integer.parseInt(value.toString().replace("%",""));
			
			if (header.contentEquals("Supp. opg<br>indenfor<br> tidsfr. %")){
				if(val>red & val<green){
						cell.setBackground(Constants.yellow);
						cell.setForeground(Color.black);
				}else if (val>=green){
					cell.setBackground(Constants.green);
					cell.setForeground(Color.black);
				}else{
					cell.setBackground(Constants.red);
					cell.setForeground(Color.black);
				}
			}
		}catch(Exception e){}
    }   
   
    private void getColoringProcentvisMersalg(double green, double red){
		try{
			int val = Integer.parseInt(value.toString().replace("%", ""));
			
			if (header.contentEquals("Procentvis<br>mersalg")){
				if(val > red & val < green){
						cell.setBackground(Constants.yellow);
						cell.setForeground(Color.black);
				}else if (val >= green){
					cell.setBackground(Constants.green);
					cell.setForeground(Color.black);
				}else{
					cell.setBackground(Constants.red);
					cell.setForeground(Color.black);
				}
			}
		}catch(Exception e){}
    }
    
}
