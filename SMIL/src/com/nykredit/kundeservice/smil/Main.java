package com.nykredit.kundeservice.smil;

import javax.swing.*;
import java.awt.*;
import java.lang.String;
import java.text.MessageFormat;
import javax.swing.JScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.KeyEvent;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;
import com.nykredit.kundeservice.swing.KSTree;
import com.nykredit.kundeservice.swing.DateSelector;
import com.nykredit.kundeservice.swing.NFrame;
import com.nykredit.kundeservice.swing.SplashScreen;

public class Main extends NFrame {
	private static final long serialVersionUID = 1L;
	private Data aOracle;
	private KSTree tree;
	private static JComboBox formattingComboBox;
	private JButton buttonGetData, buttonPrintTable;
	private JLabel labelCopyright,label8025, labelKald, labelInterneKald, labelAktiviteter;
	private JScrollPane scrollPaneAdHocTable;
	private JScrollPane ScrollPaneKSCheckBoxTree;
	public ButtonGroup radioButtonGroup = new ButtonGroup();
	public JRadioButton radioButtonAgents = new JRadioButton("Initialer", true),radioButtonTeams = new JRadioButton("Teams", false);
	private AdHocTable adHocTable;
	private DateSelector datePicker = new DateSelector();
	private OverallPerformanceColumns overallPerformanceColumns;
	
	public static void main(String[] args) {
		SplashScreen splash = new SplashScreen(Constants.programName,Constants.programDescription);
		new Main();
		splash.dispose();
	}

	private JPanel getMainPanel(){
		JPanel p = new JPanel();
		p.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.insets = new Insets(5,5,5,5);
		c.gridy = 0;
		c.gridx = 0;
		c.gridheight = 2;
		c.anchor = GridBagConstraints.NORTHWEST;
		p.add(getControlPanel(),c);
		
		c.weightx = 0;
		c.anchor = GridBagConstraints.EAST;
		c.insets = new Insets(0,0,0,5);
		c.gridheight = 1;
		c.gridy = 0;
		c.gridx = 1;
		p.add(getCallsAndActivityPanel(),c);
		
		c.anchor = GridBagConstraints.NORTHWEST;
		c.insets = new Insets(0,0,0,5);
		c.gridy = 1;
		c.gridx = 1;
		c.weightx = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;
		p.add(getTabs(),c);
		
		c.insets = new Insets(5,5,5,10);
		c.gridy = 2;
		c.gridx = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.SOUTHEAST;
		p.add(getLabelCopyright(),c);
		p.setOpaque(false);

		return p;
	}
	
	private JPanel getCallsAndActivityPanel(){
		JPanel p = new JPanel();
		p.add(labelKald =getBorderLabel(0+"","Eksterne kald",""));
		p.add(labelInterneKald =getBorderLabel(0+"","Interne kald",""));
		p.add(label8025 =getBorderLabel(0+"% (80/25)","Besvarelses %",""));
		p.add(labelAktiviteter =getBorderLabel(0+"","Aktiviteter",""));
		return p;
	}
	
	private JPanel getControlPanel(){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridy = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		p.add(datePicker,c);
		c.gridy = 1;
		c.insets = new Insets(5,0,0,0);
		p.add(getTree(),c);
		c.gridy = 2;
		c.insets = new Insets(5,0,0,0);
		c.weightx = 1;
		p.add(getCheckBoxPanel(),c);
		c.gridy = 3;
		p.add(getButtonPanel(),c);
		return p;
	}	
	
	private JPanel getCheckBoxPanel(){
		JPanel p = new JPanel(new GridLayout(8,1));
		p.add(getMainFrameCheckBox(EColumn.SHOWAGENTS));
		p.add(getMainFrameCheckBox(EColumn.SHOWSUBTOTAL));
		p.add(getMainFrameCheckBox(EColumn.SHOWTOTAL));
		p.add(getMainFrameCheckBox(EColumn.SHOWAVERAGE));
		p.add(getMainFrameCheckBox(EColumn.HIDEEMPTYLOGIN));
		p.add(new JLabel("Farve formatering:"));	
		p.add(getFormattingComboBox());		
		return p;
	}

	
	private JCheckBox getMainFrameCheckBox(final EColumn col){
		col.getCheckBox().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(col.getCheckBox().isSelected()){col.setSelected(true);}
				else{col.setSelected(false);}
				adHocTable.fillTable();	
				aOracle.setConfiguration();
			}
		});
		return col.getCheckBox();
	}
	
	private JPanel getButtonPanel(){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridy = 0;
		c.weightx = 1;
		c.fill = GridBagConstraints.BOTH;
		p.add(getButtonHent(),c);
		c.insets = new Insets(0,0,0,0);
		c.gridy = 1;
		p.add(getButtonPrint(),c);
		return p;
	}
	
	private JTabbedPane getTabs(){
		JTabbedPane tabs = new JTabbedPane();
		tabs.addTab("Data", null, getScrollPaneAdHocTable(),"Data");
		tabs.setMnemonicAt(0, KeyEvent.VK_1);
		tabs.addTab("Ops�tning",null, getConfigurationPanel(),"Ops�tning");
		tabs.setMnemonicAt(1, KeyEvent.VK_1);
		return tabs;
	}

	private JLabel getBorderLabel(String amount, String header, String type){
		JLabel l = new JLabel(amount + " " + type, JLabel.CENTER);
		l.setPreferredSize(new Dimension(75,35));
		l.setMinimumSize(new Dimension(50,35));
		TitledBorder title = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),header);
		title.setTitleJustification(TitledBorder.CENTER);
		l.setBorder(title);
		return l;
	}	

	public void setTree(){
		ScrollPaneKSCheckBoxTree.setViewportView(tree = new KSTree(aOracle));
	}
	
	private JButton getButtonHent() {
		buttonGetData = new JButton("Hent");
		buttonGetData.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				updateFrameData();
			}
		});
		return buttonGetData;
	}
	
	private void updateFrameData(){
		adHocTable.loadStatistics(tree.getSelectionWhereClause("VT.", "AT."), datePicker.getStart(),datePicker.getEnd(),radioButtonAgents.isSelected());
		overallPerformanceColumns = aOracle.get8025(datePicker.getStart(), datePicker.getEnd());
		label8025.setText(overallPerformanceColumns.getAnsweredCallsPercentage() + " (80/25)");
		labelKald.setText(overallPerformanceColumns.getIncommingCalls() + "");
		labelInterneKald.setText(overallPerformanceColumns.getInternalCalls() + "");
		labelAktiviteter.setText(overallPerformanceColumns.getClosedActivities() + "");
	}

	private JButton getButtonPrint() {
		buttonPrintTable = new JButton("Udskriv");
		buttonPrintTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				try {
					PrinterJob job = PrinterJob.getPrinterJob();
					MessageFormat[] header = new MessageFormat[4];
					header[0] = new MessageFormat("");
					header[1] = new MessageFormat("Fra: "+datePicker.getStart()+" Til: "+datePicker.getEnd());
					header[2] = new MessageFormat("Periode: "+datePicker.getPeriode());
					header[3] = new MessageFormat("Center 80/25: "+overallPerformanceColumns.getAnsweredCallsPercentage());
					
					MessageFormat[] footer = new MessageFormat[2];
					footer[0] = new MessageFormat("");
					footer[1] = new MessageFormat("");
					job.setPrintable(new MyTablePrintable(adHocTable, JTable.PrintMode.FIT_WIDTH, header, footer));
					
					    if (job.printDialog())
					      try {
					        System.out.println("Calling PrintJob.print()");
					        job.print();
					        System.out.println("End PrintJob.print()");
					      }
					      catch (PrinterException pe) {
					        System.out.println("Error printing: " + pe);
					      } 
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		return buttonPrintTable;
	}
	
	private JLabel getLabelCopyright() {
		labelCopyright = new JLabel("� Nykredit");
		labelCopyright.setFont(new Font("Dialog", Font.PLAIN, 10));
		return labelCopyright;
	}

	private KSTree getTree(){
		tree = new KSTree(aOracle);
		return tree;
	}

	private JScrollPane getScrollPaneAdHocTable() {
		scrollPaneAdHocTable = new JScrollPane(adHocTable);
		scrollPaneAdHocTable.getViewport().setBackground(Color.white);
		
		scrollPaneAdHocTable.getHorizontalScrollBar().addAdjustmentListener(new AdjustmentListener() {
			@Override
			public void adjustmentValueChanged(AdjustmentEvent arg0) {
				Main.this.adHocTable.getParent().invalidate();
			}
		});
		
		return scrollPaneAdHocTable;
	}
	public Main() {
		super(true);
		aOracle = new Data();
		aOracle.getConfiguration();
		adHocTable = new AdHocTable(aOracle);
		this.setContentPane(getMainPanel());
		this.setLocationRelativeTo(getRootPane());
		this.setTitle(Constants.programName);
		this.pack();
		this.setMinimumSize(this.getSize());
		this.setLocationRelativeTo(getRootPane());
		this.setVisible(true);
	}
	
	private JPanel getConfigurationPanel(){
		ConfigurationPanel p = new ConfigurationPanel(aOracle,adHocTable);
		return p;
	}
	
	private JComboBox getFormattingComboBox(){
		formattingComboBox = new JComboBox();
		formattingComboBox.addItem(EColumn.NOCOLORS);
		formattingComboBox.addItem(EColumn.COLORINGKONCERN);
		formattingComboBox.addItem(EColumn.COLORINGB1B6);
		formattingComboBox.addItem(EColumn.COLORING_MS);
		formattingComboBox.addItem(EColumn.COLORINGNETBANK);
		formattingComboBox.addItem(EColumn.COLORINGFORSIKRING);
		formattingComboBox.addItem(EColumn.COLORINGERHVERV);
		formattingComboBox.addItem(EColumn.COLORINGSUPPORT);
		formattingComboBox.addItem(EColumn.COLORINGE1E2);
	//	formattingComboBox.addItem(EColumn.COLORINGE1);
   //	formattingComboBox.addItem(EColumn.COLORINGE2);
		
		for(EColumn c: EColumn.values()){
			if (c.isSelected() & c.getCategory().equals(ECategory.COLORING))
				formattingComboBox.setSelectedItem(c);
			else
				formattingComboBox.setSelectedItem(0);
		}
		
		formattingComboBox.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				for(int i = 0;i<formattingComboBox.getItemCount();i++){
					if(formattingComboBox.getItemAt(i).equals(formattingComboBox.getSelectedItem()))
						((EColumn)formattingComboBox.getItemAt(i)).setSelected(true);
					else
						((EColumn)formattingComboBox.getItemAt(i)).setSelected(false);
				}
				adHocTable.fillTable();	
				aOracle.setConfiguration();
			}});
		return formattingComboBox;
	}
	
	public static JComboBox getStaticFormattingComboBox(){
		return formattingComboBox;
	}
	
}

