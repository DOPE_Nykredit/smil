package com.nykredit.kundeservice.smil;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class ConfigurationPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private AdHocTable adHocTable;
	private Data aOracle;
	private JCheckBox selectAllCheckBox;
	/**
	 * kaldPanel er metoden til kolonnen KALD
	 */
	private ConfigurationGroupPanel kaldPanel;
	private ConfigurationGroupPanel ctiPanel;
	private ConfigurationGroupPanel salgPanel;
	private ConfigurationGroupPanel aktiviteterPanel;
	private ConfigurationGroupPanel beregningerPanel;
	private ConfigurationGroupPanel ekspeditionsservicePanel;
	private ConfigurationGroupPanel historikPanel;

	/**
	 * @param aOracle
	 * @param adHocTable
	 */
	public ConfigurationPanel(Data aOracle, AdHocTable adHocTable){
		this.kaldPanel = new ConfigurationGroupPanel(this,ECategory.KALD); //kolonne Kald i fanebladet Opsætning
		this.ctiPanel = new ConfigurationGroupPanel(this,ECategory.CTI); //Kolonne CTI i fanebladet Opsætning
		this.salgPanel = new ConfigurationGroupPanel(this,ECategory.SALG); //Kolonne Salg i fanebladet Opsætning
		this.aktiviteterPanel = new ConfigurationGroupPanel(this,ECategory.AKTIVITETER); //Kolonne Aktiviteter i fanebladet Opsætning
		this.beregningerPanel = new ConfigurationGroupPanel(this,ECategory.BEREGNINGER); // Kolonne Beregninger i fanebladet Opsætning
		this.ekspeditionsservicePanel = new ConfigurationGroupPanel(this,ECategory.EKSPEDITIONSSERVICE); //Kolonne Ekspeditionsservice i fanebladet Opsætning
		this.historikPanel = new ConfigurationGroupPanel(this, ECategory.HISTORIK);
		
		this.aOracle = aOracle;
		this.adHocTable = adHocTable;
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		this.setOpaque(false);
		c.anchor = GridBagConstraints.NORTHWEST;

		c.gridx = 0;
		c.gridy = 0;
		this.add(kaldPanel,c);
		
		c.gridx = 1;
		this.add(ctiPanel,c);
		
		c.gridx = 2;
		this.add(salgPanel,c);
		
		c.gridx = 3;
		this.add(aktiviteterPanel,c);

		c.gridx = 4;
		this.add(beregningerPanel,c);
		
		c.gridx = 5;
		this.add(ekspeditionsservicePanel,c);
		
		c.gridx = 6;
		this.add(this.historikPanel, c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 7;		
		this.add(getSelectAllCheckBox(),c);
		
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 5;
		c.fill = GridBagConstraints.HORIZONTAL;
		this.add(getConfigurationButtonPanel(),c);
		
		c.gridwidth = 1;
		
		c.weightx = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 7;
		this.add(new JLabel(" ",JLabel.LEFT),c);		

		c.gridwidth = 1;
		c.gridx = 6;
		c.gridy = 0;
		c.gridheight = 4;
		this.add(new JLabel(" ",JLabel.LEFT),c);	
	}
	

	private JPanel getConfigurationButtonPanel(){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		p.setOpaque(false);
		p.setBorder(BorderFactory.createTitledBorder("Standard opsætning"));
		c.insets = new Insets(0,5,0,0);
		c.gridx = 0;
		c.gridy = 0;			
		c.anchor = GridBagConstraints.CENTER; 				//hovedoverskrift til knapperne under Basis
		p.add(getHeaderLabel("Basis"), c); 					//Placering af Knappen
		c.gridy = 1;	
		p.add(getButtonStandardBasisKoncern(), c);			//Placering af Knappen Koncern
		c.gridy = 2;	
		p.add(getButtonStandardBasisB2B8(), c);				//Placering af Knappen B1-B7
		c.gridx = 1;
		c.gridy = 0;		
		p.add(getHeaderLabel("Digitale Services"), c);
		c.gridy = 1;			
		p.add(getButtonStandardRaadgiverNetbank(), c); 		//Placering af Knappen Netbank
		c.gridy = 2;			
		p.add(getButtonStandardRaadgiverErhverv(), c); 		//Placering af Knappen Erhverv
		c.gridy = 3;			
		p.add(getButtonStandardRaadgiverSupport(), c); 		//Placering af Knappen Support
		c.gridy = 4;
		p.add(getButtonStandardMailService(), c); 			//Placering af Knappen Mailservice
		c.gridx = 2;
		c.gridy = 0;	
		p.add(getHeaderLabel("Ekspedition"), c); 			//hovedoverskrift til knapperne under Ekspedidition
		c.gridy = 1;	
		p.add(getButtonStandardEkspedition(), c); 			//Placering af Knappen E1-E2
		c.gridy = 2;
	/*	p.add(getButtonStandardE1(), c);
		c.gridy = 3;
		p.add(getButtonStandardE2(), c);
		c.gridy = 0;
*/
		c.gridx = 7;
		c.gridheight = 100;
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		p.add(new JLabel(""),c);
		
		if(System.getenv("username").equalsIgnoreCase("JSKR") || System.getenv("username").equalsIgnoreCase("MDV")){
			c.gridx = 3;
			c.gridwidth = 10;
			c.gridheight = 5;
			c.fill = GridBagConstraints.BOTH;
			
			p.add(getButtonStandardJSKR(), c);
		}
		
		return p;
	}
	// Kode til knappen "Koncern" standard opsætning for Koncern
	private JButton getButtonStandardBasisKoncern(){
		JButton b = getStandardButton("Koncern");
		
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				clearHeaderCheckBoxes();
				
				 for(EColumn col:EColumn.values())
					 if(col.equals(EColumn.KALD_INDGÅENDE) || 
						col.equals(EColumn.CTI_KORRIGERET)  || 
						col.equals(EColumn.CTI_EMAIL) || 
						col.equals(EColumn.BEREGNINGER_TILGÆNGELIGHED) || 
						col.equals(EColumn.SHOWAGENTS) || 
						col.equals(EColumn.SHOWTOTAL))
						 col.setSelected(true);
					 else
						 col.setSelected(false); 
		
				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORINGKONCERN);
				 updateConfiguration();
			}});
		return b;
	}
	
	private JButton getButtonStandardBasisB2B8(){
		JButton b = getStandardButton("B1-B7");
		
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				clearHeaderCheckBoxes();
				
				 for(EColumn col:EColumn.values())
					 if(col.equals(EColumn.KALD_INDGÅENDE) || 
						col.equals(EColumn.CTI_KORRIGERET) || 
						col.equals(EColumn.SALG_HENVISNINGER) || 
						col.equals(EColumn.SALG_GENNEMFØRTESALG) || 
						col.equals(EColumn.SALG_SALGSRATE)	|| 
						//col.equals(EColumn.BEREGNINGER_KPI) ||
						col.equals(EColumn.BEREGNINGER_TILGÆNGELIGHED) ||
						col.equals(EColumn.SHOWAGENTS) ||
						col.equals(EColumn.SHOWSUBTOTAL) || 
						col.equals(EColumn.SHOWTOTAL))
						 col.setSelected(true);
					 else
						 col.setSelected(false); 
				
				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORINGB1B6);
				 updateConfiguration(); 
			}});
		return b;
	}
	
	private JButton getButtonStandardMailService(){
		JButton b = getStandardButton("Mailservice");
		
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				clearHeaderCheckBoxes();
				
				 for(EColumn col:EColumn.values())
					 if(col.equals(EColumn.KALD_INDGÅENDE) || 
						col.equals(EColumn.KALD_WEBDESK) || 
						col.equals(EColumn.CTI_KORRIGERET) || 
						col.equals(EColumn.CTI_WEBDESK) || 
						col.equals(EColumn.SALG_HENVISNINGER) || 
						col.equals(EColumn.BEREGNINGER_KPIWEBDESK) ||
						col.equals(EColumn.BEREGNINGER_WEBDESKTALERTID) ||
						col.equals(EColumn.SHOWAGENTS) ||
						col.equals(EColumn.SHOWTOTAL))
						 col.setSelected(true);
					 else
						 col.setSelected(false); 

				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORING_MS);
				 updateConfiguration(); 
			}});
		return b;
	}
	
	private JLabel getHeaderLabel(String text){
		JLabel l = new JLabel(text,JLabel.CENTER);
		l.setFont(new Font("Verdana", Font.BOLD, 12));
		return l;
	}

	private JButton getButtonStandardRaadgiverNetbank(){
		JButton b = getStandardButton("NetBank");
		
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				clearHeaderCheckBoxes();
					
				 for(EColumn col:EColumn.values())
					 if(col.equals(EColumn.KALD_INDGÅENDE) || 
						col.equals(EColumn.CTI_KORRIGERET) || 
						col.equals(EColumn.AKTIVITETER_AFSLUTTETINDENFOR2TIMERINKLEMAIL) || 
						col.equals(EColumn.BEREGNINGER_PROCENTAKTIVITETERINDFORTIDSFRIST) ||
						//col.equals(EColumn.BEREGNINGER_KPICONSULT) ||
						col.equals(EColumn.BEREGNINGER_TILGÆNGELIGHED) ||
						col.equals(EColumn.SHOWAGENTS) || 
						col.equals(EColumn.SHOWTOTAL))
						 col.setSelected(true);
					 else
						 col.setSelected(false); 
					 
				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORINGNETBANK);
				 updateConfiguration(); 
			}});
		return b;
	}
	
	private JButton getButtonStandardRaadgiverErhverv(){
		JButton b = getStandardButton("Erhverv");
		
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				clearHeaderCheckBoxes();
				
				 for(EColumn col:EColumn.values())
					 if(col.equals(EColumn.KALD_INDGÅENDE) || 
						col.equals(EColumn.CTI_KORRIGERET) || 
						col.equals(EColumn.AKTIVITETER_AFSLUTTETINDENFOR2TIMEREXCLEMAIL) || 
						col.equals(EColumn.BEREGNINGER_PROCENTAKTIVITETERINDFORTIDSFRIST) || 
					//	col.equals(EColumn.BEREGNINGER_KPI) || 
						col.equals(EColumn.BEREGNINGER_TILGÆNGELIGHED) || 
						col.equals(EColumn.SHOWAGENTS) || 
						col.equals(EColumn.SHOWTOTAL))
						 col.setSelected(true);
					 else
						 col.setSelected(false); 

				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORINGERHVERV);
				 updateConfiguration(); 
			}});
		return b;
	}

	private JButton getButtonStandardRaadgiverSupport(){
		JButton b = getStandardButton("Support");
		
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				clearHeaderCheckBoxes();
				
				 for(EColumn col:EColumn.values())
					 if(col.equals(EColumn.CTI_KORRIGERET) ||
						col.equals(EColumn.CTI_PAUSE) ||
						col.equals(EColumn.CTI_UDDANNELSE) || 
						col.equals(EColumn.SUPPORTOPGAVER_AFSLUTTET) || 
						col.equals(EColumn.BEREGNINGER_PROCENTSUPPORTOPGAVERINDENFORTIDSFRIST) || 
						col.equals(EColumn.AKTIVITETER_OPRETTEDEKAMPAGNEPOTENTIALER) || 
						col.equals(EColumn.AKTIVITETER_AFSLUTTETINDENFOR2TIMEREXCLEMAIL) ||
						col.equals(EColumn.BEREGNINGER_KPISUPPORT) || 
						col.equals(EColumn.BEREGNINGER_PAUSEPCTAFLOGINTIME) ||
						col.equals(EColumn.SHOWAGENTS) || 
						col.equals(EColumn.SHOWTOTAL) || 
						col.equals(EColumn.HIDEEMPTYLOGIN) ||
						col.equals(EColumn.KALD_INDGÅENDE))
						 col.setSelected(true);
					 else
						 col.setSelected(false); 
					 
				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORINGSUPPORT);
				 updateConfiguration(); 
			}});
		return b;
	}

	private JButton getButtonStandardEkspedition(){
		JButton b = getStandardButton("E1-E2");
		
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				clearHeaderCheckBoxes();
				
				for(EColumn col:EColumn.values())
					 if(col.equals(EColumn.KALD_INDGÅENDE) || 
						col.equals(EColumn.KALD_UDGÅENDE) || 
						col.equals(EColumn.KALD_CBR) || 
						col.equals(EColumn.CTI_KORRIGERET) || 
						col.equals(EColumn.SALG_HENVISNINGER) || 
						col.equals(EColumn.SALG_GENNEMFØRTESALG) || 
						col.equals(EColumn.SALG_SALGSRATE) || 
						col.equals(EColumn.AKTIVITETER_MØDEBOOKING) ||
						col.equals(EColumn.BEREGNINGER_AHT) || 
					//	col.equals(EColumn.BEREGNINGER_KPI) ||
						col.equals(EColumn.BEREGNINGER_TILGÆNGELIGHED) || 
						col.equals(EColumn.EKSPEDITION_OPFØLGNINGSSERVICE) ||
						col.equals(EColumn.EKSPEDITION_OPRETTETAKTIVITETERIKKEVIDERESTILLING) ||
						col.equals(EColumn.EKSPEDITION_HENVISNINGERPRLOGINTIME) ||
						col.equals(EColumn.EKSPEDITION_SOLGTESERVICEPRODUKTER) ||
						col.equals(EColumn.EKSPEDITION_SERVICEPRODUKTERPRLOGINTIME) ||
						col.equals(EColumn.EKSPEDITION_TABTSALG) || 
						col.equals(EColumn.SHOWAGENTS) ||
						col.equals(EColumn.SHOWTOTAL) ||
						col.equals(EColumn.HIDEEMPTYLOGIN))
						 col.setSelected(true);
					 else
						 col.setSelected(false); 

				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORINGE1E2);
				 updateConfiguration(); 
			}});
		return b;
	}
	
	private JButton getButtonStandardJSKR(){
		JButton b = getStandardButton("Hans kongelige højhed, JSKR af DOPE");
		
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				clearHeaderCheckBoxes();
				
				Main.getStaticFormattingComboBox().setSelectedIndex(0);
				
				for(EColumn col:EColumn.values())
					 if(col.equals(EColumn.SALG_GENNEMFØRTESALG) || 
						col.equals(EColumn.SALG_SALGSRATE) || 
						col.equals(EColumn.AKTIVITETER_OVERSKREDET) || 
						col.equals(EColumn.SUPPORTOPGAVER_OVERSKREDET) ||
						col.equals(EColumn.SHOWTOTAL))
						 col.setSelected(true);
					 else
						 col.setSelected(false); 

				 updateConfiguration(); 
			}
		});
		
		return b;
	}
	
	private void clearHeaderCheckBoxes(){
		 kaldPanel.setHeaderCheckBox(false);
		 ctiPanel.setHeaderCheckBox(false);
		 salgPanel.setHeaderCheckBox(false);
		 aktiviteterPanel.setHeaderCheckBox(false);
		 beregningerPanel.setHeaderCheckBox(false);
		 ekspeditionsservicePanel.setHeaderCheckBox(false);
	}

	private JButton getStandardButton(String text){
		JButton b = new JButton(text);
		b.setPreferredSize(new Dimension(100,20));
		b.setMinimumSize(new Dimension(100,20));
		return b;
	}
		
	private JCheckBox getSelectAllCheckBox(){
		selectAllCheckBox = new JCheckBox("Vælg alle");
		selectAllCheckBox.setFont(new Font("Verdana", Font.BOLD, 12));
		selectAllCheckBox.setOpaque(false);
		
		if (kaldPanel.getSelectedState()&
			ctiPanel.getSelectedState()&
			salgPanel.getSelectedState()&
			aktiviteterPanel.getSelectedState()&
			ekspeditionsservicePanel.getSelectedState()){
			selectAllCheckBox.setSelected(true);
		}else{
			selectAllCheckBox.setSelected(false);
		}

		selectAllCheckBox.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				if(selectAllCheckBox.isSelected()){
					 kaldPanel.setSelectedState(true);
					 ctiPanel.setSelectedState(true);
					 salgPanel.setSelectedState(true);
					 aktiviteterPanel.setSelectedState(true);
					 beregningerPanel.setSelectedState(true);
					 ekspeditionsservicePanel.setSelectedState(true);
					 historikPanel.setSelectedState(true);
				}else{
					 kaldPanel.setSelectedState(false);
					 ctiPanel.setSelectedState(false);
					 salgPanel.setSelectedState(false);
					 aktiviteterPanel.setSelectedState(false);
					 beregningerPanel.setSelectedState(false);
					 ekspeditionsservicePanel.setSelectedState(false);
					 historikPanel.setSelectedState(false);
				}
			}
		});
		return selectAllCheckBox;
	}
	
	public void setSelectedAllCheckBox(boolean selected){
		selectAllCheckBox.setSelected(selected);
	}
	
	public void updateConfiguration(){
		adHocTable.setAdHocTableModel();
		aOracle.setConfiguration();
		selectAllCheckBox.setSelected(getOverAllSelectedState());
	}
	
	public boolean getOverAllSelectedState(){
		boolean allSelected = true;
		for (EColumn c: EColumn.values()){
			if(!c.getCheckBox().isSelected() & (!c.getCategory().equals(ECategory.SETTINGS)&!c.getCategory().equals(ECategory.COLORING)))
				allSelected = false;
		}
		return allSelected;
	}

}
