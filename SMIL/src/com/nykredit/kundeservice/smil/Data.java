package com.nykredit.kundeservice.smil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nykredit.kundeservice.data.CTIRConnection;
// test
public class Data extends CTIRConnection {
	
	public Data() {
		try{
		this.Connect();
		this.SetupSpy(Constants.programName);
		this.Hent_intet("ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS'");
		}catch (SQLException e){e.printStackTrace();}
	}
	
	public OverallPerformanceColumns get8025(String fromDate, String toDate){
		int incommingCalls = 0;
		int internalCalls = 0;
		double answeredCallsPercentage = 0;
		int closedActivities = 0;

		String sqlQuery = "SELECT " +
							  "NVL(SUM(ANTAL_KALD),0) AS INCOMMING_CALLS, " +
							  "DECODE(SUM(ANTAL_KALD), 0, 0, SUM(BESVARET_25_SEK) / SUM(ANTAL_KALD)) AS ANSWERED_CALLS_PERCENTAGE, " +
							  "NVL(SUM(INTERN_KALD),0) AS INTERNAL_CALLS " +
						  "FROM " +
						  	  "PERO_NKM_KØ_OVERSIGT " +
						  "WHERE " +
						  	  "TIDSPUNKT>='" + fromDate + "' AND TIDSPUNKT<='" + toDate + "'";
		
		ResultSet rs = runsql(sqlQuery);
		
		try {
			rs.next();
			incommingCalls = rs.getInt("INCOMMING_CALLS");
			internalCalls = rs.getInt("INTERNAL_CALLS");
			answeredCallsPercentage = rs.getDouble("ANSWERED_CALLS_PERCENTAGE");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		sqlQuery = "SELECT " +
					   "NVL(SUM(\"Antal aktiviteter\"), 0) AS CLOSED_ACTIVITIES " +
				   "FROM " +
				   	   "KS_DRIFT.DIAN_AKT_AFS_KS " +
				   "WHERE " +
				   	   "\"Faktisk afsluttet dato\" >= '" + fromDate + "' AND " +
				   	   "\"Faktisk afsluttet dato\" <= '" + toDate + "' AND " +
				   	   "\"Aktivitetstype\" NOT IN ('Indgående kald','Kundepleje')";
				
				rs = runsql(sqlQuery);
				
				try {
					rs.next();
					closedActivities = rs.getInt("CLOSED_ACTIVITIES");
				} catch (SQLException e) {e.printStackTrace();}
		
		OverallPerformanceColumns overallPerformanceColumns = new OverallPerformanceColumns(incommingCalls,
																							internalCalls,
																							answeredCallsPercentage,
																							closedActivities);
		
		return overallPerformanceColumns;
	}
 	
	public void getConfiguration(){
		String initialer = System.getenv("USERNAME").toUpperCase();
		
		ResultSet rs = runsql("SELECT * FROM KS_DRIFT.AHS_CONFIGURATION WHERE INITIALER='"+initialer+"'");
		try {
			while(rs.next()){
				EColumn.KALD_INDGÅENDE.setTrueFalseValue(rs.getInt("INDKALD"));
				EColumn.KALD_UDGÅENDE.setTrueFalseValue(rs.getInt("UDKALD"));
				EColumn.KALD_CONSULT.setTrueFalseValue(rs.getInt("CONSULTKALD"));
				EColumn.KALD_CBR.setTrueFalseValue(rs.getInt("CBRKALD"));
				EColumn.KALD_WEBDESK.setTrueFalseValue(rs.getInt("WEBDESKKALD"));
				EColumn.CTI_KORRIGERET.setTrueFalseValue(rs.getInt("KORRIGERET"));
				EColumn.CTI_TALER.setTrueFalseValue(rs.getInt("TALER"));
				EColumn.CTI_KLAR.setTrueFalseValue(rs.getInt("KLAR"));
				EColumn.CTI_ØVRIG.setTrueFalseValue(rs.getInt("ØVRIG"));
				EColumn.CTI_EMAIL.setTrueFalseValue(rs.getInt("EMAIL"));
				EColumn.CTI_WEBDESK.setTrueFalseValue(rs.getInt("WEBDESK"));
				EColumn.CTI_EFTERBEHANDLING.setTrueFalseValue(rs.getInt("EFTERBEH"));
				EColumn.CTI_ANDREOPGAVER.setTrueFalseValue(rs.getInt("ANDREOPG"));
				EColumn.CTI_KOLLEGAHJÆLP.setTrueFalseValue(rs.getInt("KOLLEGAHJ"));
				EColumn.CTI_PAUSE.setTrueFalseValue(rs.getInt("PAUSE"));
				EColumn.CTI_FROKOST.setTrueFalseValue(rs.getInt("FROKOST"));
				EColumn.CTI_UDDANNELSE.setTrueFalseValue(rs.getInt("UDDANNELSE"));
				EColumn.CTI_MØDE.setTrueFalseValue(rs.getInt("MØDE"));
				EColumn.CTI_SPÆRRET.setTrueFalseValue(rs.getInt("SPÆRRET"));
				EColumn.HISTORIK_FORSIKRINGSPRÆMIE.setTrueFalseValue(rs.getInt("FORSIKRINGSP"));
				EColumn.SALG_MERSALG.setTrueFalseValue(rs.getInt("MERSALG"));
				EColumn.SALG_HENVISNINGER.setTrueFalseValue(rs.getInt("HENVISNINGER"));
				EColumn.SALG_GENNEMFØRTESALG.setTrueFalseValue(rs.getInt("GENNEMFØRTSALG"));
				EColumn.SALG_SALGSRATE.setTrueFalseValue(rs.getInt("SALGSRATE"));
				EColumn.AKTIVITETER_EMAILINDGÅENDE.setTrueFalseValue(rs.getInt("AKTEMAILIND"));
				EColumn.AKTIVITETER_AFSLUTTET.setTrueFalseValue(rs.getInt("AKTIVITETER"));
				EColumn.AKTIVITETER_OPRETTET.setTrueFalseValue(rs.getInt("AKTIVITETEROPR"));
				EColumn.AKTIVITETER_AFSLUTTETINDENFOR2TIMERINKLEMAIL.setTrueFalseValue(rs.getInt("AKTAFSLINDENFOR2TIMEREKSMAIL"));
				EColumn.AKTIVITETER_AFSLUTTETINDENFOR2TIMEREXCLEMAIL.setTrueFalseValue(rs.getInt("AKTAFSLINDENFOR2TIMERINKLMAIL"));
				EColumn.AKTIVITETER_INDENDFORTIDSFRIST.setTrueFalseValue(rs.getInt("AKTINDTID"));
				EColumn.AKTIVITETER_OVERSKREDET.setTrueFalseValue(rs.getInt("AKTOVER"));
				EColumn.AKTIVITETER_TELEFONBESKEDER.setTrueFalseValue(rs.getInt("TLFBESK"));
				EColumn.SUPPORTOPGAVER_AFSLUTTET.setTrueFalseValue(rs.getInt("SUPPORTOPGAVER"));
				EColumn.SUPPORTOPGAVER_OVERSKREDET.setTrueFalseValue(rs.getInt("SUPPORTOPGAVEROVERSKREDNE"));
				EColumn.SUPPORTOPGAVER_INDENFORFRIST.setTrueFalseValue(rs.getInt("SUPPORTOPGINDENFORFRIST"));
				EColumn.AKTIVITETER_OPRETTEDEKAMPAGNEPOTENTIALER.setTrueFalseValue(rs.getInt("OPRETTEDEKAMPAGNEPOTENTIALER"));
				EColumn.AKTIVITETER_CALLBACKSHÅNDTERET.setTrueFalseValue(rs.getInt("CALLBACKS"));
				EColumn.BEREGNINGER_PROCENTAKTIVITETERINDFORTIDSFRIST.setTrueFalseValue(rs.getInt("AKTINDTIDBE"));
				EColumn.BEREGNINGER_AKTIVITETERPRLOGINTIME.setTrueFalseValue(rs.getInt("AKTPRLOGT"));
				EColumn.BEREGNINGER_KALDPRLOGINTIME.setTrueFalseValue(rs.getInt("KALDPRTIME"));
				EColumn.BEREGNINGER_EMAILAKTIVITETERPRLOGINTIME.setTrueFalseValue(rs.getInt("EMAILAKTPRLOGT"));
				EColumn.BEREGNINGER_EMAILAKTIVITETERPREMAILLOGINTIME.setTrueFalseValue(rs.getInt("EMAILAKTPREMAILLOGT"));
				EColumn.BEREGNINGER_AHT.setTrueFalseValue(rs.getInt("AHT"));
				//EColumn.BEREGNINGER_KPI.setTrueFalseValue(rs.getInt("KPI"));
				EColumn.BEREGNINGER_KPIWEBDESK.setTrueFalseValue(rs.getInt("KPIWEBDESK"));
				EColumn.BEREGNINGER_KPISUPPORT.setTrueFalseValue(rs.getInt("KPISUPPORT"));
			//	EColumn.BEREGNINGER_KPICONSULT.setTrueFalseValue(rs.getInt("KPICONSULT"));
				EColumn.BEREGNINGER_PAUSEPCTAFLOGINTIME.setTrueFalseValue(rs.getInt("PAUSEPCT"));
				EColumn.BEREGNINGER_TILGÆNGELIGHED.setTrueFalseValue(rs.getInt("TILGÆNGELIGHED"));
				EColumn.BEREGNINGER_WFCMPLANLAGTTID.setTrueFalseValue(rs.getInt("PLANLAGTTID"));
				EColumn.BEREGNINGER_WFCMPLANLAGTKUNDETID.setTrueFalseValue(rs.getInt("PLANLAGTKUNDETID"));
				EColumn.BEREGNINGER_KAMPAGNEPOTENTIALERPRTIME.setTrueFalseValue(rs.getInt("KAMPAGNEPOTENTIALERPREMAILTIME"));
				EColumn.BEREGNINGER_PROCENTVISMERSALG.setTrueFalseValue(rs.getInt("PROCENTVISMERSALG"));
				EColumn.BEREGNINGER_WEBDESKTALERTID.setTrueFalseValue(rs.getInt("WEBDESKTALERTID"));
				EColumn.BEREGNINGER_WEBDESKKALDPRWEBDESKTIME.setTrueFalseValue(rs.getInt("WEBDESKKALDPRWEBDESKTIME"));
				EColumn.EKSPEDITION_OPFØLGNINGSSERVICE.setTrueFalseValue(rs.getInt("OPFØLGNINGSSERVICEAFS"));
				EColumn.EKSPEDITION_INDGÅENDEKALD.setTrueFalseValue(rs.getInt("INDGÅENDEKALDDIALOG"));
				EColumn.EKSPEDITION_OPRETTETAKTIVITETERIKKEVIDERESTILLING.setTrueFalseValue(rs.getInt("OPRETTEDEAKTIKKEVIDERE"));
				EColumn.EKSPEDITION_TLFBESKEDERPCTINDKALD.setTrueFalseValue(rs.getInt("TLFBESKPCTINDKALD"));
				EColumn.EKSPEDITION_VIDERESTILLINGERPCTINDKALD.setTrueFalseValue(rs.getInt("VIDERESTILLINGERPCTINDKALD"));
				EColumn.EKSPEDITION_HENVISNINGERPRLOGINTIME.setTrueFalseValue(rs.getInt("HENVISNINGERPRLOGINTIME"));
				EColumn.EKSPEDITION_VIDERESTILLINGER.setTrueFalseValue(rs.getInt("VIDERESTILLINGER"));
				EColumn.EKSPEDITION_SOLGTESERVICEPRODUKTER.setTrueFalseValue(rs.getInt("SOLGTESERVICEPRODUKTER"));
				EColumn.EKSPEDITION_SERVICEPRODUKTERPRLOGINTIME.setTrueFalseValue(rs.getInt("SERVICEPRODUKTERPRLOGINTIME"));
				EColumn.EKSPEDITION_TABTSALG.setTrueFalseValue(rs.getInt("TABTSALG"));
				EColumn.HISTORIK_EMAILTILGÆNGELIGHED.setTrueFalseValue(rs.getInt("EMAILTILG"));
				EColumn.HISTORIK_TELEFONTILGÆNGELIGHED.setTrueFalseValue(rs.getInt("TLFTILG"));
				EColumn.HISTORIK_TILGÆNGELIGHED2011.setTrueFalseValue(rs.getInt("TILGÆNGELIGHED2011"));
				EColumn.HISTORIK_TILGÆNGELIGHED2012.setTrueFalseValue(rs.getInt("TILGÆNGELIGHED"));
				EColumn.HISTORIK_FÆRDIGGØRELSESGRAD.setTrueFalseValue(rs.getInt("FÆRDIGGG"));
				EColumn.SHOWAVERAGE.setTrueFalseValue(rs.getInt("SHOWAVERAGE"));
				EColumn.SHOWSUBTOTAL.setTrueFalseValue(rs.getInt("SHOWSUBTOTAL"));
				EColumn.SHOWAGENTS.setTrueFalseValue(rs.getInt("SHOWAGENTS"));
				EColumn.HIDEEMPTYLOGIN.setTrueFalseValue(rs.getInt("HIDEEMPTYLOGIN"));
				EColumn.NOCOLORS.setTrueFalseValue(rs.getInt("NOCOLORS"));			
				EColumn.COLORINGKONCERN.setTrueFalseValue(rs.getInt("COLORINGKONCERN"));
				EColumn.COLORINGB1B6.setTrueFalseValue(rs.getInt("COLORINGB2B8"));
				EColumn.COLORING_MS.setTrueFalseValue(rs.getInt("COLORINGB9"));
				EColumn.COLORINGNETBANK.setTrueFalseValue(rs.getInt("COLORINGNETBANK"));
				EColumn.COLORINGFORSIKRING.setTrueFalseValue(rs.getInt("COLORINGFORSIKRING"));
				EColumn.COLORINGERHVERV.setTrueFalseValue(rs.getInt("COLORINGERHVERV"));
				EColumn.COLORINGSUPPORT.setTrueFalseValue(rs.getInt("COLORINGSUPPORT"));
				EColumn.COLORINGE1E2.setTrueFalseValue(rs.getInt("COLORINGE1E2"));
			}
		} catch (SQLException e) {//e.printStackTrace();
				System.out.println("Der er en fejl i Data.java linie 81-162");	
		}
	}
	
	public void setConfiguration(){
		String initialer = System.getenv("USERNAME").toUpperCase();
		
		try {
			String sql =
					"INSERT INTO " + 
					    "KS_DRIFT.AHS_CONFIGURATION (INITIALER, " +
													"INDKALD, " +
													"UDKALD, " +
													"CONSULTKALD, " +
													"CBRKALD, " +
													"WEBDESKKALD, " +
													"KORRIGERET, " +
													"TALER, " +
													"KLAR, " +
													"ØVRIG, " +
													"EMAIL, " +
													"WEBDESK, " +						
													"EFTERBEH, " +
													"ANDREOPG, " +
													"KOLLEGAHJ, " +
													"PAUSE, " +
													"FROKOST, " +
													"UDDANNELSE, " +
													"MØDE, " +
													"SPÆRRET, " +
													"FORSIKRINGSP, " +
													"MERSALG, " +
													"HENVISNINGER, " +
													"GENNEMFØRTSALG, " +
													"SALGSRATE, " +
													"AKTEMAILIND, " +
													"AKTIVITETER, " +
													"AKTIVITETEROPR, " +
													"AKTAFSLINDENFOR2TIMERINKLMAIL, " + 
													"AKTAFSLINDENFOR2TIMEREKSMAIL, " +
													"AKTINDTID, " +
													"AKTOVER, " +
													"TLFBESK, " +
													"SUPPORTOPGAVER, " +
													"SUPPORTOPGAVEROVERSKREDNE, " +
													"SUPPORTOPGINDENFORFRIST, " +
													"OPRETTEDEKAMPAGNEPOTENTIALER, " +
													"CALLBACKS, " +
													"AKTINDTIDBE, " +
													"AKTPRLOGT, " +
													"KALDPRTIME, " +
													"EMAILAKTPRLOGT, " +
													"EMAILAKTPREMAILLOGT, " +
													"AHT, " +
													"KPI, " +
													"KPIWEBDESK, " +
													"KPISUPPORT, " +
													"KPICONSULT, " +
													"PAUSEPCT, " +
													"TILGÆNGELIGHED, " +
													"PLANLAGTTID, " +
													"PLANLAGTKUNDETID, " +
													"KAMPAGNEPOTENTIALERPREMAILTIME, " +
													"PROCENTVISMERSALG, " +
													"WEBDESKTALERTID, " +
													"WEBDESKKALDPRWEBDESKTIME, " +
													"OPFØLGNINGSSERVICEAFS, " +
													"INDGÅENDEKALDDIALOG, " +
													"OPRETTEDEAKTIKKEVIDERE, " +				
													"TLFBESKPCTINDKALD, " +
													"VIDERESTILLINGERPCTINDKALD, " +
													"HENVISNINGERPRLOGINTIME, " +
													"VIDERESTILLINGER, " +
													"SOLGTESERVICEPRODUKTER, " +
													"SERVICEPRODUKTERPRLOGINTIME, "+
													"TABTSALG, " +
													"EMAILTILG, " +
													"TLFTILG, " +
													"TILGÆNGELIGHED2011, " +
													"TILGÆNGELIGHED2012, " +
													"FÆRDIGGG, " +
													"SHOWAVERAGE, " +
													"SHOWSUBTOTAL, " +
													"SHOWAGENTS, " +
													"HIDEEMPTYLOGIN, " +
													"NOCOLORS, " +						
													"COLORINGKONCERN, " +
													"COLORINGB2B8, " +
													"COLORINGB9, " +
													"COLORINGNETBANK, " +
													"COLORINGFORSIKRING, " +
													"COLORINGERHVERV, " +
													"COLORINGSUPPORT, " +
													"COLORINGE1E2) " +
												"VALUES (" +
													"'" + initialer + "', " + 
													EColumn.KALD_INDGÅENDE.getTrueFalseValue() + ", " +
													EColumn.KALD_UDGÅENDE.getTrueFalseValue() + ", " +
													EColumn.KALD_CONSULT.getTrueFalseValue() + ", " +
													EColumn.KALD_CBR.getTrueFalseValue() + ", " +
													EColumn.KALD_WEBDESK.getTrueFalseValue()+", " +
													EColumn.CTI_KORRIGERET.getTrueFalseValue() + ", " +
													EColumn.CTI_TALER.getTrueFalseValue() + ", " +
													EColumn.CTI_KLAR.getTrueFalseValue() + ", " +
													EColumn.CTI_ØVRIG.getTrueFalseValue() + ", " +
													EColumn.CTI_EMAIL.getTrueFalseValue() + ", " +
													EColumn.CTI_WEBDESK.getTrueFalseValue() + ", " +
													EColumn.CTI_EFTERBEHANDLING.getTrueFalseValue()+", " +
													EColumn.CTI_ANDREOPGAVER.getTrueFalseValue()+", " +
													EColumn.CTI_KOLLEGAHJÆLP.getTrueFalseValue() + ", " +
													EColumn.CTI_PAUSE.getTrueFalseValue() + ", " +
													EColumn.CTI_FROKOST.getTrueFalseValue() + ", " +							
													EColumn.CTI_UDDANNELSE.getTrueFalseValue()+", " +
													EColumn.CTI_MØDE.getTrueFalseValue() + ", " +
													EColumn.CTI_SPÆRRET.getTrueFalseValue() + ", " +
													EColumn.HISTORIK_FORSIKRINGSPRÆMIE.getTrueFalseValue() + ", " +
													EColumn.SALG_MERSALG.getTrueFalseValue()+"," +
													EColumn.SALG_HENVISNINGER.getTrueFalseValue() + ", " +
													EColumn.SALG_GENNEMFØRTESALG.getTrueFalseValue() + ", " +
													EColumn.SALG_SALGSRATE.getTrueFalseValue()+", " +
													EColumn.AKTIVITETER_EMAILINDGÅENDE.getTrueFalseValue() + ", " +
													EColumn.AKTIVITETER_AFSLUTTET.getTrueFalseValue() + ", " +
													EColumn.AKTIVITETER_OPRETTET.getTrueFalseValue()+", " +
													EColumn.AKTIVITETER_AFSLUTTETINDENFOR2TIMERINKLEMAIL.getTrueFalseValue()+ ", " +
													EColumn.AKTIVITETER_AFSLUTTETINDENFOR2TIMEREXCLEMAIL.getTrueFalseValue()+ ", " +
													EColumn.AKTIVITETER_INDENDFORTIDSFRIST.getTrueFalseValue() + ", " +
													EColumn.AKTIVITETER_OVERSKREDET.getTrueFalseValue() + ", " +
													EColumn.AKTIVITETER_TELEFONBESKEDER.getTrueFalseValue() + ", " +
													EColumn.SUPPORTOPGAVER_AFSLUTTET.getTrueFalseValue() + ", " + 
													EColumn.SUPPORTOPGAVER_OVERSKREDET.getTrueFalseValue() + ", " +
													EColumn.SUPPORTOPGAVER_INDENFORFRIST.getTrueFalseValue() + ", " +
													EColumn.AKTIVITETER_OPRETTEDEKAMPAGNEPOTENTIALER.getTrueFalseValue() + ", " +
													EColumn.AKTIVITETER_CALLBACKSHÅNDTERET.getTrueFalseValue() + ", " +
													EColumn.BEREGNINGER_PROCENTAKTIVITETERINDFORTIDSFRIST.getTrueFalseValue() + ", " +
													EColumn.BEREGNINGER_AKTIVITETERPRLOGINTIME.getTrueFalseValue() + ", " +
													EColumn.BEREGNINGER_KALDPRLOGINTIME.getTrueFalseValue()+", " +
													EColumn.BEREGNINGER_EMAILAKTIVITETERPRLOGINTIME.getTrueFalseValue() + ", " +
													EColumn.BEREGNINGER_AHT.getTrueFalseValue()+", " +
												//	EColumn.BEREGNINGER_KPI.getTrueFalseValue() + ", " +
													EColumn.BEREGNINGER_KPIWEBDESK.getTrueFalseValue() + ", " +
													EColumn.BEREGNINGER_KPISUPPORT.getTrueFalseValue() + ", " +
												//	EColumn.BEREGNINGER_KPICONSULT.getTrueFalseValue() + ", " +
													EColumn.BEREGNINGER_PAUSEPCTAFLOGINTIME.getTrueFalseValue()+", " +							
													EColumn.BEREGNINGER_TILGÆNGELIGHED.getTrueFalseValue()+", " +
													EColumn.BEREGNINGER_WFCMPLANLAGTTID.getTrueFalseValue()+", " +
													EColumn.BEREGNINGER_WFCMPLANLAGTKUNDETID.getTrueFalseValue()+", " +
													EColumn.BEREGNINGER_KAMPAGNEPOTENTIALERPRTIME.getTrueFalseValue()+", "+
													EColumn.BEREGNINGER_PROCENTVISMERSALG.getTrueFalseValue() + ", " +
													EColumn.BEREGNINGER_WEBDESKTALERTID.getTrueFalseValue()+ ", " +
													EColumn.BEREGNINGER_WEBDESKKALDPRWEBDESKTIME.getTrueFalseValue()+ ", " +
													EColumn.EKSPEDITION_OPFØLGNINGSSERVICE.getTrueFalseValue()+", " +
													EColumn.EKSPEDITION_INDGÅENDEKALD.getTrueFalseValue()+", " +
													EColumn.EKSPEDITION_OPRETTETAKTIVITETERIKKEVIDERESTILLING.getTrueFalseValue()+", " +							
													EColumn.EKSPEDITION_TLFBESKEDERPCTINDKALD.getTrueFalseValue()+", " +
													EColumn.EKSPEDITION_TLFBESKEDERPCTINDKALD.getTrueFalseValue()+", " +
													EColumn.EKSPEDITION_VIDERESTILLINGERPCTINDKALD.getTrueFalseValue()+", " +
													EColumn.EKSPEDITION_HENVISNINGERPRLOGINTIME.getTrueFalseValue()+", " +
													EColumn.EKSPEDITION_VIDERESTILLINGER.getTrueFalseValue()+", " +
													EColumn.EKSPEDITION_SOLGTESERVICEPRODUKTER.getTrueFalseValue()+", " +							
													EColumn.EKSPEDITION_SERVICEPRODUKTERPRLOGINTIME.getTrueFalseValue() + ", " +
													EColumn.EKSPEDITION_TABTSALG.getTrueFalseValue()+", " +
													EColumn.HISTORIK_EMAILTILGÆNGELIGHED.getTrueFalseValue() + ", " +
													EColumn.HISTORIK_TELEFONTILGÆNGELIGHED.getTrueFalseValue()+", " +
													EColumn.HISTORIK_TILGÆNGELIGHED2011.getTrueFalseValue()+", " +
													EColumn.HISTORIK_TILGÆNGELIGHED2012.getTrueFalseValue()+", " +
													EColumn.HISTORIK_FÆRDIGGØRELSESGRAD.getTrueFalseValue() + ", " +
													EColumn.SHOWAVERAGE.getTrueFalseValue()+", " +
													EColumn.SHOWSUBTOTAL.getTrueFalseValue()+", " +
													EColumn.SHOWAGENTS.getTrueFalseValue()+", " +
													EColumn.HIDEEMPTYLOGIN.getTrueFalseValue()+", " +
													EColumn.NOCOLORS.getTrueFalseValue()+", " +						
													EColumn.COLORINGKONCERN.getTrueFalseValue()+", " +
													EColumn.COLORINGB1B6.getTrueFalseValue()+", " +
													EColumn.COLORING_MS.getTrueFalseValue()+", " +
													EColumn.COLORINGNETBANK.getTrueFalseValue()+", " +
													EColumn.COLORINGFORSIKRING.getTrueFalseValue()+", " +
													EColumn.COLORINGERHVERV.getTrueFalseValue()+", " +
													EColumn.COLORINGSUPPORT.getTrueFalseValue()+", " +
													EColumn.COLORINGE1E2.getTrueFalseValue() + ")";
			this.Hent_intet(sql);
		} catch (SQLException e) {
			try {
				String sql = "UPDATE " + 
							     "KS_DRIFT.AHS_CONFIGURATION " +
							 "SET " +
							     "INDKALD = " + EColumn.KALD_INDGÅENDE.getTrueFalseValue() + ", " +
							     "UDKALD = " + EColumn.KALD_UDGÅENDE.getTrueFalseValue() + ", " +
							     "CONSULTKALD = " + EColumn.KALD_CONSULT.getTrueFalseValue() + ", " +
							     "CBRKALD = " + EColumn.KALD_CBR.getTrueFalseValue() + ", " +
							     "WEBDESKKALD = " + EColumn.KALD_WEBDESK.getTrueFalseValue() + ", " +
							     "KORRIGERET = " + EColumn.CTI_KORRIGERET.getTrueFalseValue() + ", " +
							     "TALER = " + EColumn.CTI_TALER.getTrueFalseValue() + ", " +     
							     "KLAR = " + EColumn.CTI_KLAR.getTrueFalseValue() + ", " +
							     "ØVRIG = " + EColumn.CTI_ØVRIG.getTrueFalseValue() + ", " +
							     "EMAIL = " + EColumn.CTI_EMAIL.getTrueFalseValue() + ", " +
							     "WEBDESK = " + EColumn.CTI_WEBDESK.getTrueFalseValue() + ", " +
							     "EFTERBEH = " + EColumn.CTI_EFTERBEHANDLING.getTrueFalseValue() + ", " +
							     "ANDREOPG = " + EColumn.CTI_ANDREOPGAVER.getTrueFalseValue() + ", " +     
							     "KOLLEGAHJ = " + EColumn.CTI_KOLLEGAHJÆLP.getTrueFalseValue() + ", " +     
							     "PAUSE = " + EColumn.CTI_PAUSE.getTrueFalseValue() + ", " +
							     "FROKOST = " + EColumn.CTI_FROKOST.getTrueFalseValue() + ", " +
							     "UDDANNELSE="+EColumn.CTI_UDDANNELSE.getTrueFalseValue()+", " +
							     "MØDE = " + EColumn.CTI_MØDE.getTrueFalseValue() + ", " +
							     "SPÆRRET = " + EColumn.CTI_SPÆRRET.getTrueFalseValue() + ", " +
							     "FORSIKRINGSP = " + EColumn.HISTORIK_FORSIKRINGSPRÆMIE.getTrueFalseValue() + ", " +
							     "MERSALG = " + EColumn.SALG_MERSALG.getTrueFalseValue()+", " +
							     "HENVISNINGER = " + EColumn.SALG_HENVISNINGER.getTrueFalseValue() + ", " +
							     "GENNEMFØRTSALG = " + EColumn.SALG_GENNEMFØRTESALG.getTrueFalseValue() + ", " +
							     "SALGSRATE = " + EColumn.SALG_SALGSRATE.getTrueFalseValue() + ", " +
							     "AKTEMAILIND = " + EColumn.AKTIVITETER_EMAILINDGÅENDE.getTrueFalseValue() + ", " +     
							     "AKTIVITETER = " + EColumn.AKTIVITETER_AFSLUTTET.getTrueFalseValue() + ", " +
							     "AKTIVITETEROPR = " + EColumn.AKTIVITETER_OPRETTET.getTrueFalseValue() + ", " +
							     "AKTAFSLINDENFOR2TIMERINKLMAIL = " + EColumn.AKTIVITETER_AFSLUTTETINDENFOR2TIMERINKLEMAIL.getTrueFalseValue() + ", " +
							     "AKTAFSLINDENFOR2TIMEREKSMAIL = " + EColumn.AKTIVITETER_AFSLUTTETINDENFOR2TIMEREXCLEMAIL.getTrueFalseValue() + ", " +
							     "AKTINDTID = " + EColumn.AKTIVITETER_INDENDFORTIDSFRIST.getTrueFalseValue() + ", " +
							     "AKTOVER = " + EColumn.AKTIVITETER_OVERSKREDET.getTrueFalseValue() + ", " +
							     "TLFBESK = " + EColumn.AKTIVITETER_TELEFONBESKEDER.getTrueFalseValue() + ", " +
							     "SUPPORTOPGAVER = " + EColumn.SUPPORTOPGAVER_AFSLUTTET.getTrueFalseValue() + ", " +
							     "SUPPORTOPGAVEROVERSKREDNE = " + EColumn.SUPPORTOPGAVER_OVERSKREDET.getTrueFalseValue() + ", " +
							     "SUPPORTOPGINDENFORFRIST = " + EColumn.SUPPORTOPGAVER_INDENFORFRIST.getTrueFalseValue() + ", " +
							     "OPRETTEDEKAMPAGNEPOTENTIALER = " + EColumn.AKTIVITETER_OPRETTEDEKAMPAGNEPOTENTIALER.getTrueFalseValue() + ", " +
							     "CALLBACKS = " + EColumn.AKTIVITETER_CALLBACKSHÅNDTERET.getTrueFalseValue() + ", " +
							     "AKTINDTIDBE = " + EColumn.BEREGNINGER_PROCENTAKTIVITETERINDFORTIDSFRIST.getTrueFalseValue() + ", " +
							     "AKTPRLOGT=" + EColumn.BEREGNINGER_AKTIVITETERPRLOGINTIME.getTrueFalseValue()+", " +
							     "KALDPRTIME = " + EColumn.BEREGNINGER_KALDPRLOGINTIME.getTrueFalseValue() + ", " +
							     "EMAILAKTPRLOGT=" + EColumn.BEREGNINGER_EMAILAKTIVITETERPRLOGINTIME.getTrueFalseValue() + "," +
							     "EMAILAKTPREMAILLOGT = " + EColumn.BEREGNINGER_EMAILAKTIVITETERPREMAILLOGINTIME.getTrueFalseValue() + ", " +
							     "AHT = " + EColumn.BEREGNINGER_AHT.getTrueFalseValue() + ", " +
							//     "KPI = " + EColumn.BEREGNINGER_KPI.getTrueFalseValue() + ", " +
								 "KPIWEBDESK = " + EColumn.BEREGNINGER_KPIWEBDESK.getTrueFalseValue() + ", " +
								 "KPISUPPORT = " + EColumn.BEREGNINGER_KPISUPPORT.getTrueFalseValue() + ", " +
						//		 "KPICONSULT = " + EColumn.BEREGNINGER_KPICONSULT.getTrueFalseValue() + ", " +
								 "PAUSEPCT = " + EColumn.BEREGNINGER_PAUSEPCTAFLOGINTIME.getTrueFalseValue() + ", " +
								 "TILGÆNGELIGHED = " + EColumn.BEREGNINGER_TILGÆNGELIGHED.getTrueFalseValue() + ", " +
								 "PLANLAGTTID = " + EColumn.BEREGNINGER_WFCMPLANLAGTTID.getTrueFalseValue() + ", " +
								 "PLANLAGTKUNDETID = " + EColumn.BEREGNINGER_WFCMPLANLAGTKUNDETID.getTrueFalseValue() + ", " +
								 "KAMPAGNEPOTENTIALERPREMAILTIME = " + EColumn.BEREGNINGER_KAMPAGNEPOTENTIALERPRTIME.getTrueFalseValue() + ", " +
								 "PROCENTVISMERSALG = " + EColumn.BEREGNINGER_PROCENTVISMERSALG.getTrueFalseValue() + ", " +
								 "WEBDESKTALERTID = " + EColumn.BEREGNINGER_WEBDESKTALERTID.getTrueFalseValue() + ", " +
								 "WEBDESKKALDPRWEBDESKTIME = " + EColumn.BEREGNINGER_WEBDESKKALDPRWEBDESKTIME.getTrueFalseValue() + ", " +
								 "OPFØLGNINGSSERVICEAFS = " + EColumn.EKSPEDITION_OPFØLGNINGSSERVICE.getTrueFalseValue() + "," +
								 "INDGÅENDEKALDDIALOG = " + EColumn.EKSPEDITION_INDGÅENDEKALD.getTrueFalseValue() + ", " +
								 "OPRETTEDEAKTIKKEVIDERE = " + EColumn.EKSPEDITION_OPRETTETAKTIVITETERIKKEVIDERESTILLING.getTrueFalseValue() + ", " +
								 "TLFBESKPCTINDKALD = " + EColumn.EKSPEDITION_TLFBESKEDERPCTINDKALD.getTrueFalseValue() + ", " +
								 "VIDERESTILLINGERPCTINDKALD = " + EColumn.EKSPEDITION_VIDERESTILLINGERPCTINDKALD.getTrueFalseValue() + ", " +
								 "HENVISNINGERPRLOGINTIME = " + EColumn.EKSPEDITION_HENVISNINGERPRLOGINTIME.getTrueFalseValue() + ", " +
								 "VIDERESTILLINGER = " + EColumn.EKSPEDITION_VIDERESTILLINGER.getTrueFalseValue() + ", " +
								 "SOLGTESERVICEPRODUKTER = " + EColumn.EKSPEDITION_SOLGTESERVICEPRODUKTER.getTrueFalseValue() + ", " +
								 "SERVICEPRODUKTERPRLOGINTIME = " + EColumn.EKSPEDITION_SERVICEPRODUKTERPRLOGINTIME.getTrueFalseValue() + ", " +
								 "TABTSALG = " + EColumn.EKSPEDITION_TABTSALG.getTrueFalseValue() + ", " +
								 "EMAILTILG = " + EColumn.HISTORIK_EMAILTILGÆNGELIGHED.getTrueFalseValue() + ", " +
								 "TLFTILG = " + EColumn.HISTORIK_TELEFONTILGÆNGELIGHED.getTrueFalseValue() + ", " +
								 "TILGÆNGELIGHED2011 = " + EColumn.HISTORIK_TILGÆNGELIGHED2011.getTrueFalseValue() + ", " +
								 "TILGÆNGELIGHED2012 = " + EColumn.HISTORIK_TILGÆNGELIGHED2012.getTrueFalseValue() + ", " +
								 "FÆRDIGGG = " + EColumn.HISTORIK_FÆRDIGGØRELSESGRAD.getTrueFalseValue() + ", " +
								 "SHOWAVERAGE = " + EColumn.SHOWAVERAGE.getTrueFalseValue() + ", " +
								 "SHOWSUBTOTAL = "+EColumn.SHOWSUBTOTAL.getTrueFalseValue()+", " +
								 "SHOWAGENTS = "+EColumn.SHOWAGENTS.getTrueFalseValue()+", " +
								 "HIDEEMPTYLOGIN = "+EColumn.HIDEEMPTYLOGIN.getTrueFalseValue()+"," +
								 "NOCOLORS = "+EColumn.NOCOLORS.getTrueFalseValue()+"," +						
								 "COLORINGKONCERN = "+EColumn.COLORINGKONCERN.getTrueFalseValue()+"," +
								 "COLORINGB2B8 = "+EColumn.COLORINGB1B6.getTrueFalseValue()+"," +
								 "COLORINGB9 = "+EColumn.COLORING_MS.getTrueFalseValue()+"," +
								 "COLORINGNETBANK = " + EColumn.COLORINGNETBANK.getTrueFalseValue() + ", " +
								 "COLORINGFORSIKRING = " + EColumn.COLORINGFORSIKRING.getTrueFalseValue() + ", " +
								 "COLORINGERHVERV = " + EColumn.COLORINGERHVERV.getTrueFalseValue() + ", " +
								 "COLORINGSUPPORT = " + EColumn.COLORINGSUPPORT.getTrueFalseValue() + ", " +
								 "COLORINGE1E2 = " + EColumn.COLORINGE1E2.getTrueFalseValue() + " " +
							 "WHERE " +
							 	 "INITIALER = '" + initialer + "'";
				this.Hent_intet(sql);
			} catch (SQLException e1) {
				System.out.println("Der er en fejl i Data.java linie 173-431");	
			}
		}
	}

	public ArrayList<DataTableColumns> getAgentCbr(String fromDate, String toDate,ArrayList<DataTableColumns> dataTable){
			String sqlQuery = "SELECT " +
							      "INITIALER, " +
							      "TEAM, " +
							      "SUM(\"Korrigeret\") AS KORRIGERET, " +
							      "SUM(\"Ind kald\") AS INDGÅENDEKALD, " +
							      "SUM(\"Ud kald\") AS UDGÅENDEKALD, " +
							      "SUM(\"Consult\") AS CONSULTKALD, " +
							      "SUM(\"Klar\") AS KLAR, " +
							      "SUM(\"Taler\") AS TALERTID, " +
							      "SUM(\"Øvrig tid\") AS ØVRIGTID, " +
							      "SUM(\"Webdesk\") AS WEBDESKTID, " +
							      "SUM(\"E-mail\") AS EMAILTID, " +
							      "SUM(\"Efterbeh\") AS EFTERBEHANDLINGTID, " +
							      "SUM(\"Andre opg\") AS ANDREOPGAVERTID, " +
							      "SUM(\"Møde\") AS MØDETID, " +
							      "SUM(\"Uddannelse\") AS UDDANNELSETID, " +
							      "SUM(\"Kollegahj\") AS KOLLEGAHJÆLPTID, " +
							      "SUM(\"Frokost\") AS FROKOSTTID, " +
							      "SUM(\"Pause\") AS PAUSETID, " +
							      "SUM(\"Spærret\") AS SPÆRRETTID, " +
							      "SUM(CBR) AS CBR " +
							  "FROM " +
							  	  "KS_DRIFT.PERO_AGENT_CBR " +
							  "INNER JOIN " +
							  	  "KS_DRIFT.V_TEAM_DATO " +
							  "ON " +
							  	  "\"Initialer\" = INITIALER AND \"Dato\" = DATO " +
							  "WHERE " +
							  	  "DATO >= '" + fromDate + "' AND DATO <= '" + toDate + "' AND INITIALER IN (";
			
			ResultSet rs = runsql(getInitials(sqlQuery, dataTable));
			
			try {
				while(rs.next())
					for(DataTableColumns a: dataTable)
						if(a.getInitials().equals(rs.getString("INITIALER")) && a.getTeam().equals(rs.getString("TEAM"))){
							a.setAndreOpg(rs.getDouble("ANDREOPGAVERTID"));
							a.setCbr(rs.getDouble("CBR"));
							a.setConsult(rs.getDouble("CONSULTKALD"));
							a.setEfterbeh(rs.getDouble("EFTERBEHANDLINGTID"));
							a.setEmail(rs.getDouble("EMAILTID"));
							a.setFrokost(rs.getDouble("FROKOSTTID"));
							a.setIndKald(rs.getDouble("INDGÅENDEKALD"));
							a.setKlar(rs.getDouble("KLAR"));
							a.setKollegahj(rs.getDouble("KOLLEGAHJÆLPTID"));
							a.setKorrigeret(rs.getDouble("KORRIGERET"));
							a.setMøde(rs.getDouble("MØDETID"));
							a.setPause(rs.getDouble("PAUSETID"));
							a.setSpærret(rs.getDouble("SPÆRRETTID"));
							a.setTaler(rs.getDouble("TALERTID"));
							a.setUddannelse(rs.getDouble("UDDANNELSETID"));
							a.setUdKald(rs.getDouble("UDGÅENDEKALD"));
							a.setWebdesk(rs.getDouble("WEBDESKTID"));
							a.setØvrigTid(rs.getDouble("ØVRIGTID"));
						}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			return dataTable;
		}
	
	private String getInitials(String sql, ArrayList<DataTableColumns> dataTable){
		for(DataTableColumns s: dataTable)
			
			sql += "'" + s.getInitials() + "',";
		
		sql = sql.substring(0, sql.length() - 1);
		sql += ") GROUP BY TEAM,INITIALER";
		return sql;
	}
	
	public ArrayList<DataTableColumns> getCallbacks(String fromDate, String toDate, ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT " +
							  "INITIALER, " +
							  "TEAM, " +
							  "COUNT(*) AS CALLBACKS " +
						  "FROM " +
							  "KS_DRIFT.NYK_SIEBEL_CALLBACK_AGENT_H_V cb " +
						  "INNER JOIN " +
							  "KS_DRIFT.V_TEAM_DATO vt ON cb.INITIALS = vt.INITIALER AND trunc(LAST_UPD) = vt.DATO " +
						  "WHERE " +
						  	  "DATO >= '" + fromDate + "' AND DATO <= '" + toDate + "' AND INITIALER IN (";
				
		ResultSet rs = this.runsql(this.getInitials(sqlQuery, dataTable));
		
		try{
			while(rs.next())
				for(DataTableColumns a : dataTable)
					if(a.getInitials().equals(rs.getString("INITIALER")))
						a.setCallbacks(rs.getInt("CALLBACKS"));
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		return dataTable;
	}
	
	public ArrayList<DataTableColumns> getOprettedePotentialer (String fromDate, String toDate,ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT " +
							  "TEAM, " +
							  "INITIALER, " +
							  "COUNT(CASE WHEN \"Produkt\" IN ('Henvisning til Privat','Henvisning til Salgscentret','Henvisning til Erhverv','Henvisning til Direkte', 'Henvisning til Retail') THEN 1 END) AS HENVISNINGERQ12013, " +
							  "COUNT(CASE WHEN \"Salgstrin\" = '08 - Tabt salg' THEN 1 END) AS TABTSALG, " +
							  "COUNT(CASE WHEN \"Responsform\" = 'Internet/E-mail' THEN 1 END) AS KAMPAGNEPOTENTIALER, " +
							  "COUNT(CASE WHEN \"Produkt\" = 'Internetbank' AND \"Salgstrin\" IN ('01 - Muligt emne', '02 - Aktivt emne', '03 - Udtrykt behov', '04 - Behovsafdækning') THEN 1 END) AS SELVBETJENINGSPRODUKTER, " +
							  "COUNT(CASE WHEN \"Salgstrin\" IN ('03 - Udtrykt behov', '04 - Behovsafdækning', '05 - Aftalegrundlag', '06 - Accept', '07 - Gennemført salg', '08 - Tabt salg') AND \"Ansvarlig Region\" <> 'Servicecenter' AND NVL(\"Responsform\", 'X') <> 'Internet/E-mail' AND \"Oprettet dato\" > '2013-04-01' AND NVL(\"Produkt\", 'X') <> 'Mødeservice' THEN 1 END) AS HENVISNINGER " +						
						  "FROM " +
						  	  "KS_DRIFT.DIAN_POT_OPR " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.V_TEAM_DATO " +
						  "ON " +
						  	  "\"Oprettet af Initialer\" = INITIALER AND \"Oprettet dato\" = DATO " +		
						  "WHERE " +
						  	  "DATO >= '" + fromDate + "' AND DATO <= '" + toDate + "' AND INITIALER IN (";							
		
		ResultSet rs = this.runsql(this.getInitials(sqlQuery, dataTable));
		
		try {
			while(rs.next())
				for(DataTableColumns a: dataTable)
					if(a.getInitials().equals(rs.getString("INITIALER")) && a.getTeam().equals(rs.getString("TEAM"))){
						a.setHenvisningerQ12013(rs.getDouble("HENVISNINGERQ12013"));
						a.setTabtSalg(rs.getInt("TABTSALG"));
						a.setKampagnePotentialer(rs.getInt("KAMPAGNEPOTENTIALER"));
						a.setTilbudteSelvbetjeningsprodukter(rs.getInt("SELVBETJENINGSPRODUKTER"));
						a.setHenvisninger(rs.getInt("HENVISNINGER"));
					}	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return dataTable;
	}
	
	public ArrayList<DataTableColumns> getOprettedeAktiviteter (String fromDate, String toDate,ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT " +
						      "TEAM, " +
						      "INITIALER, " +
						      "COUNT(*) AS OPRETTET, " +
						      "COUNT(CASE WHEN \"Aktivitetstype\" = 'Telefonbesked' THEN 1 END) AS TLFBESK " +
						  "FROM " +
						  	  "KS_DRIFT.DIAN_AKT_OPR_KS " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.V_TEAM_DATO " +
						  "ON " +
						  	  "\"Oprettet Dato\" = DATO AND \"Oprettet af Initialer\" = INITIALER " +
						  "WHERE " +
						  	   "DATO> = '" + fromDate + "' AND DATO <= '" + toDate + "' AND INITIALER IN (";
		
		ResultSet rs = runsql(getInitials(sqlQuery,dataTable));
		
		try {
			while(rs.next())				
				for(DataTableColumns a: dataTable)
					if(a.getInitials().equals(rs.getString("INITIALER")) && a.getTeam().equals(rs.getString("TEAM"))){
						a.setAktiviteterOprettet(rs.getInt("OPRETTET"));
						a.setTlfBesk(rs.getDouble("TLFBESK"));
					}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return dataTable;
	}
	
	public ArrayList<DataTableColumns> getFærdiggørelsesgrad (String fromDate, String toDate, ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT " +
							  "TEAM, " +
							  "INITIALER, " +
							  "SUM(\"First Call\") AS FIRSTCALL, " +
							  "SUM(\"Kald i alt\") AS IALT " +
						  "FROM " +
						  	  "KS_DRIFT.PERO_FP_AGENT FP " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.V_TEAM_DATO VT " +
						  "ON " +
						  	  "FP.\"Dato\" = VT.DATO AND FP.\"Initialer\" = VT.INITIALER " +
						  "WHERE " +
						  	  "DATO >= '" + fromDate + "' AND DATO <= '" + toDate + "' AND INITIALER IN (";
		
		ResultSet rs = runsql(getInitials(sqlQuery,dataTable));
		
		try {
			while(rs.next())				
				for(DataTableColumns a: dataTable)
					if(a.getInitials().equals(rs.getString("INITIALER")) && a.getTeam().equals(rs.getString("TEAM"))){
						a.setFirstCall(rs.getDouble("FIRSTCALL"));
						a.setKaldIalt(rs.getDouble("IALT"));
					}	
		} catch (SQLException e) {
			e.printStackTrace();
			}
		
		return dataTable;
	}
	
	public ArrayList<DataTableColumns> getGennemførtePotentialer (String fromDate, String toDate,ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT " +
							  "TEAM, " +
							  "INITIALER, " +
							  "SUM(\"Bruttobeløb\") AS FORSIKRINGSPRÆMIE, " +
							  "NVL(SUM(CASE WHEN \"Salgsanledning\" = 'Mersalg' THEN \"Bruttobeløb\" END), 0) AS MERSALG " +
						  "FROM " +
						  	  "KS_DRIFT.DIAN_POT_07 " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.V_TEAM_DATO " +
						  "ON " +
						  	  "\"Ansvarlig Initialer\" = INITIALER AND \"Dato salgstrin\" = DATO " +
						  "WHERE " +
						  	  "\"Produktlinje\" = 'Forsikring' AND DATO >= '" + fromDate + "' AND DATO <= '" + toDate + "' AND INITIALER IN (";
		
		ResultSet rs = runsql(getInitials(sqlQuery,dataTable));
		
		try {
			while(rs.next())
				for(DataTableColumns a: dataTable)
					if(a.getInitials().equals(rs.getString("INITIALER")) && a.getTeam().equals(rs.getString("TEAM"))){
						a.setForsikringspræmie(rs.getDouble("FORSIKRINGSPRÆMIE"));
						a.setMersalg(rs.getInt("MERSALG"));
					}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return dataTable;
	}
	
	public ArrayList<DataTableColumns> getGennemførteSalg(String fromDate, String toDate, ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT " + 
						      "vt.TEAM, " +
						      "vt.INITIALER, " +
						      "COUNT(*) AS GENNEMFØRTSALG " +
						  "FROM " + 
						  	  "KS_DRIFT.DIAN_POT_07 pot " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.V_TEAM_DATO vt ON pot.\"Oprettet af Initialer\" = vt.INITIALER AND trunc(pot.\"Oprettet dato\") = vt.DATO " +
						  "WHERE " +
						  	  "\"Ansvarlig Region\" <> 'Servicecenter' AND " +
						  	  "NVL(\"Responsform\", 'X') <> 'Internet/E-mail' AND " +
						  	  "\"Dato salgstrin\" - \"Oprettet dato\" <= 45 AND " +
						  	  "\"Oprettet dato\" > '2013-04-01' AND (NVL(\"Produkt\", 'X') <> 'Mødeservice') AND  " +
						  	  "TRUNC(\"Oprettet dato\") BETWEEN '" + fromDate + "' AND '" + toDate + "' AND " +
						  	  "vt.INITIALER IN (";
		
		ResultSet rs = runsql(getInitials(sqlQuery, dataTable));
		
		try{
			while(rs.next())
				for(DataTableColumns a : dataTable)
					if(a.getInitials().equals(rs.getString("INITIALER")) && a.getTeam().equals(rs.getString("TEAM")))
						a.setGennemførtSalg(rs.getDouble("GENNEMFØRTSALG"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		return dataTable;
	}
	public ArrayList<DataTableColumns> getGennemførteSalgHistorik(String fromDate, String toDate, ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT " + 
						      "vt.TEAM, " +
						      "vt.INITIALER, " +
						      "COUNT(*) AS GENNEMFØRTSALG " +
						  "FROM (" + 
						  	  "SELECT " +  
						  	  	  "TRUNC(opr.\"Oprettet dato/tid\") AS OPRETTET_DATO_TID, " + 
						  	  	  "opr.\"Oprettet af Initialer\" AS OPRETTET_AF, " + 
						  	  	  "opr.\"Produkt\" AS HENVISNING, " + 
						  	  	  "opr.\"CPR-/CVR-nummer\" AS KUNDE_ID " +
						  	  "FROM " + 
						  	  	  "KS_DRIFT.DIAN_POT_07 pot " +
						  	  "RIGHT OUTER JOIN " + 
						  	  	  "KS_DRIFT.DIAN_POT_OPR opr ON pot.\"CPR-/CVR-nummer\" = opr.\"CPR-/CVR-nummer\" " +
						  	  "RIGHT OUTER JOIN " + 
						  	  	  "KS_DRIFT.V_TEAM_DATO vt ON vt.DATO = opr.\"Oprettet dato\" AND vt.INITIALER = opr.\"Oprettet af Initialer\" " + 
						  	  "WHERE " + 
						  	  	  "opr.\"Produkt\" in ('Henvisning til Direkte','Henvisning til Privat','Henvisning til Retail','Henvisning til Salgscentret') AND " + 
						  	  	  "pot.\"Oprettet af Region\" not in ('Kundeservice','Servicecenter') AND " + 
						  	  	  "pot.\"Produkt\" not in ('Henvisning til Direkte','Henvisning til Privat','Henvisning til Retail','Henvisning til Salgscentret') AND " + 
						  	  	  "pot.\"Dato salgstrin\" - opr.\"Oprettet dato\" BETWEEN 0 AND 45 " +
						  	  "GROUP BY " +
						  	  	  "TRUNC(opr.\"Oprettet dato/tid\"), " +
						  	  	  "opr.\"Oprettet af Initialer\", " +
						  	  	  "opr.\"Produkt\", " +
						  	  	  "opr.\"CPR-/CVR-nummer\") pot " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.V_TEAM_DATO vt ON vt.DATO = trunc(pot.OPRETTET_DATO_TID) AND vt.INITIALER = pot.OPRETTET_AF " +
						  "WHERE " +
						  	  "trunc(OPRETTET_DATO_TID) >= '" + fromDate + "' AND trunc(OPRETTET_DATO_TID) <= '" + toDate + "' AND INITIALER IN (";
		
		ResultSet rs = runsql(getInitials(sqlQuery, dataTable));
		
		try{
			while(rs.next())
				for(DataTableColumns a : dataTable)
					if(a.getInitials().equals(rs.getString("INITIALER")) && a.getTeam().equals(rs.getString("TEAM")))
						a.setGennemførtSalgQ12013(rs.getDouble("GENNEMFØRTSALG"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		return dataTable;
	}
	
	public ArrayList<DataTableColumns> getWebdeskData(String fromDate, String toDate, ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT " + 
				  			  "vt.TEAM, " +
							  "vt.INITIALER, " +
							  "COUNT(*) AS WEBDESKKALD, " +
							  "SUM(web.DURATION / 60 / 60) AS WEBDESKKALDTID " +
						  "FROM " + 
						      "KS_DRIFT.V_WEBDESK_SERVICECENTER web " + 
						  "INNER JOIN " + 
						      "KS_DRIFT.V_TEAM_DATO vt " + 
						  "ON " + 
						  	  "web.INITIALS = vt.INITIALER AND trunc(web.DATETIME) = vt.DATO " +
						  "WHERE " + 
						      "trunc(web.DATETIME) >= '" + fromDate + "' AND trunc(web.DATETIME) <= '" + toDate + "' AND " +
						      "vt.INITIALER IN (";
		
		ResultSet rs = runsql(getInitials(sqlQuery, dataTable));
		
		try {
			while(rs.next())
				for(DataTableColumns a: dataTable)
					if(a.getInitials().equals(rs.getString("INITIALER")) && a.getTeam().equals(rs.getString("TEAM"))){
						a.setWebdeskKald(rs.getInt("WEBDESKKALD"));
						a.setWebdeskTalerTid(rs.getDouble("WEBDESKKALDTID"));
					}	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataTable;
	}
		
	public ArrayList<DataTableColumns> getAfsluttedeAktiviteter(String fromDate, String toDate,ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT " +
							  "TEAM, " +
							  "INITIALER, " +
							  "COUNT(*) AS AFSLUTTET, " +
							  "COUNT(CASE WHEN \"Aktivitetstype\" = 'Mødeservice' THEN 1 END ) AS MEETINGREFERENCE, "+
							  "COUNT(CASE WHEN \"Faktisk afsluttet dato\" <= \"Senest d\" AND (\"Faktisk afslutning\" - \"Oprettet dato\" > (2 / 24) OR  \"Oprettet af Initialer\" <> \"Ansvarlig Initialer\") AND \"Aktivitetstype\" NOT IN ('Opfølgningservice', 'Email - Indgående') THEN 1 END) AS INDENFOR, " +
							  "COUNT(CASE WHEN \"Faktisk afsluttet dato\" > \"Senest d\" AND \"Aktivitetstype\" NOT IN ('Opfølgningservice', 'Email - Indgående') THEN 1 END) AS OVERSKREDET, " +
							  "COUNT(CASE WHEN \"Aktivitetstype\" = 'Email - Indgående' THEN 1 END) AS EMAILAKTIVITETER, " +
							  "COUNT(CASE WHEN \"Aktivitetstype\" = 'Opfølgningservice' THEN 1 END) AS OPFØLGNINGSSERVICEAFSLUTTET " +	
	        			  "FROM " +
	        			  	  "KS_DRIFT.DIAN_AKT_AFS_KS " +
	        			  "INNER JOIN " +
	        			  	  "KS_DRIFT.V_TEAM_DATO " +
	        			  "ON " +
	        			  	  "\"Faktisk afsluttet dato\" = DATO AND \"Ansvarlig Initialer\" = INITIALER " +
	        			  "WHERE " +
	        			  	  "DATO >= '" + fromDate + "' AND DATO <= '" + toDate + "' AND INITIALER IN (";
		ResultSet rs = runsql(getInitials(sqlQuery,dataTable));
		
		try {
			while(rs.next())
				for(DataTableColumns a: dataTable)
					if(a.getInitials().equals(rs.getString("INITIALER")) && a.getTeam().equals(rs.getString("TEAM"))){
						a.setMeetingReference(rs.getDouble("MEETINGREFERENCE"));
						a.setAktiviteterAfsluttet(rs.getDouble("AFSLUTTET"));
						a.setIndenforTidsfrist(rs.getInt("INDENFOR"));
						a.setOverskredne(rs.getInt("OVERSKREDET"));
						a.setEmailIndgående(rs.getDouble("EMAILAKTIVITETER"));
						a.setOpfølgningsserviceAfsluttet(rs.getInt("OPFØLGNINGSSERVICEAFSLUTTET"));						
					}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return dataTable;
	}
	
	public ArrayList<DataTableColumns> getSelectedAgents(String whereClause, String fromDate, String toDate,ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT DISTINCT " +
							  "VT.INITIALER, " +
							  "VT.TEAM,	" +
							  "AT.TEAM_NAVN " +
						  "FROM " +
						  	  "KS_DRIFT.V_TEAM_DATO VT " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.AGENTER_TEAMS AT " +
						  "ON " +
						  	  "VT.TEAM=AT.TEAM " +
						  "WHERE " +
						  	  "VT.DATO BETWEEN '" + fromDate + "' AND	'" + toDate + "' AND " + whereClause + " " +
						  "ORDER BY " +
						  	  "2, 1";
			
		ResultSet rs = runsql(sqlQuery);
		
		try {
			while(rs.next())
				dataTable.add(new DataTableColumns(rs.getString("INITIALER"),rs.getString("TEAM"),rs.getString("TEAM_NAVN")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return dataTable;
	}
	
	public ArrayList<DataTableColumns> getB2B8 (String fromDate, String toDate,ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT DISTINCT " +
							  "VT.INITIALER, " +
							  "VT.TEAM, " +
							  "AT.TEAM_NAVN " +
						  "FROM " +
						  	  "KS_DRIFT.V_TEAM_DATO VT " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.AGENTER_TEAMS AT " +
						  "ON " +
						  	  "VT.TEAM = AT.TEAM " +
						  "WHERE " +
						  	  "VT.TEAM IN ('B2','B3','B4','B5','B6','B7','B8') AND DATO BETWEEN '" + fromDate + "' AND '" + toDate + "'";
			
		ResultSet rs = runsql(sqlQuery);
		
		try {
			while(rs.next())
				dataTable.add(new DataTableColumns(rs.getString("INITIALER"),rs.getString("TEAM"),rs.getString("TEAM_NAVN")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return dataTable;
	}
	
	public ArrayList<DataTableColumns> getPlanlagtTid(String fromDate, String toDate,ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT " +
							  "VTD.TEAM AS TEAM, " +
							  "VTD.INITIALER AS INITIALER, " +
							  "SUM(SCH.DURATION) / 60 / 60 / 24 AS PLANLAGTTID " +
						  "FROM " +
						  	  "KS_DRIFT.WFCM_SCHEDULE SCH " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.WFCM_TYPES TYP " +
						  "ON " +
						  	  "SCH.CLASS = TYP.CLASS AND " +
						  	  "SCH.TYPE = TYP.TYPE " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.V_TEAM_DATO VTD " +
						  "ON	" +
						  	  "SCH.DATO=VTD.DATO AND " +
						  	  "SCH.INITIALER = VTD.INITIALER " +
						  "WHERE " +
						  	  "VTD.DATO >= '" + fromDate + "' AND VTD.DATO <= '" + toDate + "' AND " +
						  	  "\"Planlagt tid\" = 1 AND " +
						  	  "DATO>= '" + fromDate + "' AND DATO<= '" + toDate + "' AND VTD.INITIALER IN (";
		
		ResultSet rs = runsql(getInitials(sqlQuery,dataTable));
		
		try {
			while(rs.next())				
				for(DataTableColumns a: dataTable)
					if(a.getInitials().equals(rs.getString("INITIALER")) && a.getTeam().equals(rs.getString("TEAM")))
						a.setPlanlagttid(rs.getDouble("PLANLAGTTID"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return dataTable;
	}
	
	public ArrayList<DataTableColumns> getPlanlagtKundeTid(String fromDate, String toDate,ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT " +
							  "VTD.TEAM AS TEAM, " +
							  "VTD.INITIALER AS INITIALER, " +
							  "SUM(SCH.DURATION) / 60 / 60 / 24 AS PLANLAGTKUNDETID " +
						  "FROM " +
						  	  "KS_DRIFT.WFCM_SCHEDULE SCH " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.WFCM_TYPES TYP " +
						  "ON " +
						  	  "SCH.CLASS = TYP.CLASS AND " +
						  	  "SCH.TYPE = TYP.TYPE " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.V_TEAM_DATO VTD " +
						  "ON	" +
						  	  "SCH.DATO=VTD.DATO AND " +
						  	  "SCH.INITIALER = VTD.INITIALER " +
						  "WHERE " +
						  	  "VTD.DATO >= '" + fromDate + "' AND VTD.DATO <= '" + toDate + "' AND " +
						  	  "\"Planlagt kundetid\" = 1 AND " +
						  	  "DATO>= '" + fromDate + "' AND DATO<= '" + toDate + "' AND VTD.INITIALER IN (";
		
		ResultSet rs = runsql(getInitials(sqlQuery,dataTable));

		
		try {
			while(rs.next())				
				for(DataTableColumns a: dataTable)
					if(a.getInitials().equals(rs.getString("INITIALER")) && a.getTeam().equals(rs.getString("TEAM")))
						a.setPlanlagtKundetid(rs.getDouble("PLANLAGTKUNDETID"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return dataTable;
	}	
	
	public ArrayList<DataTableColumns> getIndgåendeKaldDialog(String fromDate, String toDate,ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT " +
							  "TEAM, " +
							  "INITIALER, " +
							  "SUM(\"Antal aktiviteter\") AS INDGÅENDEKALDDIALOG " +
						  "FROM " +
						  	  "KS_DRIFT.DIAN_AKT_AFS_KS " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.V_TEAM_DATO " +
						  "ON " +
						  	  "\"Oprettet af Initialer\"=INITIALER AND trunc(\"Oprettet dato\")=DATO " +
						  "WHERE " +
						  	  "\"Aktivitetstype\" IN ('Indgående kald', 'Telefonbesked','Send brev','Udfør') AND " +
						  	  "DATO >= '" + fromDate + "' AND DATO <= '" + toDate + "' AND INITIALER IN (";
		
		ResultSet rs = runsql(getInitials(sqlQuery,dataTable));
		
		try {
			while(rs.next())
				for(DataTableColumns a: dataTable)
					if(a.getInitials().equals(rs.getString("INITIALER")) && a.getTeam().equals(rs.getString("TEAM")))
						a.setIndgåendeKaldDialog(rs.getDouble("INDGÅENDEKALDDIALOG"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return dataTable;
	}
	
	private ResultSet runsql(String sqlQuery){
		ResultSet rs = null;
		try {rs = Hent_tabel(sqlQuery);}
		catch (Exception e) {e.printStackTrace();}
		return rs;
	}

	public ArrayList<DataTableColumns> getSupportOpgaver(String fromDate, String toDate,ArrayList<DataTableColumns> dataTable){
		String sqlQuery = "SELECT " +
							  "VT.TEAM, " +
							  "VT.INITIALER, " +
							  "COUNT(*) AS AFSLUTTEDE, " +
							  "COUNT(CASE WHEN DAT.COMPLETED_DATE <= DAT.DEADLINE THEN 1 END) AS INDENFOR " +
						  "FROM " +
						  	  "KS_DRIFT.SOP_TASK_DATA DAT " +
						  "INNER JOIN " +
						  	  "KS_DRIFT.V_TEAM_DATO VT " +
						  "ON " +
						  	  "DAT.COMPLETED_BY = VT.INITIALER AND " + 
						  	  "DAT.COMPLETED_DATE = VT.DATO " +
						  "WHERE " +
					  	  	  "DELETED = 0 AND " + 
					  	  	  "DAT.COMPLETED_DATE >= '" + fromDate + "' AND " + 
					  	  	  "DAT.COMPLETED_DATE <= '" + toDate + "' AND " +
					  	  	  "DAT.TASK_ID NOT IN (3, 4, 5, 20, 23, 25, 26, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 65, 67, 70, 83, 84, 85, 87, 99) AND " +
					  	  	  "VT.INITIALER IN (";
					
		ResultSet rs = runsql(getInitials(sqlQuery,dataTable));
	
		try {
			while(rs.next())
				for(DataTableColumns a: dataTable)
					if(a.getInitials().equals(rs.getString("INITIALER")) && a.getTeam().equals(rs.getString("TEAM"))){
						a.setSupportOpgaverAfsluttet(rs.getDouble("AFSLUTTEDE"));
						a.setSupportIndenforFrist(rs.getInt("INDENFOR"));
					}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		return dataTable;
	} 
	public ArrayList<DataTableColumns> getMødeService(String fromDate, String toDate,ArrayList<DataTableColumns> dataTable){
	
		String sqlQuery = "SELECT   \"Oprettet af Initialer\", \"Responsform\", COUNT(\"Oprettet af Initialer\") AS meetings_booked, \"Oprettet af Region\", V_T.TEAM "+
					      "FROM     KS_DRIFT.V_TEAM_DATO V_T "+
							      "RIGHT OUTER JOIN KS_DRIFT.DIAN_POT_OPR DIA ON V_T.INITIALER=DIA.\"Oprettet af Initialer\" AND V_T.DATO=DIA.\"Oprettet dato\" "+ 
							"WHERE     \"Salgstrin\" NOT IN ('01 - Muligt emne','02 - Aktivt emne') "+
							"AND 	 \"Produkt\" = 'Mødeservice' "+
							"AND      \"Oprettet dato\" BETWEEN '"+fromDate +"' AND '"+toDate +"' "+ 
							"AND	  \"Responsform\"  IS NULL "+ 
							"GROUP BY \"Oprettet af Initialer\", \"Responsform\", \"Oprettet af Region\", V_T.TEAM ";
		

		ResultSet rs = runsql(sqlQuery);
		try {
			while (rs.next()) {
				for (DataTableColumns a : dataTable) {
					if(a.getInitials().equals(rs.getString("Oprettet af Initialer")) && a.getTeam().equals(rs.getString("TEAM"))){
						a.setMeetingsBooked(rs.getDouble("meetings_booked"));
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return dataTable;
	}
}
