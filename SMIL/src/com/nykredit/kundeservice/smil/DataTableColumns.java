package com.nykredit.kundeservice.smil;

import java.text.DecimalFormat;

import com.nykredit.kundeservice.util.Formatter;
import com.nykredit.kundeservice.util.Date;

public class DataTableColumns {

	private Formatter bf = new Formatter();
	private Date date = new Date();

	private double kald_Indgående = 0, 
				   kald_Udgående = 0, 
				   kald_Consult = 0,
				   kald_Cbr = 0, 
				   kald_Webdesk = 0, 
				   cti_KorrigeretLogin = 0,
				   cti_Taler = 0, 
				   cti_Klar = 0, 
				   cti_ØvrigTid = 0, 
				   cti_Email = 0,
				   cti_Webdesk = 0, 
				   cti_Efterbehandling = 0, 
				   cti_AndreOpgaver = 0,
				   cti_Kollegahjælp = 0, 
				   cti_Pause = 0, 
				   cti_Frokost = 0,
				   cti_Uddannelse = 0, 
				   cti_Møde = 0, 
				   cti_Spærret = 0,
				   salg_Forsikringspræmie = 0, 
				   salg_Mersalg = 0,
				   salg_Henvisninger = 0,
				   salg_GennemførteSalg = 0,
				   aktiviteter_EmailIndgående = 0, 
				   aktiviteter_Afsluttet = 0,
				   aktiviteter_Oprettet = 0,
				   aktiviteter_IndenforTidsfrist = 0, 
				   aktiviteter_Overskredne = 0,
				   aktiviteter_TelefonBeskeder = 0, 
				   Aktiviteter_callbacks = 0,
				   supportopgaver_Afsluttet = 0,
				   supportopgaver_IndenforFrist = 0,
				   aktiviteter_OprettedeKampagnePotentialer = 0,
				   beregninger_Planlagttid = 0, 
				   beregninger_Planlagtkundetid = 0,
				   beregninger_WebdeskTalerTid = 0,
				   ekspedition_OpfølgningsserviceAfsluttet = 0,
				   ekspedition_IndgåendeKaldDialog,
				   ekspedition_Serviceprodukter = 0,
				   ekspedition_TabtSalg = 0,
				   historik_HenvisningerQ12013 = 0, 
				   historik_GennemførtSalgQ12013 = 0,
				   færdiggørelsesgrad = 0, 
				   kaldIalt,
				   firstCall, 
				   firstCallUdenTelefonBeskeder = 0,
				   meetingsBooked = 0,
				   meetingReference = 0;
				
	private String initials, team, teamNavn;

	public DataTableColumns() {
	}

	public DataTableColumns(String initials, String team, String teamNavn) {
		this.initials = initials;
		this.team = team;
		this.teamNavn = teamNavn;
	}

	public String getInitials() {
		return initials;
	}
	public void setInitials(String initials) {
		this.initials = initials;
	}
	
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	
	public String getTeamNavn() {
		return teamNavn;
	}
	public void setTeamNavn(String teamNavn) {
		this.teamNavn = teamNavn;
	}
	
	// ***************************** KALD *****************************
	
	public double getIndgåendeKald() {
		return kald_Indgående;
	}
	public String getIndgåendeKaldFormateret() {
		return bf.KiloDotFill(kald_Indgående, false);
	}
	public void setIndKald(double indKald) {
		this.kald_Indgående = indKald;
	}
	
	public double getUdKald() {
		return kald_Udgående;
	}
	public String getUdKaldFormateret() {
		return bf.KiloDotFill(kald_Udgående, false);
	}
	public void setUdKald(double udKald) {
		this.kald_Udgående = udKald;
	}
	
	public double getWebdeskKald() {
		return this.kald_Webdesk;
	}
	public String getWebdeskKaldFormatted() {
		return bf.KiloDotFill(this.kald_Webdesk, false);
	}
	public void setWebdeskKald(double webdeskKald) {
		this.kald_Webdesk = webdeskKald;
	}
	
	// ***************************** CTI *****************************

	public double getAndreOpg() {
		return cti_AndreOpgaver;
	}
	public String getAndreOpgFormateret() {
		return date.toTTMM(getAndreOpg(), false);
	}
	public void setAndreOpg(double andreOpg) {
		this.cti_AndreOpgaver = andreOpg;
	}

	public double getCbr() {
		return kald_Cbr;
	}
	public String getCbrFormateret() {
		return bf.KiloDotFill(this.getCbr(), false);
	}
	public void setCbr(double cbr) {
		this.kald_Cbr = cbr;
	}

	public double getConsult() {
		return kald_Consult;
	}
	public String getConsultFormateret() {
		return bf.KiloDotFill(this.getConsult(), false);
	}
	public void setConsult(double consult) {
		this.kald_Consult = consult;
	}

	public double getEfterbehandling() {
		return cti_Efterbehandling;
	}
	public String getEfterbehandlingFormateret() {
		return date.toTTMM(this.getEfterbehandling(), false);
	}
	public void setEfterbeh(double efterbeh) {
		this.cti_Efterbehandling = efterbeh;
	}

	public double getEmail() {
		return cti_Email;
	}
	public String getEmailFormateret() {
		return date.toTTMM(this.getEmail(), false);
	}
	public void setEmail(double email) {
		this.cti_Email = email;
	}

	public double getFrokost() {
		return cti_Frokost;
	}
	public String getFrokostFormateret() {
		return date.toTTMM(this.getFrokost(), false);
	}
	public void setFrokost(double frokost) {
		this.cti_Frokost = frokost;
	}

	public double getMøde() {
		return cti_Møde;
	}
	public String getMødeFormateret() {
		return date.toTTMM(this.getMøde(), false);
	}
	public void setMøde(double møde) {
		this.cti_Møde = møde;
	}

	public double getKlar() {
		return cti_Klar;
	}
	public String getKlarFormateret() {
		return date.toTTMM(this.getKlar(), false);
	}
	public void setKlar(double klar) {
		this.cti_Klar = klar;
	}

	public double getKollegahj() {
		return cti_Kollegahjælp;
	}
	public String getKollegahjFormateret() {
		return date.toTTMM(this.getKollegahj(), false);
	}
	public void setKollegahj(double kollegahj) {
		this.cti_Kollegahjælp = kollegahj;
	}

	public double getKorrigeret() {
		return cti_KorrigeretLogin;
	}
	public String getKorrigeretFormateret() {
		return date.toTTMM(this.getKorrigeret(), false);
	}
	public void setKorrigeret(double korrigeret) {
		this.cti_KorrigeretLogin = korrigeret;
	}

	public double getPause() {
		return cti_Pause;
	}
	public String getPauseFormateret() {
		return date.toTTMM(this.getPause(), false);
	}
	public void setPause(double pause) {
		this.cti_Pause = pause;
	}

	public double getSpærret() {
		return cti_Spærret;
	}
	public String getSpærretFormateret() {
		return date.toTTMM(this.getSpærret(), false);
	}
	public void setSpærret(double spærret) {
		this.cti_Spærret = spærret;
	}

	public double getTaler() {
		return cti_Taler;
	}
	public String getTalerFormateret() {
		return date.toTTMM(this.getTaler(), false);
	}
	public void setTaler(double taler) {
		this.cti_Taler = taler;
	}

	public double getUddannelse() {
		return cti_Uddannelse;
	}
	public String getUddannelseFormateret() {
		return date.toTTMM(this.getUddannelse(), false);
	}
	public void setUddannelse(double uddannelse) {
		this.cti_Uddannelse = uddannelse;
	}

	public double getWebdesk() {
		return cti_Webdesk;
	}
	public String getWebdeskFormateret() {
		return date.toTTMM(this.getWebdesk(), false);
	}
	public void setWebdesk(double webdesk) {
		this.cti_Webdesk = webdesk;
	}

	public double getØvrigTid() {
		return cti_ØvrigTid;
	}
	public String getØvrigTidFormateret() {
		return date.toTTMM(this.getØvrigTid(), false);
	}
	public void setØvrigTid(double øvrigTid) {
		this.cti_ØvrigTid = øvrigTid;
	}

	// ***************************** SALG *****************************
	
	public double getForsikringspræmie() {
		return salg_Forsikringspræmie;
	}
	public String getForsikringspræmieFormateret() {
		return bf.KiloDotFill(salg_Forsikringspræmie, false);
	}
	public void setForsikringspræmie(double forsikringspræmie) {
		this.salg_Forsikringspræmie = forsikringspræmie;
	}
	
	public double getMersalg() {
		return salg_Mersalg;
	}
	public String getMersalgFormateret() {
		return bf.KiloDotFill(salg_Mersalg, false);
	}
	public void setMersalg(double mersalg) {
		this.salg_Mersalg = mersalg;
	}
	
	public double getHenvisninger(){
		return (salg_Henvisninger);
	}
	public String getHenvisningerFormateret(){
		return bf.KiloDotFill(this.getHenvisninger(), false);
	}
	public void setHenvisninger(double henvisninger){
		this.salg_Henvisninger = henvisninger;
	}
	
	public double getGennemførtSalg(){
		return this.salg_GennemførteSalg;
	}
	public String getGennemførtSalgFormateret(){
		return bf.KiloDotFill(this.getGennemførtSalg(), false);
	}
	public void setGennemførtSalg(double gennemførteSalg){
		this.salg_GennemførteSalg = gennemførteSalg;
	}
	
	public double getSalgsrate(){
		if (this.salg_GennemførteSalg == 0)
			return 0;
		else
			return this.salg_GennemførteSalg / this.salg_Henvisninger;
	}
	public String getSalgsrateFormateret(){
		return bf.toProcent(this.getSalgsrate(), (this.salg_Henvisninger > 0)); 
	}
		
	// ***************************** AKTIVITETER *****************************

	public double getEmailIndgående() {
		return aktiviteter_EmailIndgående;
	}
	public String getEmailIndgåendeFormateret() {
		return bf.KiloDotFill(aktiviteter_EmailIndgående, false);
	}
	public void setEmailIndgående(double emailIndgående) {
		this.aktiviteter_EmailIndgående = emailIndgående;
	}

	public double getAktiviteterAfsluttet() {
		return this.aktiviteter_Afsluttet;
	}
	public String getAktiviteterAfsluttetFormateret() {
		return bf.KiloDotFill(this.aktiviteter_Afsluttet, false);
	}
	public void setAktiviteterAfsluttet(double aktiviteter) {
		this.aktiviteter_Afsluttet = aktiviteter;
	}
	
	public double getAktiviteterOprettet() {
		return this.aktiviteter_Oprettet;
	}
	public String getAktiviteterOprettetFormateret() {
		return bf.KiloDotFill(this.aktiviteter_Oprettet, false);
	}
	public void setAktiviteterOprettet(double aktiviteterOprettet) {
		this.aktiviteter_Oprettet = aktiviteterOprettet;
	}
	
	public double getAktiviteterAfsluttetIndenfor2TimerEksklEmail() {
		return this.getIndenforTidsfrist() + this.getOverskredne();
	}
	public String getAktiviteterAfsluttetIndenfor2TimerEksklEmailFormateret() {
		return bf.KiloDotFill(getAktiviteterAfsluttetIndenfor2TimerEksklEmail(), false);
	}
	
	public double getAktiviteterAfsluttetIndenfor2TimerInklEmail() {
		return this.getIndenforTidsfrist() + this.getOverskredne() + this.getEmailIndgående();
	}
	public String getAktiviteterAfsluttetIndenfor2TimerInklEmailFormateret() {
		return bf.KiloDotFill(
				this.getAktiviteterAfsluttetIndenfor2TimerInklEmail(), false);
	}
	
	public double getIndenforTidsfrist() {
		return aktiviteter_IndenforTidsfrist;
	}
	public String getIndenforTidsfristFormateret() {
		return bf.KiloDotFill(aktiviteter_IndenforTidsfrist, false);
	}
	public void setIndenforTidsfrist(double indenforTidsfrist) {
		this.aktiviteter_IndenforTidsfrist = indenforTidsfrist;
	}
	
	public double getAktiviteterIndenforTidsfrist() {
		if ((aktiviteter_IndenforTidsfrist + aktiviteter_Overskredne) == 0)
			return 0;
		else
			return aktiviteter_IndenforTidsfrist
					/ (aktiviteter_IndenforTidsfrist + aktiviteter_Overskredne);
	}
	public String getAktiviteterIndenforTidsfristFormateret() {
		return bf.toProcent(getAktiviteterIndenforTidsfrist(), false);
	}
	
	public double getOverskredne() {
		return aktiviteter_Overskredne;
	}
	public String getOverskredneFormateret() {
		return bf.KiloDotFill(aktiviteter_Overskredne, false);
	}
	public void setOverskredne(double overskredne) {
		this.aktiviteter_Overskredne = overskredne;
	}
	
	public double getTlfBeskeder() {
		return aktiviteter_TelefonBeskeder;
	}
	public String getTlfBeskederFormateret() {
		return bf.KiloDotFill(this.getTlfBeskeder(), false);
	}
	public void setTlfBesk(double tlfBesk) {
		this.aktiviteter_TelefonBeskeder = tlfBesk;
	}
	
	public double getSupportOpgaverAfsluttet() {
		return this.supportopgaver_Afsluttet;
	}
	public String getSupportOpgaverFormateret() {
		return bf.KiloDotFill(this.supportopgaver_Afsluttet, false);
	}
	public void setSupportOpgaverAfsluttet(double supportopgaver) {
		this.supportopgaver_Afsluttet = supportopgaver;
	}
	
	public double getSupportIndenforFrist() {
		return this.supportopgaver_IndenforFrist;
	}
	public String getSupportIndenForFristFormateret() {
		return bf.KiloDotFill(this.getSupportIndenforFrist(), false);
	}
	public void setSupportIndenforFrist(double supportIndenforFrist) {
		this.supportopgaver_IndenforFrist = supportIndenforFrist;
	}

	public double getSupportOpgaverOverskredne() {
		return this.supportopgaver_Afsluttet - this.supportopgaver_IndenforFrist;
	}
	public String getSupportOpgaverOverskredneFormateret() {
		return bf.KiloDotFill(this.getSupportOpgaverOverskredne(), false);
	}
	
	public double getKampagnePotentialer() {
		return aktiviteter_OprettedeKampagnePotentialer;
	}
	public String getKampagnePotentialerFormateret() {
		return bf.KiloDotFill(aktiviteter_OprettedeKampagnePotentialer, false);
	}
	public void setKampagnePotentialer(double kampagnePotentialer) {
		this.aktiviteter_OprettedeKampagnePotentialer = kampagnePotentialer;
	}
	
	public double getCallbacks(){
		return this.Aktiviteter_callbacks;
	}
	public String getCallbacksFormateret(){
		return bf.KiloDotFill(this.getCallbacks(), false);
	}
	public void setCallbacks(double callbacks){
		this.Aktiviteter_callbacks = callbacks;
	}
	
	// ***************************** BEREGNINGER *****************************
	
	public Double getAktiviteterPrLoginTime() {
		if (cti_KorrigeretLogin == 0)
			return null;

		return (aktiviteter_IndenforTidsfrist + aktiviteter_Overskredne + aktiviteter_EmailIndgående) / (cti_KorrigeretLogin * 24);
	}
	public String getAktiviteterPrLoginTimeFormateret() {
		return toOneDigit(getAktiviteterPrLoginTime(), false);
	}

	public Double getKaldPrTime() {
		if (cti_KorrigeretLogin == 0)
			return null;

		return (kald_Indgående / (cti_KorrigeretLogin * 24));
	}
	public String getKaldPrTimeFormateret() {
		return bf.KiloDotFill(getKaldPrTime(), false);
	}
	
	public Double getEmailAktiviteterPrLoginTime() {
		if (cti_KorrigeretLogin == 0)
			return null;

		return (aktiviteter_EmailIndgående / (cti_KorrigeretLogin * 24));
	}
	public String getEmailAktiviteterPrLoginTimeFormateret() {
		return toOneDigit(getEmailAktiviteterPrLoginTime(), false);
	}
	
	public Double getEmailAktiviteterPrEmailLogin() {
		if (cti_Email == 0)
			return null;

		return this.getEmailIndgående() / (cti_Email * 24);
	}
	public String getEmailAktiviteterPrEmailLoginFormateret() {
		return toOneDigit(getEmailAktiviteterPrEmailLogin(), false);
	}
	
	public double getAHT() {
		if (kald_Indgående == 0)
			return 0;

		return (cti_Taler + cti_Efterbehandling) / kald_Indgående;
	}
	public String getAHTFormateret() {
		return date.toTTMM(getAHT() * 60, false);
	}
	
	public Double getKPI() {
		if (cti_KorrigeretLogin == 0){
			return null;
		}else{
			double k =(kald_Indgående + aktiviteter_IndenforTidsfrist + aktiviteter_Overskredne + aktiviteter_EmailIndgående + Aktiviteter_callbacks) / (cti_KorrigeretLogin * 24); 
			System.out.println(Aktiviteter_callbacks);
			return k;
		}	
	}
	public String getKPIFormateret() {
		return toOneDigit(getKPI(), false);
	}
	
	public Double getKPIWebdesk() {
		if (cti_KorrigeretLogin == 0)
			return null;
		else
			return (kald_Indgående + 
					aktiviteter_IndenforTidsfrist + 
					aktiviteter_Overskredne + 
					aktiviteter_EmailIndgående +
					kald_Webdesk + 
					Aktiviteter_callbacks) / (cti_KorrigeretLogin * 24);
	}
	public String getKPIWebdeskFormateret() {
		return toOneDigit(this.getKPIWebdesk(), false);
	}
	
	public Double getKPISupport() {
		if (cti_KorrigeretLogin == 0)
			return null;
		else
			return (this.kald_Indgående + 
					this.aktiviteter_IndenforTidsfrist + 
					this.aktiviteter_Overskredne + 
					this.kald_Webdesk+
					this.supportopgaver_Afsluttet + 
					this.aktiviteter_EmailIndgående+
					this.aktiviteter_OprettedeKampagnePotentialer +
					this.Aktiviteter_callbacks) / (cti_KorrigeretLogin * 24);
	}
	public String getKPISupportFormateret() {
		return toOneDigit(getKPISupport(), false);
	}
		
	public double getKPIConsult() {
		if (cti_KorrigeretLogin == 0)
			return 0;
		else
			return (kald_Indgående + kald_Consult + aktiviteter_IndenforTidsfrist + aktiviteter_Overskredne + aktiviteter_EmailIndgående + Aktiviteter_callbacks) / (cti_KorrigeretLogin * 24);
	}
	public String getKPIConsultFormateret() {
		return toOneDigit(getKPIConsult(), false);
	}
		
	public double getPausePct() {
		if ((cti_KorrigeretLogin) == 0)
			return 0;

		return (cti_Pause / cti_KorrigeretLogin);
	}
	public String getPausePctFormateret() {
		return bf.toProcent(getPausePct(), false);
	}
		
	public double getTilgængelighed() {
		if (cti_KorrigeretLogin == 0)
			return 0;

		return (cti_Klar + cti_Taler + cti_ØvrigTid + cti_Webdesk) / (cti_KorrigeretLogin - cti_Email - cti_Uddannelse - cti_Møde);
	}
	
	public double getWFCMTilgængelighed() {
		if (cti_KorrigeretLogin == 0)
			return 0;

		return (cti_Klar + cti_Taler + cti_ØvrigTid + cti_Webdesk) / (beregninger_Planlagtkundetid);
	}
	
	public double getProduktivitetiftWFCM(){
		if (cti_KorrigeretLogin == 0){
			return 0;
		}else{
			return (getAktiviteterAfsluttetIndenfor2TimerEksklEmail()+ aktiviteter_EmailIndgående+kald_Webdesk+Aktiviteter_callbacks+kald_Indgående)/(beregninger_Planlagtkundetid *24);
		}
	
	}
	public String getTilgængelighedFormateret() {
		return bf.toProcent(getTilgængelighed(), false);
	}
		
	public double getPlanlagttid() {
		return beregninger_Planlagttid;
	}
	public String getPlanlagttidFormateret() {
		if (beregninger_Planlagttid == 0)
			return null;

		return date.toTTMM(beregninger_Planlagttid, false);
	}
	public void setPlanlagttid(double planlagttid) {
		this.beregninger_Planlagttid = planlagttid;
	}
	
	public double getPlanlagtKundetid() {
		return beregninger_Planlagtkundetid;
	}
	public String getPlanlagtKundetidFormateret() {
		if (beregninger_Planlagtkundetid == 0)
			return null;

		return date.toTTMM(beregninger_Planlagtkundetid, false);
	}
	public void setPlanlagtKundetid(double planlagtkundetid) {
		this.beregninger_Planlagtkundetid = planlagtkundetid;
	}
	
	public Double getKampagnepotentialerPrTime() {
		if (cti_KorrigeretLogin == 0)
			return null;

		return (getKampagnePotentialer() / (cti_KorrigeretLogin * 24));
	}
	public String getKampagnepotentialerPrTimeFormateret() {
		return toOneDigit(getKampagnepotentialerPrTime(), false);
	}
	public String getWFCMTilgængelighedFormattet(){
		return bf.toProcent(getWFCMTilgængelighed(), true);
	}
	public String getWFCMProduktivitetdFormattet(){
		return toOneDigit(getProduktivitetiftWFCM(), true);
	}
	
	public Double getProcentSupportOpgaverIndenforTidsfrist(){
		if(supportopgaver_Afsluttet == 0)
			return null;
		
		return (this.supportopgaver_IndenforFrist / this.supportopgaver_Afsluttet);
	}
	public String getProcentSupportOpgaverIndenforTidsfristFormateret(){
		return bf.toProcent(this.getProcentSupportOpgaverIndenforTidsfrist(), false);
	}
	
	public double getProcentvisMersalg() {
		double procentvisMersalg;

		if (Math.round((salg_Mersalg / salg_Forsikringspræmie) * 100) == 0)
			return 0;

		procentvisMersalg = (salg_Mersalg / salg_Forsikringspræmie);

		return procentvisMersalg;
	}
	public String getProcentvisMersalgFormateret() {
		return bf.toProcent(getProcentvisMersalg(), false);
	}
	
	public double getWebdeskTalerTid() {
		return this.beregninger_WebdeskTalerTid;
	}
	public String getWebdeskTalerTidFormateret() {
		if (this.beregninger_WebdeskTalerTid == 0)
			return null;
		else {
			Integer timer = (int) this.beregninger_WebdeskTalerTid;
			Long minutter = Math
					.round((this.beregninger_WebdeskTalerTid % 1) * 0.6 * 100);

			String samlet;

			if (timer < 10)
				samlet = "0" + timer.toString();
			else
				samlet = timer.toString();

			samlet += ":";

			if (minutter < 10)
				samlet += "0" + minutter.toString();
			else
				samlet += minutter.toString();

			return samlet;
		}
	}
	public void setWebdeskTalerTid(double webdeskTalerTid) {
		this.beregninger_WebdeskTalerTid = webdeskTalerTid;
	}
	
	public double getWebdeskKaldPrWebdeskTime() {
		return (this.kald_Webdesk / (beregninger_WebdeskTalerTid * 24));
	}
	public String getWebdeskKaldPrWebdeskTimeFormateret() {
		return bf.KiloDotFill(this.getWebdeskKaldPrWebdeskTime(), false);
	}
	
	// ***************************** EKSPEDITION *****************************
	
	public double getOpfølgningsserviceAfsluttet() {
		return ekspedition_OpfølgningsserviceAfsluttet;
	}
	public String getOpfølgningsserviceAfsluttetFormateret() {
		return bf.KiloDotFill(ekspedition_OpfølgningsserviceAfsluttet, false);
	}
	public void setOpfølgningsserviceAfsluttet(double opfølgningsserviceAfsluttet){
		this.ekspedition_OpfølgningsserviceAfsluttet = opfølgningsserviceAfsluttet;
	}
	
	public double getIndgåendeKaldDialog() {
		return ekspedition_IndgåendeKaldDialog;
	}
	public String getIndgåendeKaldDialogFormateret() {
		return bf.KiloDotFill(ekspedition_IndgåendeKaldDialog, false);
	}
	public void setIndgåendeKaldDialog(double indgåendeKaldDialog) {
		this.ekspedition_IndgåendeKaldDialog = indgåendeKaldDialog;
	}
	
	public Double getOprettedeAktiviteterIkkeViderestillinger(){
		if (cti_KorrigeretLogin == 0 | firstCall == 0)
			return null;

		if ((kaldIalt - getViderestillinger()) != 0)
			return ekspedition_IndgåendeKaldDialog / firstCall;
		else
			return null;
	}
	public String getOprettedeAktiviteterIkkeViderestillingerFormateret() {
		return bf.toProcent(getOprettedeAktiviteterIkkeViderestillinger(), false);
	}
	
	public double getTlfBeskederPctIndgåendeKald() {
		if (kald_Indgående == 0)
			return 0;

		return aktiviteter_TelefonBeskeder / kaldIalt;
	}
	public String getTlfBeskederPctIndgåendeKaldFormateret() {
		return bf.toProcent(getTlfBeskederPctIndgåendeKald(), false);
	}
	
	public double getViderestillingerPctIndgåendeKald() {
		if (kald_Indgående == 0)
			return 0;

		return getViderestillinger() / kaldIalt;
	}
	public String getViderestillingerPctIndgåendeKaldFormateret() {
		return bf.toProcent(getViderestillingerPctIndgåendeKald(), false);
	}
	
	public Double getHenvisningerPrLoginTime() {
		if (cti_KorrigeretLogin == 0)
			return null;

		return ((salg_Henvisninger) + historik_HenvisningerQ12013) / (cti_KorrigeretLogin * 24);
	}
	public String getHenvisningerPrLoginTimeFormateret() {
		Double h = this.getHenvisningerPrLoginTime();
		
		if(h == null)
			return null;
		
		DecimalFormat myFormat = new java.text.DecimalFormat("0.##");
		
		return myFormat.format(h);
	}
	
	public double getViderestillinger() {
		double firscalludentlfbesk = 0;
		
		if ((firstCall - aktiviteter_TelefonBeskeder) >= 0)
			firscalludentlfbesk = firstCall - aktiviteter_TelefonBeskeder;
		
		if (firscalludentlfbesk + aktiviteter_TelefonBeskeder < kaldIalt)
			return kaldIalt - firscalludentlfbesk - aktiviteter_TelefonBeskeder;
		else
			return 0;
	}
	public String getViderestillingerFormateret() {
		return bf.KiloDotFill(getViderestillinger(), false);
	}
	
	public double getTilbudteSelvbetjeningsprodukter() {
		return this.ekspedition_Serviceprodukter;
	}
	public String getTilbudteSelvbetjeningsprodukterFormateret() {
		return bf.KiloDotFill(this.ekspedition_Serviceprodukter, false);
	}
	public void setTilbudteSelvbetjeningsprodukter(double tilbudteServiceprodukter) {
		this.ekspedition_Serviceprodukter = tilbudteServiceprodukter;
	}
	
	public Double getSelvbetjeningsprodukterPrLogintime() {
		if (cti_KorrigeretLogin == 0)
			return null;

		return this.ekspedition_Serviceprodukter / (cti_KorrigeretLogin * 24);
	}
	public String getServiceprodukterPrLogintimeFormateret() {
		return toOneDigit(getSelvbetjeningsprodukterPrLogintime(), false);
	}
	
	public double getTabtSalg() {
		return ekspedition_TabtSalg;
	}
	public String getTabtSalgFormateret() {
		return bf.KiloDotFill(ekspedition_TabtSalg, false);
	}
	public void setTabtSalg(double tabtSalg) {
		this.ekspedition_TabtSalg = tabtSalg;
	}
	
	// ***************************** HISTORIK *****************************
	
	public double getEmailTilgængelighed() {
		double emailTilgængelighed;

		if (cti_KorrigeretLogin == 0)
			return 0;
		if (Math.round((cti_Email / cti_KorrigeretLogin) * 100) == 0)
			return 0;

		emailTilgængelighed = (cti_Email / cti_KorrigeretLogin);

		return emailTilgængelighed;
	}
	public String getEmailTilgængelighedFormateret() {
		return bf.toProcent(getEmailTilgængelighed(), false);
	}
		
	public double getTlfTilgængelighed() {
		double tlfTilgængelighed;

		if (cti_KorrigeretLogin == 0)
			return 0;
		if (Math.round(((cti_Klar + cti_Taler + cti_ØvrigTid) / cti_KorrigeretLogin) * 100) == 0)
			return 0;

		tlfTilgængelighed = ((cti_Klar + cti_Taler + cti_ØvrigTid) / cti_KorrigeretLogin);

		return tlfTilgængelighed;
	}
	public String getTlfTilgængelighedFormateret() {
		return bf.toProcent(getTlfTilgængelighed(), false);
	}
	
	public double getTilgængelighed2011() {
		double tilgængelighed2011;

		if (cti_KorrigeretLogin == 0)
			return 0;

		tilgængelighed2011 = (cti_Klar + cti_Taler + cti_ØvrigTid + cti_Webdesk + cti_Email)
				/ cti_KorrigeretLogin;

		return tilgængelighed2011;
	}
	public String getTilgængelighed2011Formateret() {
		return bf.toProcent(getTilgængelighed2011(), false);
	}
	
	public double getTilgængelighed2012() {
		double tilgængelighed2012;

		if (cti_KorrigeretLogin == 0)
			return 0;

		tilgængelighed2012 = (cti_Klar + cti_Taler + cti_ØvrigTid + cti_Webdesk)
				/ (cti_KorrigeretLogin - cti_Email);

		return tilgængelighed2012;
	}
	public String getTilgængelighed2012Formateret() {
		return bf.toProcent(this.getTilgængelighed2012(), false);
	}
	
	public double getFærdiggørelsesgrad() {
		if (færdiggørelsesgrad == 0) {
			if (kaldIalt == 0) {
				return 0;
			}
			if (getFirstCallUdenTelefonBeskeder() > kaldIalt) {
				færdiggørelsesgrad = 1;
			} else {
				færdiggørelsesgrad = getFirstCallUdenTelefonBeskeder()
						/ kaldIalt;
			}
			if (færdiggørelsesgrad < 0.01) {
				return 0;
			}
			return færdiggørelsesgrad;
		}
		return færdiggørelsesgrad;
	}
	public String getFærdiggørelsesgradFormateret() {
		if (getFærdiggørelsesgrad() == 0) {
			return null;
		}
		return bf.toProcent(getFærdiggørelsesgrad(), false);
	}
	public void setFærdiggørelsesgrad(double færdiggørelsesgrad) {
		this.færdiggørelsesgrad = færdiggørelsesgrad;
	}

	public double getHenvisningerQ12013() {
		return historik_HenvisningerQ12013;
	}
	public String getHenvisningerQ12013Formateret() {
		return bf.KiloDotFill(this.getHenvisningerQ12013(), false);
	}
	public void setHenvisningerQ12013(double henvisninger) {
		this.historik_HenvisningerQ12013 = henvisninger;
	}
	
	public double getGennemførtSalgQ12013() {
		return this.historik_GennemførtSalgQ12013;
	}
	public String getGennemførtSalgQ12013Formateret() {
		return bf.KiloDotFill(this.getGennemførtSalgQ12013(), false);
	}
	public void setGennemførtSalgQ12013(double gennemførtSalg) {
		this.historik_GennemførtSalgQ12013 = gennemførtSalg;
	}
	
	public double getSalgsRateQ12013() {
		if (this.historik_GennemførtSalgQ12013 == 0)
			return 0;
		else
			return this.historik_GennemførtSalgQ12013 / this.historik_HenvisningerQ12013;
	}
	public String getSalgsRateQ12013Formateret() {
		return bf.toProcent(this.getSalgsRateQ12013(), (this.historik_HenvisningerQ12013 > 0));
	}

	
	public double getFirstCall() {
		return firstCall;
	}
	public void setFirstCall(double firstCall) {
		this.firstCall = firstCall;
	}

	public double getFirstCallUdenTelefonBeskeder() {
		if (firstCallUdenTelefonBeskeder == 0) {
			if (aktiviteter_TelefonBeskeder > firstCall) {
				firstCallUdenTelefonBeskeder = 0;
			} else {
				firstCallUdenTelefonBeskeder = firstCall
						- aktiviteter_TelefonBeskeder;
			}
		}
		return firstCallUdenTelefonBeskeder;
	}
	public void setFirstCallUdenTelefonBeskeder(double firstCallUdenTelefonBeskeder) {
		this.firstCallUdenTelefonBeskeder = firstCallUdenTelefonBeskeder;
	}
		
	public double getKaldIalt() {
		return kaldIalt;
	}
	public void setKaldIalt(double kaldIalt) {
		this.kaldIalt = kaldIalt;
	}

	private String toOneDigit(Double value, boolean showZero) {
		if (value == null)
			if (showZero)
				return "0.0";
			else
				return null;

		DecimalFormat myFormat = new java.text.DecimalFormat("0.#");

		return myFormat.format(value);
	}

	public String getMeetingsBooked() {
		return ""+(int)meetingsBooked;
	}

	public void setMeetingsBooked(double meetingsBooked) {
		this.meetingsBooked = meetingsBooked;
	}
	public double getMeetingsBookedDouble(){
		return this.meetingsBooked;
	}

	public void setMeetingReference(double double1) {
		this.meetingReference = double1;
	}
	public double getMeetingRefence(){
		return this.meetingReference;
	}
}
