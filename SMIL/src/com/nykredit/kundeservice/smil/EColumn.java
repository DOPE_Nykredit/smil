package com.nykredit.kundeservice.smil;

import javax.swing.JCheckBox;

public enum EColumn {
	/* KALD */
	KALD_INDGÅENDE(ECategory.KALD, "Indgående kald", "Ind.<br>kald", true),
	KALD_UDGÅENDE(ECategory.KALD, "Udgående kald", "Udg.<br>kald", false),
	KALD_CONSULT(ECategory.KALD, "Consult kald", "Consult<br>kald", false),
	KALD_CBR(ECategory.KALD, "CBR kald", "CBR kald", false),
	KALD_WEBDESK(ECategory.KALD, "Webdesk kald", "Webdesk<br>kald", false),
	/* CTI */
	CTI_KORRIGERET(ECategory.CTI, "Korrigeret Login", "Korrigeret<br>login", true),
	CTI_TALER(ECategory.CTI, "Taler", "Taler", false),
	CTI_KLAR(ECategory.CTI, "Klar", "Klar", false), 
	CTI_ØVRIG(ECategory.CTI, "Øvrig tid", "Øvrig<br>tid", false),
	CTI_EMAIL(ECategory.CTI, "Email tid", "Email<br>tid", false),
	CTI_WEBDESK(ECategory.CTI, "Webdesk", "Webdesk", false),
	CTI_EFTERBEHANDLING(ECategory.CTI, "Efterbehandling", "Efter-<br>behandling", false),
	CTI_ANDREOPGAVER(ECategory.CTI, "Andre opgaver", "Andre<br>opgaver", false), 
	CTI_KOLLEGAHJÆLP(ECategory.CTI, "Kollegahjælp", "Kollega-<br>hjælp", false),
	CTI_PAUSE(ECategory.CTI, "Pause", "Pause", false),
	CTI_FROKOST(ECategory.CTI, "Frokost", "Frokost", false), 
	CTI_UDDANNELSE(ECategory.CTI, "Uddannelses", "Uddannelse", false),
	CTI_MØDE(ECategory.CTI, "Møde", "Møde", false), 
	CTI_SPÆRRET(ECategory.CTI, "Spærret", "Spærret", false), 
	/* SALG */
	SALG_MERSALG(ECategory.SALG, "Mersalg", "Mersalg", true),
	SALG_HENVISNINGER(ECategory.SALG, "Henvisninger", "Henvis-<br>ninger", true),
	SALG_GENNEMFØRTESALG(ECategory.SALG, "Gennemførte salg", "Gennemfør-<br>te salg", false),
	SALG_SALGSRATE(ECategory.SALG, "Salgsrate", "Salgsrate", false),
	/* AKTIVITETER */
	AKTIVITETER_EMAILINDGÅENDE(ECategory.AKTIVITETER, "Email indgående", "Email<br>indg.", false),
	AKTIVITETER_AFSLUTTET(ECategory.AKTIVITETER, "Afsluttet", "Akt.<br>afs.", false),
	AKTIVITETER_OPRETTET(ECategory.AKTIVITETER, "Oprettet", "Akt.<br>opr.", false),
	AKTIVITETER_AFSLUTTETINDENFOR2TIMERINKLEMAIL(ECategory.AKTIVITETER, "Afsl. indenfor 2 t. (inkl. e-mail)", "Afsl. akt<br>ind. 2t.<br>(m. e-mail)", false),
	AKTIVITETER_AFSLUTTETINDENFOR2TIMEREXCLEMAIL(ECategory.AKTIVITETER,"Afsl. indenfor 2t. (ekskl. e-mail)", "Afsl. akt<br>ind. 2t.<br>(u. e-mail)", false),
	AKTIVITETER_INDENDFORTIDSFRIST(ECategory.AKTIVITETER, "Indenfor tidsfrist", "Akt.<br>ind.<br>tidsfr.", false),
	AKTIVITETER_OVERSKREDET(ECategory.AKTIVITETER, "Overskredet", "Akt.<br>overskr.", false),
	AKTIVITETER_TELEFONBESKEDER(ECategory.AKTIVITETER, "Telefonbeskeder", "Akt. tlf<br>besk.", false),
	SUPPORTOPGAVER_AFSLUTTET(ECategory.AKTIVITETER, "Support opgaver", "Supp.<br>opgaver", false),
	SUPPORTOPGAVER_INDENFORFRIST(ECategory.AKTIVITETER, "Supp.opg. indenfor tidsfrist", "Supp. opg<br>indenfor<br>tidsf.", false),
	SUPPORTOPGAVER_OVERSKREDET(ECategory.AKTIVITETER, "Supportopgaver overskredet", "Supp. opg.<br>overskredet", false),
	AKTIVITETER_OPRETTEDEKAMPAGNEPOTENTIALER(ECategory.AKTIVITETER, "Oprettede kamp. potentialer", "Opr.<br>kampagne<br>potentialer", false),
	AKTIVITETER_CALLBACKSHÅNDTERET(ECategory.AKTIVITETER, "Callbacks håndteret", "Callbacks<br>håndteret", false),
	AKTIVITETER_MØDEBOOKING(ECategory.AKTIVITETER, "Mødeservice", "Mødeservice", false),

	/* BEREGNINGER */
	BEREGNINGER_PROCENTAKTIVITETERINDFORTIDSFRIST(ECategory.BEREGNINGER, "Aktiviteter indenfor tidsfrist", "Akt.<br>ind.<br>tidsfr. %", true),
	BEREGNINGER_AKTIVITETERPRLOGINTIME(ECategory.BEREGNINGER, "Aktiviteter pr. logintime", "Akt. pr.<br>logintime", true),
	BEREGNINGER_KALDPRLOGINTIME(ECategory.BEREGNINGER, "Kald pr. logintime", "Kald pr.<br>logintime", false),
	BEREGNINGER_EMAILAKTIVITETERPRLOGINTIME(ECategory.BEREGNINGER, "Emails pr. logintime", "Email<br>pr.<br>logintime", true),
	BEREGNINGER_EMAILAKTIVITETERPREMAILLOGINTIME(ECategory.BEREGNINGER, "Emails pr. Email time", "Email<br>pr.<br>Email time", true),
	BEREGNINGER_AHT(ECategory.BEREGNINGER, "AHT", "AHT", false),
//	BEREGNINGER_KPI(ECategory.BEREGNINGER, "Produktivitet", "Produktivitet", true),
	BEREGNINGER_KPIWEBDESK(ECategory.BEREGNINGER, "Produktivitet", "Produktivitet", true),
	BEREGNINGER_KPISUPPORT(ECategory.BEREGNINGER, "Produktivitet m. supportopgaver", "Produkti-<br>vitet m.<br>supp. opg", true),
//	BEREGNINGER_KPICONSULT(ECategory.BEREGNINGER, "Produktivitet m. consult", "Produkti-<br>vitet m.<br>consult", true),
	BEREGNINGER_PAUSEPCTAFLOGINTIME(ECategory.BEREGNINGER, "Pause % logintime", "Pause<br>%<br>logintime", false),
	BEREGNINGER_TILGÆNGELIGHED(ECategory.BEREGNINGER, "Tilgængelighed", "Tilgænge-<br>lighed", false),
	BEREGNINGER_WFCMPLANLAGTTID(ECategory.BEREGNINGER, "WFCM Planlagttid", "WFCM<br>planlagt<br>tid", false),
	BEREGNINGER_KAMPAGNEPOTENTIALERPRTIME(ECategory.BEREGNINGER, "Kampagnepotentialer pr. time", "Kampagne-<br>potentialer<br>pr. time", false),
	BEREGNINGER_PROCENTSUPPORTOPGAVERINDENFORTIDSFRIST(ECategory.BEREGNINGER, "Sup. opg. ind. tidsfr. %", "Supp. opg<br>indenfor<br> tidsfr. %", false),
	BEREGNINGER_PROCENTVISMERSALG(ECategory.BEREGNINGER, "Procentvis mersalg", "Procentvis<br>mersalg", false),
	BEREGNINGER_WEBDESKTALERTID(ECategory.BEREGNINGER, "Webdesk taler tid", "Webdesk<br>taler<br>tid", false),
	BEREGNINGER_WEBDESKKALDPRWEBDESKTIME(ECategory.BEREGNINGER, "Webdesk kald pr. webdesktime", "Webdesk<br> kald pr.<br>webdesktime", false),
	BEREGNINGER_WFCMPLANLAGTKUNDETID(ECategory.BEREGNINGER, "WFCM PlanlagtKundetid", "WFCM<br>planlagt<br>kundetid", false),
	BEREGNINGER_Tilgængelighed_ift_WFCM(ECategory.BEREGNINGER, "Tilgængelighed ift. WFCM", "Tilgængelighed ift. WFCM", false),
	BEREGNINGER_Produktivitet_ift_WFCM(ECategory.BEREGNINGER, "Produktivitet ift. WFCM", "Produktivitet ift. WFCM", false),

	/*Ekspeditionsservice*/
	EKSPEDITION_OPFØLGNINGSSERVICE(ECategory.EKSPEDITIONSSERVICE, "Opfølgningsservice afsluttet", "Opf.<br>service<br>afs.", false),
	EKSPEDITION_INDGÅENDEKALD(ECategory.EKSPEDITIONSSERVICE, "Aktiviteter indgående kald", "Akt.<br>ind.<br>kald", false),
	EKSPEDITION_OPRETTETAKTIVITETERIKKEVIDERESTILLING(ECategory.EKSPEDITIONSSERVICE, "Dialoreg Opr. på IKKE viderestillede kald", "Opr. akt.<br>på IKKE<br>vdr. kald", false),
	EKSPEDITION_TLFBESKEDERPCTINDKALD(ECategory.EKSPEDITIONSSERVICE, "Telefonbeskeder % indgående kald", "Tlf. besk.<br>%<br>ind. kald", false),
	EKSPEDITION_VIDERESTILLINGERPCTINDKALD(ECategory.EKSPEDITIONSSERVICE, "Viderestillinger % indgående kald", "Vdr. %<br>ind.<br>kald", false),
	EKSPEDITION_HENVISNINGERPRLOGINTIME(ECategory.EKSPEDITIONSSERVICE, "Henvisninger pr. logintime", "Henv.<br>pr.<br>logintime", false),
	EKSPEDITION_VIDERESTILLINGER(ECategory.EKSPEDITIONSSERVICE, "Viderestillinger", "Videre-<br>stillinger", false),
	EKSPEDITION_SOLGTESERVICEPRODUKTER(ECategory.EKSPEDITIONSSERVICE, "Solgte selvbetjeningsprodukter", "Solgte<br>selvb.<br>prod.", false),
	EKSPEDITION_SERVICEPRODUKTERPRLOGINTIME(ECategory.EKSPEDITIONSSERVICE, "Selvbetjeningsprodukter pr. logintime", "Selvb.<br>prod. pr.<br>logintime", false),
	EKSPEDITION_TABTSALG(ECategory.EKSPEDITIONSSERVICE, "Oprettede aktiviteter tabt salg", "Opr. akt.<br>tabt salg", false),
	/* Historik */
	HISTORIK_EMAILTILGÆNGELIGHED(ECategory.HISTORIK, "Email tilgængelighed", "Email<br>tilgænge-<br>lighed", false),
	HISTORIK_TELEFONTILGÆNGELIGHED(ECategory.HISTORIK, "Telefon Tilgænglighed", "Tlf.<br>tilgænge-<br>lighed", false),
	HISTORIK_TILGÆNGELIGHED2012(ECategory.HISTORIK, "Tilgængelighed 2012", "Tilgænge-<br>lighed 2012", false),
	HISTORIK_TILGÆNGELIGHED2011(ECategory.HISTORIK, "Tilgængelighed 2011", "Tilgænge-<br>lighed 2011", false),
	HISTORIK_FÆRDIGGØRELSESGRAD(ECategory.HISTORIK, "Færdiggørelsesgrad", "Færdig-<br>gørelse", false),
	HISTORIK_HENVISNINGERQ12013(ECategory.HISTORIK, "Henvisninger Q1", "Henvisnin-<br>ger Q1 2013", false),
	HISTORIK_GENNEMFØRTESALGQ12013(ECategory.HISTORIK, "Gennemførte salg Q1", "Gennemfør-<br>te salg Q1<br>2013", false),
	HISTORIK_SALGSRATEQ12013(ECategory.HISTORIK, "Salgsrate Q1", "Salgsrate<br>Q1 2013", false),
	HISTORIK_FORSIKRINGSPRÆMIE(ECategory.HISTORIK, "Forsikringspræmie", "Forsikr.<br>præmie", true),
	/* Settings */
	SHOWAVERAGE(ECategory.SETTINGS, "Gns. B1-B7 & R1-R2", "", false),
	SHOWSUBTOTAL(ECategory.SETTINGS, "Vis Sub total", "", true),
	SHOWTOTAL(ECategory.SETTINGS, "Vis Total", "", true),
	SHOWAGENTS(ECategory.SETTINGS, "Vis initialer", "", true),
	HIDEEMPTYLOGIN(ECategory.SETTINGS, "Skjul mederarb. u. login", "", false),
	/* Coloring */
	NOCOLORS(ECategory.COLORING, "Ingen farve formatering", "", true),
	COLORINGKONCERN(ECategory.COLORING, "Koncern", "", false),
	COLORINGB1B6(ECategory.COLORING, "B1-B7", "", false),
	COLORING_MS(ECategory.COLORING, "Mailservice", "", false),
	COLORINGNETBANK(ECategory.COLORING, "Netbank", "", false),
	COLORINGFORSIKRING(ECategory.COLORING, "Forsikring", "", false),
	COLORINGERHVERV(ECategory.COLORING, "Erhverv", "", false),
	COLORINGSUPPORT(ECategory.COLORING, "Support", "", false),
	COLORINGE1E2(ECategory.COLORING, "E1-E2", "", false),
	COLORINGE1(ECategory.COLORING, "E1", "", false),
	COLORINGE2(ECategory.COLORING, "E2", "", false);

	private ECategory category;
	private String checkBoxName;
	private String tableHeaderName;
	private boolean selected;
	private String rowData;
	private JCheckBox checkBox;

	EColumn(ECategory category, String checkBoxName, String tableHeaderName, boolean isselected) {
		this.category = category;
		this.checkBoxName = checkBoxName;
		this.tableHeaderName = tableHeaderName;
		this.selected = isselected;
		this.checkBox = new JCheckBox(checkBoxName, selected);
		checkBox.setOpaque(false);	
	}

	public String getCheckBoxName() {
		return checkBoxName;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
		this.checkBox.setSelected(selected);
	}
	
	public String getRowData() {
		return rowData;
	}

	public void setCheckBoxName(String checkBoxName) {
		this.checkBoxName = checkBoxName;
	}

	public void setRowData(String rowData) {
		this.rowData = rowData;
	}
	
	public JCheckBox getCheckBox() {
		return checkBox;
	}

	public void setCheckBox(JCheckBox checkBox) {
		this.checkBox = checkBox;
	}

	public ECategory getCategory() {
		return category;
	}

	public static void insertRow(DataTableColumns r) {
		KALD_INDGÅENDE.setRowData(r.getIndgåendeKaldFormateret());
		KALD_UDGÅENDE.setRowData(r.getUdKaldFormateret());
		KALD_CONSULT.setRowData(r.getConsultFormateret());
		KALD_CBR.setRowData(r.getCbrFormateret());
		KALD_WEBDESK.setRowData(r.getWebdeskKaldFormatted());
		
		CTI_KORRIGERET.setRowData(r.getKorrigeretFormateret());
		CTI_TALER.setRowData(r.getTalerFormateret());
		CTI_KLAR.setRowData(r.getKlarFormateret());
		CTI_ØVRIG.setRowData(r.getØvrigTidFormateret());
		CTI_EMAIL.setRowData(r.getEmailFormateret());
		CTI_WEBDESK.setRowData(r.getWebdeskFormateret());
		CTI_EFTERBEHANDLING.setRowData(r.getEfterbehandlingFormateret());
		CTI_ANDREOPGAVER.setRowData(r.getAndreOpgFormateret());
		CTI_KOLLEGAHJÆLP.setRowData(r.getKollegahjFormateret());
		CTI_PAUSE.setRowData(r.getPauseFormateret());
		CTI_FROKOST.setRowData(r.getFrokostFormateret());
		CTI_UDDANNELSE.setRowData(r.getUddannelseFormateret());
		CTI_MØDE.setRowData(r.getMødeFormateret());
		CTI_SPÆRRET.setRowData(r.getSpærretFormateret());
		
		HISTORIK_FORSIKRINGSPRÆMIE.setRowData(r.getForsikringspræmieFormateret());
		SALG_MERSALG.setRowData(r.getMersalgFormateret());
		SALG_HENVISNINGER.setRowData(r.getHenvisningerFormateret());
		SALG_GENNEMFØRTESALG.setRowData(r.getGennemførtSalgFormateret());
		SALG_SALGSRATE.setRowData(r.getSalgsrateFormateret());
		
		AKTIVITETER_EMAILINDGÅENDE.setRowData(r.getEmailIndgåendeFormateret());
		AKTIVITETER_AFSLUTTET.setRowData(r.getAktiviteterAfsluttetFormateret());
		AKTIVITETER_OPRETTET.setRowData(r.getAktiviteterOprettetFormateret());
		AKTIVITETER_AFSLUTTETINDENFOR2TIMERINKLEMAIL.setRowData(r.getAktiviteterAfsluttetIndenfor2TimerInklEmailFormateret());
		AKTIVITETER_AFSLUTTETINDENFOR2TIMEREXCLEMAIL.setRowData(r.getAktiviteterAfsluttetIndenfor2TimerEksklEmailFormateret());
		AKTIVITETER_INDENDFORTIDSFRIST.setRowData(r.getIndenforTidsfristFormateret());
		AKTIVITETER_OVERSKREDET.setRowData(r.getOverskredneFormateret());
		AKTIVITETER_TELEFONBESKEDER.setRowData(r.getTlfBeskederFormateret());
		SUPPORTOPGAVER_AFSLUTTET.setRowData(r.getSupportOpgaverFormateret());
		SUPPORTOPGAVER_OVERSKREDET.setRowData(r.getSupportOpgaverOverskredneFormateret());
		SUPPORTOPGAVER_INDENFORFRIST.setRowData(r.getSupportIndenForFristFormateret());
		AKTIVITETER_OPRETTEDEKAMPAGNEPOTENTIALER.setRowData(r.getKampagnePotentialerFormateret());
		AKTIVITETER_CALLBACKSHÅNDTERET.setRowData(r.getCallbacksFormateret());
		AKTIVITETER_MØDEBOOKING.setRowData(r.getMeetingsBooked());

		
		BEREGNINGER_PROCENTAKTIVITETERINDFORTIDSFRIST.setRowData(r.getAktiviteterIndenforTidsfristFormateret());
		BEREGNINGER_AKTIVITETERPRLOGINTIME.setRowData(r.getAktiviteterPrLoginTimeFormateret());
		BEREGNINGER_AKTIVITETERPRLOGINTIME.setRowData(r.getAktiviteterPrLoginTimeFormateret());
		BEREGNINGER_KALDPRLOGINTIME.setRowData(r.getKaldPrTimeFormateret());
		BEREGNINGER_EMAILAKTIVITETERPRLOGINTIME.setRowData(r.getEmailAktiviteterPrLoginTimeFormateret());
		BEREGNINGER_EMAILAKTIVITETERPREMAILLOGINTIME.setRowData(r.getEmailAktiviteterPrEmailLoginFormateret());
		BEREGNINGER_AHT.setRowData(r.getAHTFormateret());
		//BEREGNINGER_KPI.setRowData(r.getKPIFormateret());
		BEREGNINGER_KPIWEBDESK.setRowData(r.getKPIWebdeskFormateret());
		BEREGNINGER_KPISUPPORT.setRowData(r.getKPISupportFormateret());
	//	BEREGNINGER_KPICONSULT.setRowData(r.getKPIConsultFormateret());
		BEREGNINGER_PAUSEPCTAFLOGINTIME.setRowData(r.getPausePctFormateret());
		BEREGNINGER_TILGÆNGELIGHED.setRowData(r.getTilgængelighedFormateret());
		BEREGNINGER_WFCMPLANLAGTTID.setRowData(r.getPlanlagttidFormateret());
		BEREGNINGER_KAMPAGNEPOTENTIALERPRTIME.setRowData(r.getKampagnepotentialerPrTimeFormateret());
		BEREGNINGER_PROCENTSUPPORTOPGAVERINDENFORTIDSFRIST.setRowData(r.getProcentSupportOpgaverIndenforTidsfristFormateret());
		BEREGNINGER_PROCENTVISMERSALG.setRowData(r.getProcentvisMersalgFormateret());
		BEREGNINGER_WEBDESKTALERTID.setRowData(r.getWebdeskTalerTidFormateret());
		BEREGNINGER_WEBDESKKALDPRWEBDESKTIME.setRowData(r.getWebdeskKaldPrWebdeskTimeFormateret());
		BEREGNINGER_WFCMPLANLAGTKUNDETID.setRowData(r.getPlanlagtKundetidFormateret());
		BEREGNINGER_Tilgængelighed_ift_WFCM.setRowData(r.getWFCMTilgængelighedFormattet());
		BEREGNINGER_Produktivitet_ift_WFCM.setRowData(""+r.getWFCMProduktivitetdFormattet());
		
		
		
		
		EKSPEDITION_OPFØLGNINGSSERVICE.setRowData(r.getOpfølgningsserviceAfsluttetFormateret());
		EKSPEDITION_INDGÅENDEKALD.setRowData(r.getIndgåendeKaldDialogFormateret());
		EKSPEDITION_OPRETTETAKTIVITETERIKKEVIDERESTILLING.setRowData(r.getOprettedeAktiviteterIkkeViderestillingerFormateret());
		EKSPEDITION_TLFBESKEDERPCTINDKALD.setRowData(r.getTlfBeskederPctIndgåendeKaldFormateret());
		EKSPEDITION_VIDERESTILLINGERPCTINDKALD.setRowData(r.getViderestillingerPctIndgåendeKaldFormateret());
		EKSPEDITION_HENVISNINGERPRLOGINTIME.setRowData(r.getHenvisningerPrLoginTimeFormateret());
		EKSPEDITION_VIDERESTILLINGER.setRowData(r.getViderestillingerFormateret());
		EKSPEDITION_SOLGTESERVICEPRODUKTER.setRowData(r.getTilbudteSelvbetjeningsprodukterFormateret());
		EKSPEDITION_SERVICEPRODUKTERPRLOGINTIME.setRowData(r.getServiceprodukterPrLogintimeFormateret());
		EKSPEDITION_TABTSALG.setRowData(r.getTabtSalgFormateret());
		
		HISTORIK_EMAILTILGÆNGELIGHED.setRowData(r.getEmailTilgængelighedFormateret());
		HISTORIK_TELEFONTILGÆNGELIGHED.setRowData(r.getTlfTilgængelighedFormateret());
		HISTORIK_TILGÆNGELIGHED2011.setRowData(r.getTilgængelighed2011Formateret());
		HISTORIK_TILGÆNGELIGHED2012.setRowData(r.getTilgængelighed2012Formateret());
		HISTORIK_FÆRDIGGØRELSESGRAD.setRowData(r.getFærdiggørelsesgradFormateret());
		HISTORIK_HENVISNINGERQ12013.setRowData(r.getHenvisningerQ12013Formateret());
		HISTORIK_GENNEMFØRTESALGQ12013.setRowData(r.getGennemførtSalgQ12013Formateret());
		HISTORIK_SALGSRATEQ12013.setRowData(r.getSalgsRateQ12013Formateret());
	}
	
	public static void getEkspeditionGoalText() {
		for (EColumn c: EColumn.values()){
			if(c.equals(BEREGNINGER_AHT))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: &lt;=04:40<br>Rød: >=04:55");
			else if(c.equals(HISTORIK_SALGSRATEQ12013))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: >=12,5%<br>Rød: &lt;=11,8%");			
			else if(c.equals(EKSPEDITION_HENVISNINGERPRLOGINTIME))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: >=0,15<br>Rød: &lt;=0,09");
			else if(c.equals(BEREGNINGER_TILGÆNGELIGHED))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: >=66%<br>Rød: &lt;=62%");			
			else 
				c.setRowData("");
		}
	}
	public static void getE1GoalText() {
		for (EColumn c: EColumn.values()){
			if(c.equals(BEREGNINGER_AHT))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: &lt;=04:38<br>Rød: >=04:52");
			else if(c.equals(HISTORIK_SALGSRATEQ12013))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: >=12,5%<br>Rød: &lt;=11,8%");			
			else if(c.equals(EKSPEDITION_HENVISNINGERPRLOGINTIME))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: >=0,15<br>Rød: &lt;=0,09");
			else if(c.equals(BEREGNINGER_TILGÆNGELIGHED))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: >=67%<br>Rød: &lt;=63%");			
			else 
				c.setRowData("");
		}
	}
	public static void getE2Text() {
		for (EColumn c: EColumn.values()){
			if(c.equals(BEREGNINGER_AHT))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: &lt;=04:44<br>Rød: >=05:00");
			else if(c.equals(HISTORIK_SALGSRATEQ12013))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: >=12,5%<br>Rød: &lt;=11,8%");			
			else if(c.equals(EKSPEDITION_HENVISNINGERPRLOGINTIME))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: >=0,15<br>Rød: &lt;=0,09");
			else if(c.equals(BEREGNINGER_TILGÆNGELIGHED))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: >=64%<br>Rød: &lt;=60%");			
			else 
				c.setRowData("");
		}
	}
	
	public int getTrueFalseValue(){
		if (selected){return 1;}{return 0;}
	}
	
	public void setTrueFalseValue(int value){
		if (value == 1){
			selected = true; 
			checkBox.setSelected(true);
		}
		else{
			selected = false; 
			checkBox.setSelected(false);
		}
	}

	public String getTableHeaderName() {
		return "<html><p style=\"text-align: center;\">"+tableHeaderName;
	}

	public String toString(){
		if (this.getCategory().equals(ECategory.COLORING)| this.getCategory().equals(ECategory.SETTINGS))
			return this.getCheckBoxName();
		else 
			return this.getTableHeaderName();
	}

}
