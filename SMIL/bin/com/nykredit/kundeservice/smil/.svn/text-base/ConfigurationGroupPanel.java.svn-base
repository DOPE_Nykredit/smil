package com.nykredit.kundeservice.smil;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JPanel;


public class ConfigurationGroupPanel extends JPanel{

	private static final long serialVersionUID = 1L;
	private JCheckBox headerCheckBox;
	private ECategory category;
	private ConfigurationPanel parent;
	private boolean allSelected = true;

	public ConfigurationGroupPanel(ConfigurationPanel parent,ECategory category){
		this.parent = parent;
		this.category = category;
		this.setOpaque(false);
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		headerCheckBox = getCategoryCheckBox(category);
		
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(5,5,0,0);
		this.add(headerCheckBox,c);
		c.insets = new Insets(0,15,5,0);
		c.fill = GridBagConstraints.NONE;
		c.gridx = 0;
		c.gridy = 1;
		this.add(getConfigurationGroupPanel(),c);
	}
	
	private JPanel getConfigurationGroupPanel(){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();	
		p.setOpaque(false);
		c.anchor = GridBagConstraints.NORTH;
		c.fill =GridBagConstraints.HORIZONTAL;
		c.weighty = 1;
		c.gridx=0;
		
		int y = 0;
		for (final EColumn col: EColumn.values()){
			if (col.getCategory().equals(category)){
				c.gridy = y;
				
				col.getCheckBox().addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent arg0) {
						if(col.getCheckBox().isSelected()){
							col.setSelected(true);

							if (getSelectedState())
								headerCheckBox.setSelected(true);
							}
						else{
							col.setSelected(false);
							headerCheckBox.setSelected(false);
							allSelected = false;
						}
						parent.updateConfiguration();
					}});
				p.add(col.getCheckBox(),c);
				
				y++;
			}
		}
		
		return p;
	}
	
	private JCheckBox getCategoryCheckBox(final ECategory category){
		headerCheckBox = new JCheckBox(category.toString());
		headerCheckBox.setFont(new Font("Verdana", Font.BOLD, 12));
		headerCheckBox.setOpaque(false);
		headerCheckBox.setSelected(getSelectedState());
		headerCheckBox.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(headerCheckBox.isSelected()){
					setSelectedState(true);
				}else{
					setSelectedState(false);
				}
				parent.updateConfiguration();
			}
		});
		return headerCheckBox;
	}
	
	public void setSelectedState(boolean selectAll){
		allSelected = selectAll;
		
		headerCheckBox.setSelected(selectAll);
		
		for (EColumn col: EColumn.values()){
			if (col.getCategory().equals(category)){
				col.getCheckBox().setSelected(selectAll);
				col.setSelected(selectAll);
			}
		}
		parent.updateConfiguration();
	}

	public boolean getSelectedState(){
		allSelected = true;
		for (EColumn col: EColumn.values()){
			if(!col.getCheckBox().isSelected() & col.getCategory().equals(category))
				allSelected = false;
		}
		return allSelected;
	}
	
	public void setHeaderCheckBox(boolean selected){
		headerCheckBox.setSelected(selected);
	}
}