package com.nykredit.kundeservice.smil;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class ConfigurationPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private AdHocTable adHocTable;
	private Data aOracle;
	private JCheckBox selectAllCheckBox;
	/**
	 * kaldPanel er metoden til kolonnen KALD
	 */
	private ConfigurationGroupPanel kaldPanel;
	private ConfigurationGroupPanel ctiPanel;
	private ConfigurationGroupPanel salgPanel;
	private ConfigurationGroupPanel aktiviteterPanel;
	private ConfigurationGroupPanel beregningerPanel;
	private ConfigurationGroupPanel ekspeditionsservicePanel;

	/**
	 * @param aOracle
	 * @param adHocTable
	 */
	public ConfigurationPanel(Data aOracle, AdHocTable adHocTable){
		this.kaldPanel = new ConfigurationGroupPanel(this,ECategory.KALD); //kolonne Kald i fanebladet Ops�tning
		this.ctiPanel = new ConfigurationGroupPanel(this,ECategory.CTI); //Kolonne CTI i fanebladet Ops�tning
		this.salgPanel = new ConfigurationGroupPanel(this,ECategory.SALG); //Kolonne Salg i fanebladet Ops�tning
		this.aktiviteterPanel = new ConfigurationGroupPanel(this,ECategory.AKTIVITETER); //Kolonne Aktiviteter i fanebladet Ops�tning
		this.beregningerPanel = new ConfigurationGroupPanel(this,ECategory.BEREGNINGER); // Kolonne Beregninger i fanebladet Ops�tning
		this.ekspeditionsservicePanel = new ConfigurationGroupPanel(this,ECategory.EKSPEDITIONSSERVICE); //Kolonne Ekspeditionsservice i fanebladet Ops�tning
		this.aOracle = aOracle;
		this.adHocTable = adHocTable;
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		this.setOpaque(false);
		c.anchor = GridBagConstraints.NORTHWEST;

		c.gridx = 0;
		c.gridy = 0;
		this.add(kaldPanel,c);
		
		c.gridx = 1;
		c.gridy = 0;
		this.add(ctiPanel,c);
		
		c.gridx = 2;
		c.gridy = 0;
		this.add(salgPanel,c);
		
		c.gridx = 3;
		c.gridy = 0;
		this.add(aktiviteterPanel,c);

		c.gridx = 4;
		c.gridy = 0;
		this.add(beregningerPanel,c);
		
		c.gridx = 5;
		c.gridy = 0;
		this.add(ekspeditionsservicePanel,c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 6;		
		this.add(getSelectAllCheckBox(),c);
		
		c.gridx = 0;
		c.gridy = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		this.add(getConfigurationButtonPanel(),c);
		
		c.weightx = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 6;
		this.add(new JLabel(" ",JLabel.LEFT),c);		

		c.gridwidth = 1;
		c.gridx = 6;
		c.gridy = 0;
		c.gridheight = 4;
		this.add(new JLabel(" ",JLabel.LEFT),c);	

	}
	

	private JPanel getConfigurationButtonPanel(){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		p.setOpaque(false);
		p.setBorder(BorderFactory.createTitledBorder("Standard ops�tning"));
		c.insets = new Insets(0,5,0,0);
		c.gridx =0;
		c.gridy =0;			
		c.anchor = GridBagConstraints.CENTER; 				//hovedoverskrift til knapperne under Basis
		p.add(getHeaderLabel("Basis"),c); 					//Placering af Knappen
		c.gridy =1;	
		p.add(getButtonStandardBasisKoncern(),c);			//Placering af Knappen Koncern
		c.gridy =2;	
		p.add(getButtonStandardBasisB2B8(),c);				//Placering af Knappen B1-B7
		c.gridy =3;
		p.add(getButtonStandardRaadgiverForsikring(),c); 	//Placering af Knappen Forsikring
		c.gridx =1;
		c.gridy =0;		
		p.add(getHeaderLabel("Digitale Services"),c);
		c.gridy =1;			
		p.add(getButtonStandardRaadgiverNetbank(),c); 		//Placering af Knappen Netbank
		c.gridy =2;			
		p.add(getButtonStandardRaadgiverErhverv(),c); 		//Placering af Knappen Erhverv
		c.gridy =3;			
		p.add(getButtonStandardRaadgiverSupport(),c); 		//Placering af Knappen Support
		c.gridy =4;
		p.add(getButtonStandardMailService(),c); 			//Placering af Knappen Mailservice
		c.gridx =2;
		c.gridy =0;	
		c.anchor = GridBagConstraints.CENTER;
		p.add(getHeaderLabel("Ekspedition"),c); 			//hovedoverskrift til knapperne under Ekspedidition
		c.gridy =1;	
		p.add(getButtonStandardEkspedition(),c); 			//Placering af Knappen E1-E2
		c.gridx = 7;
		c.gridy = 0;
		c.gridheight = 100;
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		p.add(new JLabel(""),c);
		
		return p;
	}
	// Kode til knappen "Koncern" standard ops�tning for Koncern
	private JButton getButtonStandardBasisKoncern(){
		JButton b = getStandardButton("Koncern");
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				clearHeaderCheckBoxes();
				 for(EColumn col:EColumn.values()){
					 if(
						col.equals(EColumn.KORRIGERET)
						| col.equals(EColumn.TILG�NGELIGHED)
						| col.equals(EColumn.EMAIL)
						| col.equals(EColumn.INDKALD)
						| col.equals(EColumn.SHOWAGENTS)//Med p� alle knapper
						| col.equals(EColumn.SHOWTOTAL)	//Med p� alle knapper
						){
						 col.setSelected(true);
					 }else{
						 col.setSelected(false); 
					 }
				 }
				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORINGKONCERN);
				 updateConfiguration(); 
			}});
		return b;
	}
	// Kode til knappen "B1-B7" standard ops�tning for B1-B7
	private JButton getButtonStandardBasisB2B8(){
		JButton b = getStandardButton("B1-B7");
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {

				clearHeaderCheckBoxes();
				 for(EColumn col:EColumn.values()){
					 if(col.equals(EColumn.KORRIGERET) || 
						col.equals(EColumn.TILG�NGELIGHED) || 
						col.equals(EColumn.INDKALD) || 
						col.equals(EColumn.HENVISNINGER) || 
						col.equals(EColumn.FORSIKRINGSP) || 
						col.equals(EColumn.KPI)	|| 
						col.equals(EColumn.SHOWAGENTS) || //Med p� alle knapper
						col.equals(EColumn.SHOWSUBTOTAL) || 
						col.equals(EColumn.SHOWTOTAL /*Med p� alle knapper*/ )){
						 col.setSelected(true);
					 }else{
						 col.setSelected(false); 
					 }
				 }
				 
				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORINGB1B7);
				 updateConfiguration(); 
			}});
		return b;
	}
	// Kode til knappen "Mailservice" standard ops�tning for Mailservice
	private JButton getButtonStandardMailService(){
		JButton b = getStandardButton("Mailservice");
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				
				clearHeaderCheckBoxes();
				
				 for(EColumn col:EColumn.values()){
					 if(
						col.equals(EColumn.KORRIGERET)
						| col.equals(EColumn.INDKALD)
						| col.equals(EColumn.WEBDESK)
						| col.equals(EColumn.HENVISNINGER)		
						| col.equals(EColumn.FORSIKRINGSP)		
						| col.equals(EColumn.KPI)	
						| col.equals(EColumn.SHOWAGENTS)//Med p� alle knapper
						| col.equals(EColumn.SHOWTOTAL)	//Med p� alle knapper
						){
						 col.setSelected(true);
					 }else{
						 col.setSelected(false); 
					 }
				 }
				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORING_MS);
				 updateConfiguration(); 
			}});
		return b;
	}
	
	
	private JLabel getHeaderLabel(String text){
		JLabel l = new JLabel(text,JLabel.CENTER);
		l.setFont(new Font("Verdana", Font.BOLD, 12));
		return l;
	}
	// Kode til knappen "R1-R2" standard ops�tning for R1-R2
	private JButton getButtonStandardRaadgiverR1R2(){
		JButton b = getStandardButton("R1-R2");
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				
				clearHeaderCheckBoxes();
				
				 for(EColumn col:EColumn.values()){
					 if(col.equals(EColumn.KORRIGERET) || 
						col.equals(EColumn.CBRKALD) || 
						col.equals(EColumn.TILG�NGELIGHED) || 
						col.equals(EColumn.INDKALD) || 
						col.equals(EColumn.CONSULTKALD) || 
						col.equals(EColumn.HENVISNINGER) || 
						col.equals(EColumn.AKTINDTIDBE) || 
						col.equals(EColumn.KPIRAADGIVER) || 
						col.equals(EColumn.RAADGIVERPRODUKTER) || 
						col.equals(EColumn.SHOWAGENTS) || //Med p� alle knapper 
						col.equals(EColumn.SHOWSUBTOTAL) || 
						col.equals(EColumn.SHOWTOTAL)){	//Med p� alle knapper
						 col.setSelected(true);
					 }else{
						 col.setSelected(false); 
					 }
				 }
				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORINGR1R2);
				 updateConfiguration(); 
			}});
		return b;
	}
	// Kode til knappen "Netbank" standard ops�tning for Netbank
	private JButton getButtonStandardRaadgiverNetbank(){
		JButton b = getStandardButton("NetBank");
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				
				clearHeaderCheckBoxes();
				
				 for(EColumn col:EColumn.values()){
					 if(
						col.equals(EColumn.KORRIGERET)
						| col.equals(EColumn.TILG�NGELIGHED)
						| col.equals(EColumn.INDKALD)
						| col.equals(EColumn.AKTINDTIDBE)
						| col.equals(EColumn.SHOWAGENTS)//Med p� alle knapper
						| col.equals(EColumn.SHOWTOTAL)	//Med p� alle knapper
						){
						 col.setSelected(true);
					 }else{
						 col.setSelected(false); 
					 }
				 }
				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORINGNETBANK);
				 updateConfiguration(); 
			}});
		return b;
	}
	// Kode til knappen "Forsikring" standard ops�tning for Forsikring
	private JButton getButtonStandardRaadgiverForsikring(){
		JButton b = getStandardButton("Forsikring");
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				
				clearHeaderCheckBoxes();
				
				 for(EColumn col:EColumn.values()){
					 if(
						col.equals(EColumn.KORRIGERET)
						| col.equals(EColumn.TILG�NGELIGHED)
						| col.equals(EColumn.MERSALG)						
						| col.equals(EColumn.INDKALD)
						| col.equals(EColumn.HENVISNINGER)
						| col.equals(EColumn.FORSIKRINGSP)
						| col.equals(EColumn.AKTINDTIDBE)
						| col.equals(EColumn.KPI)
						| col.equals(EColumn.SHOWAGENTS)//Med p� alle knapper
						| col.equals(EColumn.SHOWTOTAL)	//Med p� alle knapper
						| col.equals(EColumn.PROCENTVISMERSALG)							
						){
						 col.setSelected(true);
					 }else{
						 col.setSelected(false); 
					 }
				 }
				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORINGFORSIKRING);
				 updateConfiguration(); 
			}});
		return b;
	}
	
	// Kode til knappen "Erhverv" standard ops�tning for Erhverv
	private JButton getButtonStandardRaadgiverErhverv(){
		JButton b = getStandardButton("Erhverv");
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				
				clearHeaderCheckBoxes();
				
				 for(EColumn col:EColumn.values()){
					 if(
						col.equals(EColumn.KORRIGERET)
						| col.equals(EColumn.TILG�NGELIGHED)
						| col.equals(EColumn.INDKALD)
						| col.equals(EColumn.KPI)
						| col.equals(EColumn.AKTPRLOGT)
						| col.equals(EColumn.AKTINDTIDBE)
						| col.equals(EColumn.SHOWAGENTS) //Med p� alle knapper
						| col.equals(EColumn.SHOWTOTAL)//Med p� alle knapper
						){
						 col.setSelected(true);
					 }else{
						 col.setSelected(false); 
					 }
				 }
				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORINGERHVERV);
				 updateConfiguration(); 
			}});
		return b;
	}
	// Kode til knappen "Support" standard ops�tning for Support
	private JButton getButtonStandardRaadgiverSupport(){
		JButton b = getStandardButton("Support");
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				
				clearHeaderCheckBoxes();
				
				 for(EColumn col:EColumn.values()){
					 if(
						col.equals(EColumn.KORRIGERET)
						| col.equals(EColumn.EMAIL)
						| col.equals(EColumn.ANDREOPG)
						| col.equals(EColumn.OPRETTEDEKAMPAGNEPOTENTIALER)
						| col.equals(EColumn.AKTINDTIDBE)
						| col.equals(EColumn.SUPPORTOPGAVER)
						| col.equals(EColumn.SUPPORTOPGINDENFORFRIST)
						| col.equals(EColumn.KPISUPPORT)
						| col.equals(EColumn.SHOWAGENTS) //med p� alle knapper
						| col.equals(EColumn.SHOWTOTAL)	//med p� alle knapper
						| col.equals(EColumn.HIDEEMPTYLOGIN)//med p� alle knapper
						){
						 col.setSelected(true);
					 }else{
						 col.setSelected(false); 
					 }
				 }
				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORINGSUPPORT);
				 updateConfiguration(); 
			}});
		return b;
	}
// Kode til knappen "E1-E2" standard ops�tning for E1-E2
	private JButton getButtonStandardEkspedition(){
		JButton b = getStandardButton("E1-E2");
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				
				clearHeaderCheckBoxes();
				
				 for(EColumn col:EColumn.values()){
					 if(
						col.equals(EColumn.KORRIGERET)
						| col.equals(EColumn.CBRKALD)
						| col.equals(EColumn.INDKALD)
						| col.equals(EColumn.UDKALD)
						| col.equals(EColumn.GNSH�ND)
						| col.equals(EColumn.TILG�NGELIGHED)		
						| col.equals(EColumn.HENVISNINGER)
						| col.equals(EColumn.SERVICEPRODUKTERPRLOGINTIME)
						| col.equals(EColumn.KPI)						
						| col.equals(EColumn.OPRAKTIVITETIKKEVIDRESTILLING)
						| col.equals(EColumn.OPF�LGNINGSSERVICE)
						| col.equals(EColumn.HENVISNINGERPRLOGINTIME)
						| col.equals(EColumn.TILBUDTESERVICEPRODUKTER)
						| col.equals(EColumn.TABTSALG)
						| col.equals(EColumn.SHOWAGENTS)//Med p� alle knapper
						| col.equals(EColumn.SHOWTOTAL)	//Med p� alle knapper
						| col.equals(EColumn.HIDEEMPTYLOGIN)	//Med p� alle knapper
						){
						 col.setSelected(true);
					 }else{
						 col.setSelected(false); 
					 }
				 }
				 Main.getStaticFormattingComboBox().setSelectedItem(EColumn.COLORINGE1E2);
				 updateConfiguration(); 
			}});
		return b;
	}
	
	private void clearHeaderCheckBoxes(){
		 kaldPanel.setHeaderCheckBox(false);
		 ctiPanel.setHeaderCheckBox(false);
		 salgPanel.setHeaderCheckBox(false);
		 aktiviteterPanel.setHeaderCheckBox(false);
		 beregningerPanel.setHeaderCheckBox(false);
		 ekspeditionsservicePanel.setHeaderCheckBox(false);
	}

	private JButton getStandardButton(String text){
		JButton b = new JButton(text);
		b.setPreferredSize(new Dimension(100,20));
		b.setMinimumSize(new Dimension(100,20));
		return b;
	}
	//kode som bruges til funktionaliteten af checkboxen "V�lg Alle"
	private JCheckBox getSelectAllCheckBox(){
		selectAllCheckBox = new JCheckBox("V�lg alle");
		selectAllCheckBox.setFont(new Font("Verdana", Font.BOLD, 12));
		selectAllCheckBox.setOpaque(false);
		
		if (kaldPanel.getSelectedState()&
			ctiPanel.getSelectedState()&
			salgPanel.getSelectedState()&
			aktiviteterPanel.getSelectedState()&
			ekspeditionsservicePanel.getSelectedState()){
			selectAllCheckBox.setSelected(true);
		}else{
			selectAllCheckBox.setSelected(false);
		}

		selectAllCheckBox.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				if(selectAllCheckBox.isSelected()){
					 kaldPanel.setSelectedState(true);
					 ctiPanel.setSelectedState(true);
					 salgPanel.setSelectedState(true);
					 aktiviteterPanel.setSelectedState(true);
					 beregningerPanel.setSelectedState(true);
					 ekspeditionsservicePanel.setSelectedState(true);	
					
				}else{
					 kaldPanel.setSelectedState(false);
					 ctiPanel.setSelectedState(false);
					 salgPanel.setSelectedState(false);
					 aktiviteterPanel.setSelectedState(false);
					 beregningerPanel.setSelectedState(false);
					 ekspeditionsservicePanel.setSelectedState(false);	
				}
			}
		});
		return selectAllCheckBox;
	}
	
	public void setSelectedAllCheckBox(boolean selected){
		selectAllCheckBox.setSelected(selected);
	}
	
	public void updateConfiguration(){
		adHocTable.setAdHocTableModel();
		aOracle.setConfiguration();
		selectAllCheckBox.setSelected(getOverAllSelectedState());
	}
	
	public boolean getOverAllSelectedState(){
		boolean allSelected = true;
		for (EColumn c: EColumn.values()){
			if(!c.getCheckBox().isSelected() & (!c.getCategory().equals(ECategory.SETTINGS)&!c.getCategory().equals(ECategory.COLORING)))
				allSelected = false;
		}
		return allSelected;
	}

}
