package com.nykredit.kundeservice.smil;

import javax.swing.JCheckBox;


/**
 * Navnene på de enkelte checkboxe i fanebladet Opsætning og hvilke "forespørgsler de kalder
 *
 */

public enum EColumn {
	/* KALD */
	CBRKALD(ECategory.KALD,
			"CBR kald",
			"CBR kald", 
			false),
	CONSULTKALD(ECategory.KALD, 
				"Consult kald",
				"Consult<br>kald",
				false),
	FIRSTCALL(ECategory.KALD, 
			  "FirstCall kald",
			  "FirstCall<br>kald", 
			  false),
	FIRSTCALLUDENTELEFONBESKEDER(ECategory.KALD,
								 "FirstCall uden telefonbeskeder",
								 "FirstCall<br>uden<br>tlf. besk.",
								 false),
	INDKALD(ECategory.KALD,
			"Indgående kald",
			"Ind.<br>kald", 
			true), 
	KALDIALT(ECategory.KALD, 
			 "Kald i alt",
			 "Kald<br>i alt", 
			 false),
	UDKALD(ECategory.KALD, 
		   "Udgående kald",
		   "Udg.<br>kald", 
		   false),
	WEBDESKKALD(ECategory.KALD,
			    "Webdesk kald",
			    "Webdesk<br>kald",
			    false),
	/* CTI */
	ANDREOPG(ECategory.CTI, 
			 "Andre opgaver",
			 "Andre<br>opgaver", 
			 false), 
	EFTERBEH(ECategory.CTI,
			 "Efterbehandling",
			 "Efter-<br>behandling", 
			 false),
	EMAIL(ECategory.CTI, 
		  "Email tid",
		  "Email<br>tid", 
		  false),
	FROKOST(ECategory.CTI, 
			"Frokost",
			"Frokost", 
			false), 
	KLAR(ECategory.CTI, 
		 "Klar",
		 "Klar", 
		 false), 
	KOLLEGAHJ(ECategory.CTI,
			  "Kollegahjælp",
			  "Kollega-<br>hjælp", 
			  false),
	KORRIGERET(ECategory.CTI,
			   "Korrigeret Login",
			   "Korrigeret<br>login", 
			   true), 
	MØDE(ECategory.CTI, 
		 "Møde",
		 "Møde", 
		 false), 
	PAUSE(ECategory.CTI,
		  "Pause",
		  "Pause", 
		  false), 
	SPÆRRET(ECategory.CTI, 
			"Spærret",
			"Spærret", 
			false), 
	TALER(ECategory.CTI,
		  "Taler",
		  "Taler", 
		  false), 
	UDDANNELSE(ECategory.CTI, 
			   "Uddannelses",
			   "Uddannelse", 
			   false), 
	WEBDESK(ECategory.CTI, 
			"Webdesk",
			"Webdesk", 
			false), 
	ØVRIG(ECategory.CTI, 
		  "Øvrig tid",
		  "Øvrig<br>tid", 
		  false),
		  
	/* SALG */
	FORSIKRINGSP(ECategory.SALG,
			     "Forsikringspræmie",
			     "Forsikr.<br>præmie", 
			     true),
	HENVISNINGER(ECategory.SALG,
				 "Henvisninger",
				 "Henvis-<br>ninger", 
				 true),
	HENVISNINGERHITRATE(ECategory.SALG,"Henvisninger Hitrate","Henvis-<br>ninger <br>Hitrate", true),
	MERSALG(ECategory.SALG, "Mersalg","Mersalg", true),
	OPRETTEDEKAMPAGNEPOTENTIALER(ECategory.SALG,"Oprettede kampagnepotentialer","Oprettede<br>kampagne-<br>potentialer",false),
	/* AKTIVITETER */
	AKTIVITETEROPR(ECategory.AKTIVITETER,"Oprettede","Akt.<br>opr.",false),
	AKTIVITETER(ECategory.AKTIVITETER, "Afsluttede","Akt.<br>afs.", false),
	AKTIVITETER_AFSLUTTET_INDENFOR_2_TIMER_EXCL_EMAIL(ECategory.AKTIVITETER,"Afsl. indenfor 2t. (ekskl. e-mail)", "Afsl. akt<br>ind. 2t.<br>(u. e-mail)",false),
	AKTIVITETER_AFSLUTTET_INDENFOR_2_TIMER_INKL_EMAIL(ECategory.AKTIVITETER, "Afsl. indenfor 2 t. (inkl. e-mail)", "Afsl. akt<br>ind. 2t.<br>(m. e-mail)", false),
	AKTEMAILIND(ECategory.AKTIVITETER, "Email indgående","Email<br>indg.", false),
	AKTINDTID(ECategory.AKTIVITETER,"indenfor tidsfrist","Akt.<br>ind.<br>tidsfr.", false),
	AKTOVER(ECategory.AKTIVITETER, "Overskredne","Akt.<br>overskr.",false), 
	TLFBESK(ECategory.AKTIVITETER, "Telefonbeskeder","Akt.<br>tlf<br>besk.", false),
	SUPPORTOPGAVER(ECategory.AKTIVITETER, "Support opgaver","Supp.<br>opgaver", false),
	SUPPORTOPGAVEROVERSKREDNE(ECategory.AKTIVITETER, "Overskredne Supportopgaver","Overskr.<br>Supp.<br>opg.", false),
	SUPPORTOPGINDENFORFRIST(ECategory.AKTIVITETER, "Supp.opg. indenfor tidsfrist","Supp.opg<br>indenfor<br>tidsf.", false),
	/* BEREGNINGER */
	AKTINDTIDBE(ECategory.BEREGNINGER, "Aktiviteter indenfor tidsfrist","Akt.<br>ind.<br>tidsfr. %", true),
	AKTPRLOGT(ECategory.BEREGNINGER, "Aktiviteter pr. logintime","Akt. pr.<br>logintime", true),
	EMAILAKTPRLOGT(ECategory.BEREGNINGER, "Emails pr. logintime","Email<br>pr.<br>logintime", true),
	EMAILAKTPREMAILLOGT(ECategory.BEREGNINGER, "Emails pr. Email time","Email<br>pr.<br>Email time", true),
	EMAILTILG(ECategory.BEREGNINGER, "Email tilgængelighed","Email<br>tilgænge-<br>lighed", false),
	FÆRDIGGG(ECategory.BEREGNINGER,"Færdiggørelsesgrad","Færdig-<br>gørelse", true), 
	GNSHÅND(ECategory.BEREGNINGER,"Gns. håndtering","Gns.<br>håndtering", true),
	KPI(ECategory.BEREGNINGER, "KPI","KPI", true),
	KPIRAADGIVER(ECategory.BEREGNINGER, "KPI m. Webdesk","KPI m. Webdesk", true),
	KPISUPPORT(ECategory.BEREGNINGER, "KPI m. supportopgaver","KPI m.<br>support-<br>opgaver", true),
	PAUSEPCT(ECategory.BEREGNINGER, "Pause % logintime","Pause<br>%<br>logintime", false), 
	TILGÆNGELIGHED(ECategory.BEREGNINGER, "Tilgængelighed","Tilgænge-<br>lighed", true),
	TILGÆNGELIGHED2011(ECategory.BEREGNINGER, "Tilgængelighed 2011","Tilgænge-<br>lighed 2011", false),
	TLFTILG(ECategory.BEREGNINGER,"Telefon Tilgænglighed","Tlf.<br>tilgænge-<br>lighed", false),
	PLANLAGTTID(ECategory.BEREGNINGER,"WFCM Planlagttid","WFCM<br>planlagt<br>tid",false),
	KAMPAGNEPOTENTIALERPREMAILTIME(ECategory.BEREGNINGER,"Kampagnepotentialer pr. time","Kampagne-<br>potentialer<br>pr. time",false),
	AKTFORSKOPRPRANDREOPGTIME(ECategory.BEREGNINGER,"Aktiviteter pr. Andreopgavetime","Akt. pr.<br>Andre opg.<br>time",false),
	PROCENTVISMERSALG(ECategory.BEREGNINGER,"Procentvis mersalg","Procentvis<br>mersalg",false),
	RAADGIVERPRODUKTER(ECategory.BEREGNINGER,"Rådgiver produkter","Rådgiver<br>produkter",false),
	WEBDESKTALERTID(ECategory.BEREGNINGER,"Webdesk taler tid", "Webdesk<br>taler<br>tid", false),
	/* Settings */
	SHOWAVERAGE(ECategory.SETTINGS,
			    "Gns. B1-B7 & R1-R2",
			    "", 
			    false),
	SHOWTOP10(ECategory.SETTINGS,
			  "Top10% B1-B7 & R1-R2",
			  "", 
			  false),
	SHOWSUBTOTAL(ECategory.SETTINGS,
			     "Vis Sub total",
			     "",
			     true),
	SHOWTOTAL(ECategory.SETTINGS,
			  "Vis Total",
			  "",
			  true),
	SHOWAGENTS(ECategory.SETTINGS,
			   "Vis initialer",
			   "",
			   true),
	HIDEEMPTYLOGIN(ECategory.SETTINGS,
			       "Skjul mederarb. u. login",
			       "",
			       false),
	
	/* Coloring */
	NOCOLORS(ECategory.COLORING,
			 "Ingen farve formatering",
			 "",
			 true),
	COLORINGKONCERN(ECategory.COLORING,
					"Koncern",
					"",
					false),
	COLORINGB1B7(ECategory.COLORING,
			     "B1-B7",
			     "",
			     false),
	COLORING_MS(ECategory.COLORING,
			    "Mailservice",
			    "",
			    false),
	COLORINGR1R2(ECategory.COLORING,
				 "R1-R2",
				 "",
				 false),
	COLORINGNETBANK(ECategory.COLORING,
					"Netbank",
					"",
					false),
	COLORINGFORSIKRING(ECategory.COLORING,
					   "Forsikring",
					   "",
					   false),
	
	//COLORINGVIP(ECategory.COLORING,"VIP","",false),
	COLORINGERHVERV(ECategory.COLORING,
					"Erhverv",
					"",
					false),
	COLORINGSUPPORT(ECategory.COLORING,
					"Support",
					"",
					false),
	COLORINGE1E2(ECategory.COLORING,
				 "E1-E2",
				 "",
				 false),
	
	/*Ekspeditionsservice*/
	OPFØLGNINGSSERVICE(ECategory.EKSPEDITIONSSERVICE,
					   "Opfølgningsservice afsluttet",
					   "Opf.<br>service<br>afs.",
					   false),
	INDGÅENDEKALDDIALOG(ECategory.EKSPEDITIONSSERVICE,
						"Aktiviteter indgående kald",
						"Akt.<br>ind.<br>kald",
						false),
	PLANLAGTTIDSTUDERENDE(ECategory.EKSPEDITIONSSERVICE,
						  "WFCM planlagttid studerende",
						  "WFCM<br>planlagt<br>tid stud.",
						  false),
	KALDPRTIME(ECategory.EKSPEDITIONSSERVICE,
			   "Kald pr. logintime",
			   "Kald<br>pr.<br>logintime",
			   false),
	OPRAKTIVITETIKKEVIDRESTILLING(ECategory.EKSPEDITIONSSERVICE,
								  "Dialoreg Opr. på IKKE viderestillede kald",
								  "Opr. akt.<br>på IKKE<br>vdr. kald",
								  false),
	TLFBESKEDERPCTINDKALD(ECategory.EKSPEDITIONSSERVICE, 
						  "Telefonbeskeder % indgående kald", 
						  "Tlf. besk.<br>%<br>ind. kald", 
						  false),
	VIDERESTILLINGERPCTINDKALD(ECategory.EKSPEDITIONSSERVICE, 
							   "Viderestillinger % indgående kald", 
							   "Vdr. %<br>ind.<br>kald", 
							   false),
	FÆRDIGGØRELSESGRADPCTINDKALD(ECategory.EKSPEDITIONSSERVICE, 
								 "Færdiggørelse % indgående kald",
								 "Færdiggr.<br>% ind.<br>kald", 
								 false),
	HENVISNINGERPRLOGINTIME(ECategory.EKSPEDITIONSSERVICE, 
							"Henvisninger pr. logintime", 
							"Henv.<br>pr.<br>logintime", 
							false),
	VIDERESTILLINGER(ECategory.EKSPEDITIONSSERVICE, 
					 "Viderestillinger",
					 "Videre-<br>stillinger", 
					 false),
	TILBUDTESERVICEPRODUKTER(ECategory.EKSPEDITIONSSERVICE, 
							 "Serviceprodukter",
							 "Service-<br>produkter", 
							 false),
	SOLGTESERVICEPRODUKTER(ECategory.EKSPEDITIONSSERVICE, 
						   "Solgte serviceprodukter 2011",
						   "Solgte<br>service-<br>prod. 2011",  
						   false),
	SERVICEPRODUKTERPRLOGINTIME(ECategory.EKSPEDITIONSSERVICE, 
								"Serviceprodukter pr. logintime",
								"Serviceprod.<br>pr.<br>logintime", 
								false),
	TABTSALG(ECategory.EKSPEDITIONSSERVICE, 
			 "Oprettede aktiviteter tabt salg", 
			 "Opr. akt.<br>tabt salg", 
			 false);

	private ECategory category;
	private String checkBoxName;
	private String tableHeaderName;
	private boolean selected;
	private String rowData;
	private JCheckBox checkBox;

	EColumn(ECategory category, String checkBoxName, String tableHeaderName, boolean isselected) {
		this.category = category;
		this.checkBoxName = checkBoxName;
		this.tableHeaderName = tableHeaderName;
		this.selected = isselected;
		this.checkBox = new JCheckBox(checkBoxName, selected);
		checkBox.setOpaque(false);	
		}

	public String getCheckBoxName() {
		return checkBoxName;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
		this.checkBox.setSelected(selected);
	}
	
	public String getRowData() {
		return rowData;
	}

	public void setCheckBoxName(String checkBoxName) {
		this.checkBoxName = checkBoxName;
	}

	public void setRowData(String rowData) {
		this.rowData = rowData;
	}
	
	public JCheckBox getCheckBox() {
		return checkBox;
	}

	public void setCheckBox(JCheckBox checkBox) {
		this.checkBox = checkBox;
	}

	public ECategory getCategory() {
		return category;
	}

	public static void insertRow(DataTableColumns r) {
		AKTIVITETER.setRowData(r.getAktiviteterFormateret());
		AKTEMAILIND.setRowData(r.getEmailIndgåendeFormateret());
		AKTINDTID.setRowData(r.getIndenforTidsfristFormateret());
		AKTINDTIDBE.setRowData(r.getAktiviteterIndenforTidsfristFormateret());
		AKTOVER.setRowData(r.getOverskredneFormateret());
		AKTPRLOGT.setRowData(r.getAktiviteterPrLoginTimeFormateret());
		ANDREOPG.setRowData(r.getAndreOpgTTMM());
		CBRKALD.setRowData(r.getCbrFormateret());
		CONSULTKALD.setRowData(r.getConsultFormateret());
		EFTERBEH.setRowData(r.getEfterbehTTMM());
		EMAIL.setRowData(r.getEmailTTMM());
		EMAILAKTPRLOGT.setRowData(r.getEmailAktiviteterPrLoginTimeFormateret());
		EMAILTILG.setRowData(r.getEmailTilgængelighedFormateret());
		FIRSTCALL.setRowData(r.getFirstCallFormateret());
		FORSIKRINGSP.setRowData(r.getForsikringspræmieFormateret());
		FROKOST.setRowData(r.getFrokostTTMM());
		FÆRDIGGG.setRowData(r.getFærdiggørelsesgradFormateret());
		GNSHÅND.setRowData(r.getGnsHåndteringFormateret());
		HENVISNINGER.setRowData(r.getHenvisningerFormateret());
		HENVISNINGERHITRATE.setRowData(r.getHenvisningerHitrateFormateret());
		INDKALD.setRowData(r.getIndKaldFormateret());
		KALDIALT.setRowData(r.getKaldIaltFormateret());
		KLAR.setRowData(r.getKlarTTMM());
		KOLLEGAHJ.setRowData(r.getKollegahjTTMM());
		KORRIGERET.setRowData(r.getKorrigeretTTMM());
		KPI.setRowData(r.getKPIFormateret());
		KPIRAADGIVER.setRowData(r.getKPIRaadgiverFormateret());
		KPISUPPORT.setRowData(r.getKPISupportFormateret());
		MERSALG.setRowData(r.getMersalgFormateret());
		MØDE.setRowData(r.getMødeTTMM());
		PAUSE.setRowData(r.getPauseTTMM());
		PAUSEPCT.setRowData(r.getPauseTidFormateret());
		SPÆRRET.setRowData(r.getSpærretTTMM());
		TALER.setRowData(r.getTalerTTMM());
		TILGÆNGELIGHED.setRowData(r.getTilgængelighedFormateret());
		TILGÆNGELIGHED2011.setRowData(r.getTilgængelighed2011Formateret());
		TLFBESK.setRowData(r.getTlfBeskFormateret());
		SUPPORTOPGAVER.setRowData(r.getSupportOpgaverFormateret());
		SUPPORTOPGAVEROVERSKREDNE.setRowData(r.getSupportOverskredneFormateret());
		SUPPORTOPGINDENFORFRIST.setRowData(r.getSupportIndenForFristFormateret());
		TLFTILG.setRowData(r.getTlfTilgængelighedFormateret());
		UDDANNELSE.setRowData(r.getUddannelseTTMM());
		UDKALD.setRowData(r.getUdKaldFormateret());
		WEBDESK.setRowData(r.getWebdeskTTMM());
		WEBDESKKALD.setRowData(r.getWebdeskKaldFormatted());
		WEBDESKTALERTID.setRowData(r.getWebdeskTalerTid());
		ØVRIG.setRowData(r.getØvrigTidTTMM());
		KALDPRTIME.setRowData(r.getKaldPrTimeFormateret());
		PLANLAGTTID.setRowData(r.getPlanlagttidTTMM());
		PLANLAGTTIDSTUDERENDE.setRowData(r.getPlanlagttidStuderendeTTMM());
		AKTIVITETEROPR.setRowData(r.getAktiviteterIndgåendeFormateret());
		OPRAKTIVITETIKKEVIDRESTILLING.setRowData(r.getOprettedeAktiviteterIkkeViderestillingerFormateret());
		TILBUDTESERVICEPRODUKTER.setRowData(r.getTilbudteServiceprodukterFormateret());
		SOLGTESERVICEPRODUKTER.setRowData(r.getSolgteServiceprodukterFormateret());
		OPFØLGNINGSSERVICE.setRowData(r.getOpfølgningsserviceAfsluttetFormateret());
		TLFBESKEDERPCTINDKALD.setRowData(r.getTlfBeskederPctIndgåendeKaldFormateret());
		VIDERESTILLINGERPCTINDKALD.setRowData(r.getViderestillingerPctIndgåendeKaldFormateret());
		FÆRDIGGØRELSESGRADPCTINDKALD.setRowData(r.getFærdiggørelsesgradPctIndgåendeKaldFormateret());
		HENVISNINGERPRLOGINTIME.setRowData(r.getHenvisningerPrLoginTimeFormateret());
		VIDERESTILLINGER.setRowData(r.getViderestillingerFormateret());
		INDGÅENDEKALDDIALOG.setRowData(r.getIndgåendeKaldDialogFormateret());
		FIRSTCALLUDENTELEFONBESKEDER.setRowData(r.getFirstCallUdenTelefonBeskederFormateret());
		TABTSALG.setRowData(r.getTabtSalgFormateret());
		EMAILAKTPREMAILLOGT.setRowData(r.getEmailAktiviteterPrEmailLoginFormateret());
		OPRETTEDEKAMPAGNEPOTENTIALER.setRowData(r.getKampagnePotentialerFormateret());
		KAMPAGNEPOTENTIALERPREMAILTIME.setRowData(r.getKampagnepotentialerPrEmailTimeFormateret());
		AKTIVITETER_AFSLUTTET_INDENFOR_2_TIMER_EXCL_EMAIL.setRowData(r.getAktiviteterAfsluttetIndenfor2TimerEksklEmailFormateret());
		AKTIVITETER_AFSLUTTET_INDENFOR_2_TIMER_INKL_EMAIL.setRowData(r.getAktiviteterAfsluttetIndenfor2TimerInklEmailFormateret());
		AKTFORSKOPRPRANDREOPGTIME.setRowData(r.getAktiviteterAfsluttetIndenfor2TimerPrAndreOpgTimeFormateret());
		PROCENTVISMERSALG.setRowData(r.getProcentvisMersalgFormateret());
		RAADGIVERPRODUKTER.setRowData(r.getRaadgiverProdukterFormateret());
		SERVICEPRODUKTERPRLOGINTIME.setRowData(r.getServiceprodukterPrLogintimeFormateret());
	}
	
	public static void getEkspeditionGoalText() {
		for (EColumn c: EColumn.values()){
			if(c.equals(TILGÆNGELIGHED))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: >=64%<br>Gul: 63-62%<xmp>Rød: <=61%");
			else if(c.equals(OPRAKTIVITETIKKEVIDRESTILLING))
				c.setRowData("<html><p style=\"text-align: center;\">Mål: 25%");			
			else if(c.equals(FÆRDIGGØRELSESGRADPCTINDKALD))
				c.setRowData("<html><p style=\"text-align: center;\">Mål: 40%");		
			else if(c.equals(HENVISNINGERPRLOGINTIME))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: >=0,3<br>Gul: 0,2-0,1<xmp>Rød: <0,1");
			else if(c.equals(SERVICEPRODUKTERPRLOGINTIME))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: >=0,3<br>Gul: 0,2-0,1<xmp>Rød: <0,1");
			else if(c.equals(KPI))
				c.setRowData("<html><p style=\"text-align: center;\">Grøn: >=10,0<br>Gul: 9,9-7,2<xmp>Rød: <=9,4");			
			else 
				c.setRowData("");
		}
	}
	
	public int getTrueFalseValue(){
		if (selected){return 1;}{return 0;}
	}
	
	public void setTrueFalseValue(int value){
		if (this == KPISUPPORT) {
			System.out.print("");
		}
		if (value == 1){selected = true; checkBox.setSelected(true);}
		else{selected = false; checkBox.setSelected(false);}
	}

	public String getTableHeaderName() {
		return "<html><p style=\"text-align: center;\">"+tableHeaderName;
	}

	public String toString(){
		if (this.getCategory().equals(ECategory.COLORING)| this.getCategory().equals(ECategory.SETTINGS))
			return this.getCheckBoxName();
		else 
			return this.getTableHeaderName();
	}

}
