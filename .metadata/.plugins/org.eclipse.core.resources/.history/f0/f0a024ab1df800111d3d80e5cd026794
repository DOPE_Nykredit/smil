package com.nykredit.kundeservice.smil;

import java.text.DecimalFormat;
import com.nykredit.kundeservice.util.Formatter;
import com.nykredit.kundeservice.util.Date;

public class DataTableColumns {

	private Formatter bf = new Formatter();
	private Date date = new Date();
	
	private boolean forGnsOrTop10;
	private double 
	indKald,
	udKald,
	consult,
	cbr,
	webdeskKald,
	henvisninger,
	hitrate,
	firstCall,
	kaldIalt,
	tlfBesk,
	forsikringspræmie,
	mersalg,
	indenforTidsfrist,
	aktiviteter,
	supportopgaver,
	supportoverskredne,
	supportIndenforFrist,
	overskredne,
	emailIndgående,
	korrigeret,
	klar,
	taler,
	øvrigTid,
	webdesk,
	email,
	efterbeh,
	andreOpg,
	møde,
	uddannelse,
	kollegahj,
	frokost,
	pause,
	spærret,
	planlagttidStuderende,
	planlagttid,
	aktiviteterIndgående,
	tilbudteServiceprodukter,
	indgåendeKaldDialog,
	solgteServiceprodukter,
	opfølgningsserviceAfsluttet,
	tabtSalg,
	kampagnePotentialer,
	raadgiverProdukter,
	viderestillinger = 0,
	viderestillingerPctIndgåendeKald = 0,
	tlfBeskederPctIndgåendeKald = 0,
	færdiggørelsesgradPctIndgåendeKald = 0,
	henvisningerPrLoginTime = 0,
	oprettedeAktiviteterIkkeViderestillinger = 0,
	kaldPrTime = 0,
	tilgængelighed = 0,
	tilgængelighed2011 = 0,
	gnsHåndtering = 0,
	færdiggørelsesgrad = 0,
	aktiviteterPrLoginTime = 0,
	emailAktiviteterPrLoginTime = 0,
	emailAktiviteterPrEmailLogin = 0,
	aktiviteterIndenforTidsfrist = 0,
	kPI = 0, 
	kPIRaadgiver = 0,
	kPISupport = 0,
	emailTilgængelighed = 0,
	tlfTilgængelighed = 0,
	pauseTid = 0,
	firstCallUdenTelefonBeskeder = 0,
	kampagnepotentialerPrEmailTime = 0,
	aktiviteterAfsluttetIndenfor2TimerEksklEmail = 0,
	aktiviteterAfsluttetIndenfor2TimerInklEmail = 0,
	aktiviteterAfsluttetIndenfor2TimerPrAndreOpgTime = 0,
	procentvisMersalg = 0,
	serviceprodukterPrLogintime = 0;
	
	private String webdeskTalerTid = null;
	
	private String initials, team,teamNavn;
	public DataTableColumns() {
	}
	
	public DataTableColumns(String initials, String team, String teamNavn) {
		this.initials = initials;
		this.team = team;
		this.teamNavn = teamNavn;
	}

	public DataTableColumns(String initials, String team, String teamNavn, boolean forGnsOrTop10) {
		this.forGnsOrTop10 = forGnsOrTop10;
		this.initials = initials;
		this.team = team;
		this.teamNavn = teamNavn;
	}

	public double getAktiviteter() {
		return aktiviteter;
	}
	
	public String getAktiviteterFormateret() {
		return bf.KiloDotFill(aktiviteter, false);
	}

	public double getAktiviteterIndenforTidsfrist() {
		if (aktiviteterIndenforTidsfrist == 0) {
			if ((indenforTidsfrist + overskredne) == 0) {
				return 0;
			}
			aktiviteterIndenforTidsfrist = indenforTidsfrist / (indenforTidsfrist + overskredne);
		}
		return aktiviteterIndenforTidsfrist;
	}

	public String getAktiviteterIndenforTidsfristFormateret() {
		return bf.toProcent(getAktiviteterIndenforTidsfrist(), false);
	}
//**********************************SUPPORTOPGAVER***********************************************	
	public double getSupportOpgaver() {
		return supportopgaver;
	}
	
	public String getSupportOpgaverFormateret() {
		return bf.KiloDotFill(supportopgaver, false);
	}

	public double getSupportIndenForFrist() {
		//System.out.println("getSupportIndenForFrist(): supportopgaver "+supportopgaver+"  supportoverskredne="+supportoverskredne);
		
		return ((supportopgaver - supportoverskredne)/ (supportopgaver));
	}
	
	/*
	public double getSupportIndenForFrist() {
		System.out.println("getSupportIndenForFrist(): "+"");
		
		if (supportIndenforFristProcent == 0) {
			if ((supportIndenforFrist - supportoverskredne) == 0) {
				return 0;
			}
			supportIndenforFristProcent = ((supportopgaver - supportoverskredne)
					/ (supportopgaver));
		}
		return supportIndenforFristProcent;
	}
	 * 
	 * 
	 * public double getSupportIndenforFristProcent(){
		return supportIndenforFristProcent;
	}*/

	//OMREGNER TAL TIL PROCENT SÅ DET VISES SOM PROCENT
	public String getSupportIndenForFristFormateret() {
		return bf.toProcent(getSupportIndenForFrist(), false);
	}
	
	public double getSupportOverskredne() {
		return supportoverskredne;
	}
	public String getSupportOverskredneFormateret() {
		return bf.KiloDotFill(supportoverskredne, false);
	}
//**************************************************************************************************
	public double getAktiviteterIndgående() {
		return aktiviteterIndgående;
	}

	public String getAktiviteterIndgåendeFormateret() {
		return bf.KiloDotFill(aktiviteterIndgående, false);
	}
//***************************************************************************************************
	public double getAktiviteterPrLoginTime() {
		if (aktiviteterPrLoginTime == 0) {
			if (korrigeret == 0) {
				return 0;
			}
			if ((indenforTidsfrist + overskredne + emailIndgående) / (korrigeret * 24) < 0.1) {
				return 0;
			}
			aktiviteterPrLoginTime = (indenforTidsfrist + overskredne + emailIndgående) / (korrigeret * 24);
		}
		return aktiviteterPrLoginTime;
	}

	public String getAktiviteterPrLoginTimeFormateret() {
		return toOneDigit(getAktiviteterPrLoginTime());
	}

	public double getAndreOpg() {
		return andreOpg;
	}

	public String getAndreOpgTTMM() {
		return date.toTTMM(andreOpg, false);
	}

	public double getCbr() {
		return cbr;
	}

	public String getCbrFormateret() {
		return bf.KiloDotFill(cbr, false);
	}

	public double getConsult() {
		return consult;
	}

	public String getConsultFormateret() {
		return bf.KiloDotFill(consult, false);
	}

	public double getEfterbeh() {
		return efterbeh;
	}

	public String getEfterbehTTMM() {
		return date.toTTMM(efterbeh, false);
	}

	public double getEmail() {
		return email;
	}

	public double getEmailAktiviteterPrLoginTime(){
		if (emailAktiviteterPrLoginTime == 0) {
			if (korrigeret == 0)
				return 0;
			
			emailAktiviteterPrLoginTime = emailIndgående / (korrigeret * 24);
		}
		return emailAktiviteterPrLoginTime;
	}

	public String getEmailAktiviteterPrLoginTimeFormateret() {
		return toOneDigit(getEmailAktiviteterPrLoginTime());
	}

	public double getEmailIndgående() {
		return emailIndgående;
	}

	public String getEmailIndgåendeFormateret() {
		return bf.KiloDotFill(emailIndgående, false);
	}

	public double getEmailTilgængelighed() {
		if (emailTilgængelighed == 0) {
			if (Math.round((email / korrigeret) * 100) == 0) {
				return 0;
			}
			emailTilgængelighed = (email / korrigeret);
		}
		return emailTilgængelighed;
	}

	public String getEmailTilgængelighedFormateret() {
		return bf.toProcent(getEmailTilgængelighed(),false);
	}

	public String getEmailTTMM() {
		return date.toTTMM(email,false);
	}

	public double getFirstCall() {
		return firstCall;
	}

	public String getFirstCallFormateret() {
		return bf.KiloDotFill(firstCall, false);
	}

	public double getFirstCallUdenTelefonBeskeder() {
		if (firstCallUdenTelefonBeskeder == 0){
			if (tlfBesk > firstCall) {
				firstCallUdenTelefonBeskeder = 0;
			} else {
				firstCallUdenTelefonBeskeder = firstCall - tlfBesk;
			}	
		}
		return firstCallUdenTelefonBeskeder;
	}

	public String getFirstCallUdenTelefonBeskederFormateret() {
		return bf.KiloDotFill(firstCallUdenTelefonBeskeder,false);
	}

	public boolean getForGnsOrTop10() {
		return forGnsOrTop10;
	}

	public double getForsikringspræmie() {
		return forsikringspræmie;
	}

	public String getForsikringspræmieFormateret() {
		return bf.KiloDotFill(forsikringspræmie,false);
	}

	public double getFrokost() {
		return frokost;
	}

	public String getFrokostTTMM() {
		return date.toTTMM(frokost, false);
	}

	public double getFærdiggørelsesgrad() {
		if (færdiggørelsesgrad == 0) {
			if (kaldIalt == 0) {
				return 0;
			}
			if (getFirstCallUdenTelefonBeskeder() > kaldIalt) {
				færdiggørelsesgrad = 1;
			} else {
				færdiggørelsesgrad = getFirstCallUdenTelefonBeskeder() / kaldIalt;
			}
			if (færdiggørelsesgrad < 0.01) {
				return 0;
			}
			return færdiggørelsesgrad;
		}
		return færdiggørelsesgrad;
	}

	public String getFærdiggørelsesgradFormateret() {
		if (getFærdiggørelsesgrad() == 0) {
			return null;
		}
		return bf.toProcent(getFærdiggørelsesgrad(), false);
	}

	public double getFærdiggørelsesgradPctIndgåendeKald() {
		if (færdiggørelsesgradPctIndgåendeKald == 0){
			if (kaldIalt == 0) return 0;
			færdiggørelsesgradPctIndgåendeKald = getFirstCallUdenTelefonBeskeder() / kaldIalt;
		}
		return færdiggørelsesgradPctIndgåendeKald;
	}

	public String getFærdiggørelsesgradPctIndgåendeKaldFormateret() {
		return bf.toProcent(getFærdiggørelsesgradPctIndgåendeKald(), false);
	}

	public double getGnsHåndtering() {
		if (gnsHåndtering == 0) {
			if (indKald == 0) {
				return 0;
			}
			gnsHåndtering = (taler + efterbeh) / indKald;
			;
		}
		return gnsHåndtering;
	}

	public String getGnsHåndteringFormateret() {
		return date.toTTMM(getGnsHåndtering() * 60, false);
	}

	public double getHenvisninger() {
		return henvisninger;
	}

	public String getHenvisningerFormateret() {
		return bf.KiloDotFill(henvisninger, false);
	}
//****************************************Hitrate henvisninger**********************************************	
	public double getHenvisningerHitrate() {
		return hitrate;
	}

	public String getHenvisningerHitrateFormateret() {
		return bf.toProcent(hitrate,false);
	}
//*******************************Henvisninger pr. Login Time*************************************************
	public double getHenvisningerPrLoginTime() {
		if (henvisningerPrLoginTime == 0){
			if (korrigeret == 0)
				return 0;
			
			henvisningerPrLoginTime = henvisninger / (korrigeret * 24);
		}
		return henvisningerPrLoginTime;
	}

	public String getHenvisningerPrLoginTimeFormateret() {
		return toOneDigit(getHenvisningerPrLoginTime());
	}
	
	public double getServiceprodukterPrLogintime() {
		if (serviceprodukterPrLogintime == 0){
			if (korrigeret == 0)
				return 0;
			
			serviceprodukterPrLogintime = tilbudteServiceprodukter / (korrigeret * 24);
		}
		return serviceprodukterPrLogintime;
	}

	public String getServiceprodukterPrLogintimeFormateret() {
		return toOneDigit(getServiceprodukterPrLogintime());
	}


	public double getIndenforTidsfrist() {
		return indenforTidsfrist;
	}

	public String getIndenforTidsfristFormateret() {
		return bf.KiloDotFill(indenforTidsfrist, false);
	}

	public double getIndgåendeKaldDialog() {
		return indgåendeKaldDialog;
	}

	public String getIndgåendeKaldDialogFormateret() {
		return bf.KiloDotFill(indgåendeKaldDialog, false);
	}

	public double getIndKald() {
		return indKald;
	}

	public String getIndKaldFormateret() {
		return bf.KiloDotFill(indKald, false);
	}

	public String getInitials() {
		return initials;
	}

	public double getKaldIalt() {
		return kaldIalt;
	}

	public String getKaldIaltFormateret() {
		return bf.KiloDotFill(kaldIalt, false);
	}

	public double getKaldPrTime() {
		if(kaldPrTime == 0){
			if (korrigeret == 0)
				return 0;	
			
			kaldPrTime = ((indKald / (korrigeret * 24)));
		}
		return kaldPrTime;
	}

	public String getKaldPrTimeFormateret() {
		return bf.KiloDotFill(getKaldPrTime(), false);
	}

	public double getKlar() {
		return klar;
	}

	public String getKlarTTMM() {
		return date.toTTMM(klar, false);
	}

	public double getKollegahj() {
		return kollegahj;
	}

	public String getKollegahjTTMM() {
		return date.toTTMM(kollegahj, false);
	}

	public double getKorrigeret() {
		return korrigeret;
	}

	public String getKorrigeretTTMM() {
		return date.toTTMM(korrigeret, false);
	}
//***************************************KPI************************************************
	public double getKPI() {
		if (kPI == 0) {
			if (korrigeret == 0)
				return 0;
			
			kPI = (indKald + indenforTidsfrist + overskredne + emailIndgående) / (korrigeret * 24);
		}
		return kPI;
	}
	
	public String getKPIFormateret() {
		return toOneDigit(getKPI());
	}
	
//***************************************KPI Rådgiver************************************************	
	public double getKPIRaadgiver() {
		if (kPIRaadgiver == 0) {
			if (korrigeret == 0)
				return 0;
			
			kPIRaadgiver = (indKald + indenforTidsfrist + overskredne + emailIndgående + consult) / (korrigeret * 24);
		}
		return kPIRaadgiver;
	}

	public String getKPIRaadgiverFormateret() {
		return toOneDigit(getKPIRaadgiver());
	}
//***************************************KPI Support************************************************
	public double getKPISupport() {
		if (kPISupport == 0) {
			if (korrigeret == 0) {
				return 0;
			}
			kPISupport = (indKald + indenforTidsfrist + overskredne + emailIndgående + supportopgaver) / (korrigeret * 24);
		}
		return kPISupport;
	}
	public String getKPISupportFormateret() {
		return toOneDigit(getKPISupport());
	}
//************************************MERSALG*********************************************************
	public double getMersalg() {
		return mersalg;
	}

	public String getMersalgFormateret() {
		return bf.KiloDotFill(mersalg, false);
	}

	public double getMøde() {
		return møde;
	}

	public String getMødeTTMM() {
		return date.toTTMM(møde, false);
	}

	public double getOpfølgningsserviceAfsluttet() {
		return opfølgningsserviceAfsluttet;
	}

	public String getOpfølgningsserviceAfsluttetFormateret() {
		return bf.KiloDotFill(opfølgningsserviceAfsluttet, false);
	}

	public double getOprettedeAktiviteterIkkeViderestillinger(){
		if (oprettedeAktiviteterIkkeViderestillinger == 0){
			if(korrigeret == 0 | firstCall == 0)
				return 0;
			
			if((kaldIalt-getViderestillinger())!=0)
				oprettedeAktiviteterIkkeViderestillinger = indgåendeKaldDialog / firstCall;
		}
		return oprettedeAktiviteterIkkeViderestillinger;
	}

	public String getOprettedeAktiviteterIkkeViderestillingerFormateret() {
		return bf.toProcent(getOprettedeAktiviteterIkkeViderestillinger(), false);
	}

	public double getOverskredne() {
		return overskredne;
	}

	public String getOverskredneFormateret() {
		return bf.KiloDotFill(overskredne, false);
	}

	public double getPause() {
		return pause;
	}

	public double getPauseTid() {
		if (pauseTid == 0) {
			if ((pause) == 0)
				return 0;

			pauseTid = (pause / korrigeret);
		}
		return pauseTid;
	}

	public String getPauseTidFormateret() {
		return bf.toProcent(getPauseTid(), false);
	}

	public String getPauseTTMM() {
		return date.toTTMM(pause, false);
	}

	public double getPlanlagttid() {
		return planlagttid;
	}

	public double getPlanlagttidStuderende() {
		return planlagttidStuderende;
	}

	public String getPlanlagttidStuderendeTTMM() {
		if (planlagttidStuderende == 0)
			return null;
		
		return date.toTTMM(planlagttidStuderende, false);
	}

	public String getPlanlagttidTTMM() {
		if (planlagttid == 0)
			return null;
		
		return date.toTTMM(planlagttid, false);
	}

	public double getSolgteServiceprodukter() {
		return solgteServiceprodukter;
	}

	public String getSolgteServiceprodukterFormateret() {
		return bf.KiloDotFill(solgteServiceprodukter, false);
	}

	public double getSpærret() {
		return spærret;
	}

	public String getSpærretTTMM() {
		return date.toTTMM(spærret, false);
	}

	public double getTabtSalg() {
		return tabtSalg;
	}

	public String getTabtSalgFormateret() {
		return bf.KiloDotFill(tabtSalg, false);
	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////////
	// //////////////////////////FORMLER BASERET PÅ FLERE VÆRDIER
	// /////////////////////////////////////////////
	// ////////////////////////////////////////////////////////////////////////////////////////////////////////

	public double getTaler() {
		return taler;
	}

	public String getTalerTTMM() {
		return date.toTTMM(taler,false);
	}

	public String getTeam() {
		return team;
	}

	public String getTeamNavn() {
		return teamNavn;
	}

	public double getTilbudteServiceprodukter() {
		return tilbudteServiceprodukter;
	}

	public String getTilbudteServiceprodukterFormateret() {
		return bf.KiloDotFill(tilbudteServiceprodukter, false);
	}

	public double getTilgængelighed() {
		if (tilgængelighed == 0) {
			if (korrigeret == 0)
				return 0;

			tilgængelighed = (klar + taler + øvrigTid + webdesk) / (korrigeret - email);
		}
		return tilgængelighed;
	}

	public String getTilgængelighedFormateret() {
		return bf.toProcent(getTilgængelighed(), false);
	}
	
	public double getTilgængelighed2011() {
		if (tilgængelighed2011 == 0) {
			if (korrigeret == 0)
				return 0;
			
			tilgængelighed2011 = (klar + taler + øvrigTid + webdesk + email) / korrigeret;
		}
		return tilgængelighed2011;
	}

	public String getTilgængelighed2011Formateret() {
		return bf.toProcent(getTilgængelighed2011(), false);
	}

	public double getTlfBesk() {
		return tlfBesk;
	}

	public double getTlfBeskederPctIndgåendeKald() {
		if(tlfBeskederPctIndgåendeKald == 0){
			if (indKald == 0)
				return 0;
			
			tlfBeskederPctIndgåendeKald = tlfBesk / kaldIalt;
		}
		return tlfBeskederPctIndgåendeKald;
	}

	public String getTlfBeskederPctIndgåendeKaldFormateret() {
		return bf.toProcent(getTlfBeskederPctIndgåendeKald(), false);
	}

	public String getTlfBeskFormateret() {
		return bf.KiloDotFill(tlfBesk, false);
	}
	
	public double getProcentvisMersalg() {
		if (procentvisMersalg == 0) {
			if (Math.round((mersalg / forsikringspræmie) * 100) == 0) {
				return 0;
			}
			procentvisMersalg = (mersalg / forsikringspræmie);
		}
		return procentvisMersalg;
	}
	
	public void setProcentvisMersalg(double procentvisMersalg){
		this.procentvisMersalg = procentvisMersalg;
	}
	
	public String getProcentvisMersalgFormateret() {
		return bf.toProcent(getProcentvisMersalg(),false);
	}

	public double getTlfTilgængelighed() {
		if (tlfTilgængelighed == 0) {
			if (Math.round(((klar + taler + øvrigTid) / korrigeret) * 100) == 0) {
				return 0;
			}
			tlfTilgængelighed = ((klar + taler + øvrigTid) / korrigeret);
		}
		return tlfTilgængelighed;
	}

	public String getTlfTilgængelighedFormateret() {
		return bf.toProcent(getTlfTilgængelighed(),false);
	}

	public double getUddannelse() {
		return uddannelse;
	}

	public String getUddannelseTTMM() {
		return date.toTTMM(uddannelse,false);
	}

	public double getUdKald() {
		return udKald;
	}

	public String getUdKaldFormateret() {
		return bf.KiloDotFill(udKald,false);
	}

	public double getViderestillinger() {
		if (viderestillinger == 0){
			double firscalludentlfbesk = 0;
			if ((firstCall-tlfBesk)>=0){
				firscalludentlfbesk = firstCall-tlfBesk;
				}
			if(firscalludentlfbesk+tlfBesk<kaldIalt){
				viderestillinger = kaldIalt-firscalludentlfbesk-tlfBesk;
				}
		}
		return viderestillinger;
	}

	public String getViderestillingerFormateret() {
		return bf.KiloDotFill(getViderestillinger(), false);
	}

	public double getViderestillingerPctIndgåendeKald() {
		if (viderestillingerPctIndgåendeKald == 0){
			if (indKald == 0)return 0;	
			viderestillingerPctIndgåendeKald = getViderestillinger()/kaldIalt;
		}
		return viderestillingerPctIndgåendeKald;
	}

	public String getViderestillingerPctIndgåendeKaldFormateret() {
		return bf.toProcent(getViderestillingerPctIndgåendeKald(),false);
	}

	public double getWebdesk() {
		return webdesk;
	}

	public String getWebdeskTTMM() {
		return date.toTTMM(webdesk,false);
	}

	public double getØvrigTid() {
		return øvrigTid;
	}

	public String getØvrigTidTTMM() {
		return date.toTTMM(øvrigTid,false);
	}

	public String getWebdeskKald(){
		return bf.KiloDotFill(this.webdeskKald, false);
	}
	public String getWebdeskTalerTid(){
		return this.webdeskTalerTid;
	}
	
	public void setWebdeskKald(double webdeskKald){
		this.webdeskKald = webdeskKald;
	}
	public void setWebdeskTalerTid(String webdeskTalerTid){
		this.webdeskTalerTid = webdeskTalerTid;
	}
	
	public void setAktiviteter(double aktiviteter) {
		this.aktiviteter = aktiviteter;
	}

	public void setAktiviteterIndenforTidsfrist(double aktiviteterIndenforTidsfrist) {
		this.aktiviteterIndenforTidsfrist = aktiviteterIndenforTidsfrist;
	}

	public void setAktiviteterIndgående(double aktiviteterIndgående) {
		this.aktiviteterIndgående = aktiviteterIndgående;
	}

	public void setAktiviteterPrLoginTime(double aktiviteterPrLoginTime) {
		this.aktiviteterPrLoginTime = aktiviteterPrLoginTime;
	}
//***********************SUPPORTOPGAVER*********************************
	public void setSupportOpgaver(double supportopgaver) {
		this.supportopgaver = supportopgaver;
	}
	
	public void setSupportOverskredne(double supportoverskredne) {
		this.supportoverskredne = supportoverskredne;
	}
	
	public void setSupportIndenForFrist(double supportIndenforFrist) {
		this.setSupportIndenforFrist(supportIndenforFrist);
	}
	
	/*public void setSupportIndenforFristProcent(double supportIndenforFristProcent) {
		this.supportIndenforFristProcent = supportIndenforFristProcent;
	}*/
//*************************************************************************************************
	public void setAndreOpg(double andreOpg) {
		this.andreOpg = andreOpg;
	}

	public void setCbr(double cbr) {
		this.cbr = cbr;
	}

	public void setConsult(double consult) {
		this.consult = consult;
	}

	public void setEfterbeh(double efterbeh) {
		this.efterbeh = efterbeh;
	}

	public void setEmail(double email) {
		this.email = email;
	}

	public void setEmailAktiviteterPrLoginTime(double emailAktiviteterPrLoginTime){
		this.emailAktiviteterPrLoginTime = emailAktiviteterPrLoginTime;
	}
	

	public void setEmailIndgående(double emailIndgående) {
		this.emailIndgående = emailIndgående;
	}

	public void setEmailTilgængelighed(double emailTilgængelighed) {
		this.emailTilgængelighed = emailTilgængelighed;
	}
	
	public void setFirstCall(double firstCall) {
		this.firstCall = firstCall;
	}

	public void setFirstCallUdenTelefonBeskeder(double firstCallUdenTelefonBeskeder) {
		this.firstCallUdenTelefonBeskeder = firstCallUdenTelefonBeskeder;
	}
	
	public void setForGnsOrTop10(boolean forGnsOrTop10) {
		this.forGnsOrTop10 = forGnsOrTop10;
	}
	
	public void setForsikringspræmie(double forsikringspræmie) {
		this.forsikringspræmie = forsikringspræmie;
	}

	public void setFrokost(double frokost) {
		this.frokost = frokost;
	}
	
	public void setFærdiggørelsesgrad(double færdiggørelsesgrad) {
		this.færdiggørelsesgrad = færdiggørelsesgrad;
	}

	public void setFærdiggørelsesgradPctIndgåendeKald(
			double færdiggørelsesgradPctIndgåendeKald) {
		this.færdiggørelsesgradPctIndgåendeKald = færdiggørelsesgradPctIndgåendeKald;
	}

	public void setGnsHåndtering(double gnsHåndtering) {
		this.gnsHåndtering = gnsHåndtering;
	}
	
	public void setHenvisninger(double henvisninger) {
		this.henvisninger = henvisninger;
	}
	
	public void setHenvisningerHitrate(double hitrate) {
		this.hitrate = hitrate;
	}

	public void setHenvisningerPrLoginTime(double henvisningerPrLoginTime) {
		this.henvisningerPrLoginTime = henvisningerPrLoginTime;
	}
	
	public void setServiceprodukterPrLoginTime(double serviceprodukterPrLogintime) {
		this.serviceprodukterPrLogintime = serviceprodukterPrLogintime;
	}
	
	public void setIndenforTidsfrist(double indenforTidsfrist) {
		this.indenforTidsfrist = indenforTidsfrist;
	}
	
	public void setIndgåendeKaldDialog(double indgåendeKaldDialog) {
		this.indgåendeKaldDialog = indgåendeKaldDialog;
	}

	public void setIndKald(double indKald) {
		this.indKald = indKald;
	}
	
	public void setInitials(String initials) {
		this.initials = initials;
	}

	public void setKaldIalt(double kaldIalt) {
		this.kaldIalt = kaldIalt;
	}

	public void setKaldPrTime(double kaldPrTime) {
		this.kaldPrTime = kaldPrTime;
	}
	
	public void setKlar(double klar) {
		this.klar = klar;
	}

	public void setKollegahj(double kollegahj) {
		this.kollegahj = kollegahj;
	}

	public void setKorrigeret(double korrigeret) {
		this.korrigeret = korrigeret;
	}

	public void setKPI(double kPI) {
		this.kPI = kPI;
	}
	
	public void setKPIRaadgiver(double kPIRaadgiver) {
		this.kPIRaadgiver = kPIRaadgiver;
	}
//*********************************KPI support********************************************	
	public void setKPISupport(double kPISupport) {
		this.kPISupport = kPISupport;
	}
	public void setMersalg(double mersalg) {
		this.mersalg = mersalg;
	}

	public void setMøde(double møde) {
		this.møde = møde;
	}

	public void setOpfølgningsserviceAfsluttet(double opfølgningsserviceAfsluttet) {
		this.opfølgningsserviceAfsluttet = opfølgningsserviceAfsluttet;
	}
	
	public void setOprettedeAktiviteterIkkeViderestillinger(
			double oprettedeAktiviteterIkkeViderestillinger) {
		this.oprettedeAktiviteterIkkeViderestillinger = oprettedeAktiviteterIkkeViderestillinger;
	}

	public void setOverskredne(double overskredne) {
		this.overskredne = overskredne;
	}
	
	public void setPause(double pause) {
		this.pause = pause;
	}

	public void setPauseTid(double pauseTid) {
		this.pauseTid = pauseTid;
	}

	public void setPlanlagttid(double planlagttid) {
		this.planlagttid = planlagttid;
	}

	public void setPlanlagttidStuderende(double planlagttidStuderende) {
		this.planlagttidStuderende = planlagttidStuderende;
	}

	public void setSolgteServiceprodukter(double solgteServiceprodukter) {
		this.solgteServiceprodukter = solgteServiceprodukter;
	}

	public void setSpærret(double spærret) {
		this.spærret = spærret;
	}
	
	public void setTabtSalg(double tabtSalg) {
		this.tabtSalg = tabtSalg;
	}

	public void setTaler(double taler) {
		this.taler = taler;
	}

	public void setTeam(String team) {
		this.team = team;
	}
	
	public void setTeamNavn(String teamNavn) {
		this.teamNavn = teamNavn;
	}

	public void setTilbudteServiceprodukter(double tilbudteServiceprodukter) {
		this.tilbudteServiceprodukter = tilbudteServiceprodukter;
	}

	public void setTilgængelighed(double tilgængelighed) {
		this.tilgængelighed = tilgængelighed;
	}
	
	public void setTilgængelighed2011(double tilgængelighed2011) {
		this.tilgængelighed2011 = tilgængelighed2011;
	}
	
	public void setTlfBesk(double tlfBesk) {
		this.tlfBesk = tlfBesk;
	}

	public void setTlfBeskederPctIndgåendeKald(double tlfBeskederPctIndgåendeKald) {
		this.tlfBeskederPctIndgåendeKald = tlfBeskederPctIndgåendeKald;
	}

	public void setTlfTilgængelighed(double tlfTilgængelighed) {
		this.tlfTilgængelighed = tlfTilgængelighed;
	}

	public void setUddannelse(double uddannelse) {
		this.uddannelse = uddannelse;
	}
	
	public void setUdKald(double udKald) {
		this.udKald = udKald;
	}

	public void setViderestillinger(double viderestillinger) {
		this.viderestillinger = viderestillinger;
	}
	
	public void setViderestillingerPctIndgåendeKald(double viderestillingerPctIndgåendeKald) {
		this.viderestillingerPctIndgåendeKald = viderestillingerPctIndgåendeKald;
	}

	public void setWebdesk(double webdesk) {
		this.webdesk = webdesk;
	}

	public void setØvrigTid(double øvrigTid) {
		this.øvrigTid = øvrigTid;
	}

	private String toOneDigit(double value){
		if (value == 0)return null;
		DecimalFormat myFormat = new java.text.DecimalFormat("0.0");
		return myFormat.format(value);
	}

	public double getEmailAktiviteterPrEmailLogin() {
		if (emailAktiviteterPrEmailLogin == 0) {
			if (email == 0) {
				return 0;
			}
			emailAktiviteterPrEmailLogin = this.getEmailIndgående() / (email * 24);
		}
		return emailAktiviteterPrEmailLogin;
	}

	public void setEmailAktiviteterPrEmailLogin(double emailAktiviteterPrEmailLogin) {
		this.emailAktiviteterPrEmailLogin = emailAktiviteterPrEmailLogin;
	}
	
	public String getEmailAktiviteterPrEmailLoginFormateret(){
		return toOneDigit(getEmailAktiviteterPrEmailLogin());
	}

	public double getKampagnePotentialer() {
		return kampagnePotentialer;
	}
	
	public String getKampagnePotentialerFormateret() {
		return bf.KiloDotFill(kampagnePotentialer,false);
	}
	
	public void setKampagnePotentialer(double kampagnePotentialer) {
		this.kampagnePotentialer = kampagnePotentialer;
	}

	public double getKampagnepotentialerPrEmailTime() {
		if (this.kampagnepotentialerPrEmailTime == 0) {
			if (email == 0) {
				return 0;
			}
			kampagnepotentialerPrEmailTime = getKampagnePotentialer() / (email * 24);
		}
		return kampagnepotentialerPrEmailTime;
	}
	public String getKampagnepotentialerPrEmailTimeFormateret(){
		return toOneDigit(getKampagnepotentialerPrEmailTime());
	}

	public void setKampagnepotentialerPrEmailTime(
			double kampagnepotentialerPrEmailTime) {
		this.kampagnepotentialerPrEmailTime = kampagnepotentialerPrEmailTime;
	}

	public double getAktiviteterAfsluttetIndenfor2TimerEksklEmail() {
		if (this.aktiviteterAfsluttetIndenfor2TimerEksklEmail == 0) {
			aktiviteterAfsluttetIndenfor2TimerEksklEmail = this.getIndenforTidsfrist() + this.getOverskredne();
		}
		
		return aktiviteterAfsluttetIndenfor2TimerEksklEmail;
	}
	
	public String getAktiviteterAfsluttetIndenfor2TimerEksklEmailFormateret() {
		return bf.KiloDotFill(getAktiviteterAfsluttetIndenfor2TimerEksklEmail(), false);
	}	

	public void setAktiviteterAfsluttetIndenfor2TimerEksklEmail(double aktiviteterAfsluttetIndenfor2TimerEksklEmail) {
		this.aktiviteterAfsluttetIndenfor2TimerEksklEmail = aktiviteterAfsluttetIndenfor2TimerEksklEmail;
	}
	
	public double getAktiviteterAfsluttetIndenfor2TimerInklEmail(){
		if (this.aktiviteterAfsluttetIndenfor2TimerInklEmail == 0) {
			aktiviteterAfsluttetIndenfor2TimerInklEmail = this.getIndenforTidsfrist() + this.getOverskredne() + this.getEmailIndgående();
		}
		
		return aktiviteterAfsluttetIndenfor2TimerInklEmail;
	}
	public String getAktiviteterAfsluttetIndenfor2TimerInklEmailFormateret(){
		return bf.KiloDotFill(this.getAktiviteterAfsluttetIndenfor2TimerInklEmail(), false);
	}
	
	public void setAktiviteterAfsluttetIndenfor2TimerInklEmail(double aktiviteterAfsluttetIndenfor2TimerInklEmail){
		this.aktiviteterAfsluttetIndenfor2TimerInklEmail = aktiviteterAfsluttetIndenfor2TimerInklEmail;
	}

	public double getAktiviteterAfsluttetIndenfor2TimerPrAndreOpgTime() {
		if (this.aktiviteterAfsluttetIndenfor2TimerPrAndreOpgTime == 0){
			if(this.andreOpg == 0 | getAktiviteterAfsluttetIndenfor2TimerEksklEmail() == 0)return 0;
			aktiviteterAfsluttetIndenfor2TimerPrAndreOpgTime = this.getAndreOpg()/getAktiviteterAfsluttetIndenfor2TimerEksklEmail();
		}
		return aktiviteterAfsluttetIndenfor2TimerPrAndreOpgTime;
	}
	
	public String getAktiviteterAfsluttetIndenfor2TimerPrAndreOpgTimeFormateret() {
		return date.toTTMM(getAktiviteterAfsluttetIndenfor2TimerPrAndreOpgTime(),false);
	}	

	public void setAktiviteterAfsluttetIndenfor2TimerPrAndreOpgTime(double aktiviteterAfsluttetForskelligFraOprettetPrAndreOpgTime) {
		this.aktiviteterAfsluttetIndenfor2TimerPrAndreOpgTime = aktiviteterAfsluttetForskelligFraOprettetPrAndreOpgTime;
	}

	public double getRaadgiverProdukter() {
		return raadgiverProdukter;
	}
	
	public String getRaadgiverProdukterFormateret(){
		return bf.KiloDotFill(raadgiverProdukter,false);
	}

	public void setRaadgiverProdukter(double raadgiverProdukter) {
		this.raadgiverProdukter = raadgiverProdukter;
	}

	public double getSupportIndenforFrist() {
		return supportIndenforFrist;
	}

	public void setSupportIndenforFrist(double supportIndenforFrist) {
		this.supportIndenforFrist = supportIndenforFrist;
	}
}
